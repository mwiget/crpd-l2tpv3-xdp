#!/bin/bash

SECONDS=0

echo -n "waiting for BGP sessions on rr1 from all VTEPs are up ..."
while true; do
  docker exec rr1 cli show bgp summary | grep Establ | wc -l | grep 3 && break
  echo -n "."
  sleep 1
done

echo ""
for router in r1 r2 r3; do
  for interface in eth0 eth1 eth2; do
    echo "collecting xdp stats on $router ..."
    if ! docker exec -ti $router /root/update_xdp_vxlan_fdb.sh $interface ; then
      echo -n ""
    fi
  done
done

docker-compose up -d vethpass
