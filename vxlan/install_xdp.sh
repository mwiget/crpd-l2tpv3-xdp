#!/bin/bash

set -e

for container in r1 r2 r3; do
  for interface in eth0 eth1 eth2; do
    echo "installing xdp on $interface @ $container ..."
    docker exec $container /bin/bash -c "ulimit -l 2048 && xdp_loader -d $interface --auto-mode --force --filename /root/xdp_router.o --progsec xdp_vxlan && /root/update_xdp_vxlan_fdb.sh $interface"
  done
done

docker-compose up -d vethpass
