## evpn/vxlan lab setup

Uses cRPD as RR and FRR instances as VTEP, Alpine hosts used as endpoints, deployed via docker-compose.

Add license keys to cRPD instances rr1 and rr2! Required for BGP sessions to come up.

Developed on Linux (Ubuntu 20.04), but works as well on Docker Desktop for Mac.


```mermaid
graph TB
rr1["rr1 10.0.1.11"]-.-|BGP|r1["  r1 VTEP<br/>10.0.1.21"]
rr1["rr1 10.0.1.11"]-.-|BGP|r2["  r2 VTEP<br/>10.0.1.22"]
rr1["rr1 10.0.1.11"]-.-|BGP|r3["  r3 VTEP<br/>10.0.1.23"]
rr2["rr2 10.0.1.12"]-.-|BGP|r1
rr2["rr2 10.0.1.12"]-.-|BGP|r2
rr2["rr2 10.0.1.12"]-.-|BGP|r3
r1---host11["    host11<br/>192.168.11.11"]
r1---host12["    host12<br/>192.168.12.12"]
r2---host21["    host21<br/>192.168.11.21"]
r2---host22["    host22<br/>192.168.12.22"]
r3---host31["    host31<br/>192.168.11.31"]
r3---host32["    host32<br/>192.168.12.32"]
style rr1 fill:#390
style rr2 fill:#390
style r1 fill:#f90
style r2 fill:#f90
style r3 fill:#f90
style host11 fill:#09f
style host12 fill:#f9f
style host21 fill:#09f
style host22 fill:#f9f
style host31 fill:#09f
style host32 fill:#f9f
```

```
$ docker-compose up -d
. . .
$ docker-compose ps
    Name                  Command             State             Ports           
--------------------------------------------------------------------------------
host11          /bin/sh /config/network-in    Up                                
                ...                                                             
host12          /bin/sh /config/network-in    Up                                
                ...                                                             
host21          /bin/sh /config/network-in    Up                                
                ...                                                             
host22          /bin/sh /config/network-in    Up                                
                ...                                                             
host31          /bin/sh /config/network-in    Up                                
                ...                                                             
host32          /bin/sh /config/network-in    Up                                
                ...                                                             
r1              /bin/bash /etc/frr/run-       Up                                
                frr.sh                                                          
r2              /bin/bash /etc/frr/run-       Up                                
                frr.sh                                                          
r3              /bin/bash /etc/frr/run-       Up                                
                frr.sh                                                          
rr1             /sbin/runit-init.sh           Up      179/tcp, 22/tcp, 3784/tcp,
                                                      4784/tcp, 50051/tcp,      
                                                      6784/tcp, 7784/tcp,       
                                                      830/tcp                   
rr2             /sbin/runit-init.sh           Up      179/tcp, 22/tcp, 3784/tcp,
                                                      4784/tcp, 50051/tcp,      
                                                      6784/tcp, 7784/tcp,       
                                                      830/tcp                   
vxlan_links_1   /usr/bin/python3 /add_link    Up                                
...                                                             
```

Validate connectivity:

```
$ ./validate.sh

waiting for BGP sessions on rr1 from all VTEPs are up .............3
waiting for BGP sessions on rr2 from all VTEPs are up ...3

ping from host11 to 192.168.11.21 ...
PING 192.168.11.21 (192.168.11.21) 56(84) bytes of data.
64 bytes from 192.168.11.21: icmp_seq=1 ttl=64 time=0.193 ms
64 bytes from 192.168.11.21: icmp_seq=2 ttl=64 time=0.154 ms
64 bytes from 192.168.11.21: icmp_seq=3 ttl=64 time=0.121 ms

--- 192.168.11.21 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2026ms
rtt min/avg/max/mdev = 0.121/0.156/0.193/0.029 ms

ping from host11 to 192.168.11.31 ...
PING 192.168.11.31 (192.168.11.31) 56(84) bytes of data.
64 bytes from 192.168.11.31: icmp_seq=1 ttl=64 time=0.171 ms
64 bytes from 192.168.11.31: icmp_seq=2 ttl=64 time=0.150 ms
64 bytes from 192.168.11.31: icmp_seq=3 ttl=64 time=0.120 ms

--- 192.168.11.31 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2040ms
rtt min/avg/max/mdev = 0.120/0.147/0.171/0.020 ms

ping from host12 to 192.168.12.22 ...
PING 192.168.12.22 (192.168.12.22) 56(84) bytes of data.
64 bytes from 192.168.12.22: icmp_seq=1 ttl=64 time=0.157 ms
64 bytes from 192.168.12.22: icmp_seq=2 ttl=64 time=0.121 ms
64 bytes from 192.168.12.22: icmp_seq=3 ttl=64 time=0.120 ms

--- 192.168.12.22 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2029ms
rtt min/avg/max/mdev = 0.120/0.132/0.157/0.017 ms

ping from host12 to 192.168.12.32 ...
PING 192.168.12.32 (192.168.12.32) 56(84) bytes of data.
64 bytes from 192.168.12.32: icmp_seq=1 ttl=64 time=0.162 ms
64 bytes from 192.168.12.32: icmp_seq=2 ttl=64 time=0.146 ms
64 bytes from 192.168.12.32: icmp_seq=3 ttl=64 time=0.108 ms

--- 192.168.12.32 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2032ms
rtt min/avg/max/mdev = 0.108/0.138/0.162/0.022 ms

updating xdp fdb on r1 interface eth0 ...
vtep_source=10.0.1.21
before foreach ...
fa:cc:c1:0e:52:5d local 9 eth1 (null)
ca:6a:b3:33:97:44 remote 0 (null) 10.0.1.23
b2:52:3c:d2:99:25 local 10 br100 (null)
a2:ac:31:f6:13:8a remote 0 (null) 10.0.1.22
72:26:f4:d3:c9:e8 local 11 eth2 (null)
42:30:a3:d4:2b:ce remote 0 (null) 10.0.1.23
d2:b9:f8:92:55:a5 local 13 br200 (null)
5a:f8:1f:0b:ce:3a remote 0 (null) 10.0.1.22
updating tx_port map ...
tx_port: vxlan100(8)
tx_port: ip6tnl0(6)
tx_port: br100(10)
tx_port: eth1(9)
tx_port: tunl0(7)
tx_port: lo(1)
tx_port: sit0(5)
tx_port: gretap0(3)
tx_port: eth2(11)
tx_port: vxlan200(12)
tx_port: erspan0(4)
tx_port: eth0(64)
tx_port: gre0(2)
tx_port: br200(13)
updating xdp fdb on r1 interface eth1 ...
vtep_source=10.0.1.21
before foreach ...
fa:cc:c1:0e:52:5d local 9 eth1 (null)
ca:6a:b3:33:97:44 remote 0 (null) 10.0.1.23
b2:52:3c:d2:99:25 local 10 br100 (null)
a2:ac:31:f6:13:8a remote 0 (null) 10.0.1.22
72:26:f4:d3:c9:e8 local 11 eth2 (null)
42:30:a3:d4:2b:ce remote 0 (null) 10.0.1.23
d2:b9:f8:92:55:a5 local 13 br200 (null)
5a:f8:1f:0b:ce:3a remote 0 (null) 10.0.1.22
updating tx_port map ...
tx_port: vxlan100(8)
tx_port: ip6tnl0(6)
tx_port: br100(10)
tx_port: eth1(9)
tx_port: tunl0(7)
tx_port: lo(1)
tx_port: sit0(5)
tx_port: gretap0(3)
tx_port: eth2(11)
tx_port: vxlan200(12)
tx_port: erspan0(4)
tx_port: eth0(64)
tx_port: gre0(2)
tx_port: br200(13)
updating xdp fdb on r1 interface eth2 ...
vtep_source=10.0.1.21
before foreach ...
fa:cc:c1:0e:52:5d local 9 eth1 (null)
ca:6a:b3:33:97:44 remote 0 (null) 10.0.1.23
b2:52:3c:d2:99:25 local 10 br100 (null)
a2:ac:31:f6:13:8a remote 0 (null) 10.0.1.22
72:26:f4:d3:c9:e8 local 11 eth2 (null)
42:30:a3:d4:2b:ce remote 0 (null) 10.0.1.23
d2:b9:f8:92:55:a5 local 13 br200 (null)
5a:f8:1f:0b:ce:3a remote 0 (null) 10.0.1.22
updating tx_port map ...
tx_port: vxlan100(8)
tx_port: ip6tnl0(6)
tx_port: br100(10)
tx_port: eth1(9)
tx_port: tunl0(7)
tx_port: lo(1)
tx_port: sit0(5)
tx_port: gretap0(3)
tx_port: eth2(11)
tx_port: vxlan200(12)
tx_port: erspan0(4)
tx_port: eth0(64)
tx_port: gre0(2)
tx_port: br200(13)
updating xdp fdb on r2 interface eth0 ...
vtep_source=10.0.1.22
before foreach ...
fa:cc:c1:0e:52:5d remote 0 (null) 10.0.1.21
ca:6a:b3:33:97:44 remote 0 (null) 10.0.1.23
ce:7e:94:b9:ef:18 local 9 br100 (null)
a2:ac:31:f6:13:8a local 13 eth1 (null)
72:26:f4:d3:c9:e8 remote 0 (null) 10.0.1.21
42:30:a3:d4:2b:ce remote 0 (null) 10.0.1.23
5a:f8:1f:0b:ce:3a local 15 eth2 (null)
22:ff:72:d6:47:b2 local 11 br200 (null)
updating tx_port map ...
tx_port: ip6tnl0(6)
tx_port: eth2(15)
tx_port: tunl0(7)
tx_port: eth0(62)
tx_port: gre0(2)
tx_port: vxlan100(8)
tx_port: lo(1)
tx_port: eth1(13)
tx_port: gretap0(3)
tx_port: br200(11)
tx_port: erspan0(4)
tx_port: sit0(5)
tx_port: br100(9)
tx_port: vxlan200(10)
updating xdp fdb on r2 interface eth1 ...
vtep_source=10.0.1.22
before foreach ...
fa:cc:c1:0e:52:5d remote 0 (null) 10.0.1.21
ca:6a:b3:33:97:44 remote 0 (null) 10.0.1.23
ce:7e:94:b9:ef:18 local 9 br100 (null)
a2:ac:31:f6:13:8a local 13 eth1 (null)
72:26:f4:d3:c9:e8 remote 0 (null) 10.0.1.21
42:30:a3:d4:2b:ce remote 0 (null) 10.0.1.23
5a:f8:1f:0b:ce:3a local 15 eth2 (null)
22:ff:72:d6:47:b2 local 11 br200 (null)
updating tx_port map ...
tx_port: ip6tnl0(6)
tx_port: eth2(15)
tx_port: tunl0(7)
tx_port: eth0(62)
tx_port: gre0(2)
tx_port: vxlan100(8)
tx_port: lo(1)
tx_port: eth1(13)
tx_port: gretap0(3)
tx_port: br200(11)
tx_port: erspan0(4)
tx_port: sit0(5)
tx_port: br100(9)
tx_port: vxlan200(10)
updating xdp fdb on r2 interface eth2 ...
vtep_source=10.0.1.22
before foreach ...
fa:cc:c1:0e:52:5d remote 0 (null) 10.0.1.21
ca:6a:b3:33:97:44 remote 0 (null) 10.0.1.23
ce:7e:94:b9:ef:18 local 9 br100 (null)
a2:ac:31:f6:13:8a local 13 eth1 (null)
72:26:f4:d3:c9:e8 remote 0 (null) 10.0.1.21
42:30:a3:d4:2b:ce remote 0 (null) 10.0.1.23
5a:f8:1f:0b:ce:3a local 15 eth2 (null)
22:ff:72:d6:47:b2 local 11 br200 (null)
updating tx_port map ...
tx_port: ip6tnl0(6)
tx_port: eth2(15)
tx_port: tunl0(7)
tx_port: eth0(62)
tx_port: gre0(2)
tx_port: vxlan100(8)
tx_port: lo(1)
tx_port: eth1(13)
tx_port: gretap0(3)
tx_port: br200(11)
tx_port: erspan0(4)
tx_port: sit0(5)
tx_port: br100(9)
tx_port: vxlan200(10)
updating xdp fdb on r3 interface eth0 ...
vtep_source=10.0.1.23
before foreach ...
fa:cc:c1:0e:52:5d remote 0 (null) 10.0.1.21
ca:6a:b3:33:97:44 local 17 eth1 (null)
d6:cb:2e:d2:31:11 local 9 br100 (null)
a2:ac:31:f6:13:8a remote 0 (null) 10.0.1.22
72:26:f4:d3:c9:e8 remote 0 (null) 10.0.1.21
42:30:a3:d4:2b:ce local 19 eth2 (null)
5a:f8:1f:0b:ce:3a remote 0 (null) 10.0.1.22
7e:8f:f4:cf:d5:1e local 11 br200 (null)
updating tx_port map ...
tx_port: sit0(5)
tx_port: vxlan100(8)
tx_port: eth2(19)
tx_port: br200(11)
tx_port: eth0(58)
tx_port: gre0(2)
tx_port: gretap0(3)
tx_port: erspan0(4)
tx_port: lo(1)
tx_port: br100(9)
tx_port: tunl0(7)
tx_port: eth1(17)
tx_port: vxlan200(10)
tx_port: ip6tnl0(6)
updating xdp fdb on r3 interface eth1 ...
vtep_source=10.0.1.23
before foreach ...
fa:cc:c1:0e:52:5d remote 0 (null) 10.0.1.21
ca:6a:b3:33:97:44 local 17 eth1 (null)
d6:cb:2e:d2:31:11 local 9 br100 (null)
a2:ac:31:f6:13:8a remote 0 (null) 10.0.1.22
72:26:f4:d3:c9:e8 remote 0 (null) 10.0.1.21
42:30:a3:d4:2b:ce local 19 eth2 (null)
5a:f8:1f:0b:ce:3a remote 0 (null) 10.0.1.22
7e:8f:f4:cf:d5:1e local 11 br200 (null)
updating tx_port map ...
tx_port: sit0(5)
tx_port: vxlan100(8)
tx_port: eth2(19)
tx_port: br200(11)
tx_port: eth0(58)
tx_port: gre0(2)
tx_port: gretap0(3)
tx_port: erspan0(4)
tx_port: lo(1)
tx_port: br100(9)
tx_port: tunl0(7)
tx_port: eth1(17)
tx_port: vxlan200(10)
tx_port: ip6tnl0(6)
updating xdp fdb on r3 interface eth2 ...
vtep_source=10.0.1.23
before foreach ...
fa:cc:c1:0e:52:5d remote 0 (null) 10.0.1.21
ca:6a:b3:33:97:44 local 17 eth1 (null)
d6:cb:2e:d2:31:11 local 9 br100 (null)
a2:ac:31:f6:13:8a remote 0 (null) 10.0.1.22
72:26:f4:d3:c9:e8 remote 0 (null) 10.0.1.21
42:30:a3:d4:2b:ce local 19 eth2 (null)
5a:f8:1f:0b:ce:3a remote 0 (null) 10.0.1.22
7e:8f:f4:cf:d5:1e local 11 br200 (null)
updating tx_port map ...
tx_port: sit0(5)
tx_port: vxlan100(8)
tx_port: eth2(19)
tx_port: br200(11)
tx_port: eth0(58)
tx_port: gre0(2)
tx_port: gretap0(3)
tx_port: erspan0(4)
tx_port: lo(1)
tx_port: br100(9)
tx_port: tunl0(7)
tx_port: eth1(17)
tx_port: vxlan200(10)
tx_port: ip6tnl0(6)

ping from host11 to 192.168.11.21 ...
PING 192.168.11.21 (192.168.11.21) 56(84) bytes of data.
64 bytes from 192.168.11.21: icmp_seq=1 ttl=64 time=0.067 ms
64 bytes from 192.168.11.21: icmp_seq=2 ttl=64 time=0.110 ms
64 bytes from 192.168.11.21: icmp_seq=3 ttl=64 time=0.104 ms

--- 192.168.11.21 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2037ms
rtt min/avg/max/mdev = 0.067/0.093/0.110/0.019 ms

ping from host11 to 192.168.11.31 ...
PING 192.168.11.31 (192.168.11.31) 56(84) bytes of data.
64 bytes from 192.168.11.31: icmp_seq=1 ttl=64 time=0.075 ms
64 bytes from 192.168.11.31: icmp_seq=2 ttl=64 time=0.116 ms
64 bytes from 192.168.11.31: icmp_seq=3 ttl=64 time=0.085 ms

--- 192.168.11.31 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2048ms
rtt min/avg/max/mdev = 0.075/0.092/0.116/0.017 ms

ping from host12 to 192.168.12.22 ...
PING 192.168.12.22 (192.168.12.22) 56(84) bytes of data.
64 bytes from 192.168.12.22: icmp_seq=1 ttl=64 time=0.062 ms
64 bytes from 192.168.12.22: icmp_seq=2 ttl=64 time=0.101 ms
64 bytes from 192.168.12.22: icmp_seq=3 ttl=64 time=0.085 ms

--- 192.168.12.22 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2024ms
rtt min/avg/max/mdev = 0.062/0.082/0.101/0.016 ms

ping from host12 to 192.168.12.32 ...
PING 192.168.12.32 (192.168.12.32) 56(84) bytes of data.
64 bytes from 192.168.12.32: icmp_seq=1 ttl=64 time=0.063 ms
64 bytes from 192.168.12.32: icmp_seq=2 ttl=64 time=0.107 ms
64 bytes from 192.168.12.32: icmp_seq=3 ttl=64 time=0.082 ms

--- 192.168.12.32 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2053ms
rtt min/avg/max/mdev = 0.063/0.084/0.107/0.018 ms

show bgp summary on r1 ...

L2VPN EVPN Summary:
BGP router identifier 10.0.1.21, local AS number 65000 vrf-id 0
BGP table version 0
RIB entries 11, using 2024 bytes of memory
Peers 2, using 41 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
10.0.1.11       4      65000      11       7        0    0    0 00:00:24            8
10.0.1.12       4      65000      11       7        0    0    0 00:00:24            8

Total number of neighbors 2

show interface vxlan100 on r1 ...
Interface vxlan100 is up, line protocol is up
  Link ups:       1    last: 2021/02/03 20:13:06.25
  Link downs:     2    last: 2021/02/03 20:13:06.25
  vrf: default
  index 8 metric 0 mtu 1500 speed 4294967295 
  flags: <UP,BROADCAST,RUNNING,MULTICAST>
  Type: Unknown
  HWaddr: b2:52:3c:d2:99:25
  inet6 fe80::b052:3cff:fed2:9925/64
  Interface Type Vxlan
  VxLAN Id 100 VTEP IP: 10.0.1.21 Access VLAN Id 1

  Master interface: br100

show evpn mac vni 100 on r1 ...
Number of MACs (local and remote) known for this VNI: 4
MAC               Type   Intf/Remote VTEP      VLAN  Seq #'s
fa:cc:c1:0e:52:5d local  eth1                        0/0
ca:6a:b3:33:97:44 remote 10.0.1.23                   0/0
b2:52:3c:d2:99:25 local  br100                 1     0/0
a2:ac:31:f6:13:8a remote 10.0.1.22                   0/0

bridge fdb show br br100 on r1 ...
33:33:00:00:00:02 dev gretap0 self permanent
33:33:00:00:00:02 dev erspan0 self permanent
a2:ac:31:f6:13:8a dev vxlan100 vlan 1 extern_learn master br100 
a2:ac:31:f6:13:8a dev vxlan100 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 vlan 1 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 extern_learn master br100 
b2:52:3c:d2:99:25 dev vxlan100 vlan 1 master br100 permanent
b2:52:3c:d2:99:25 dev vxlan100 master br100 permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.22 self permanent
ca:6a:b3:33:97:44 dev vxlan100 dst 10.0.1.23 self extern_learn 
a2:ac:31:f6:13:8a dev vxlan100 dst 10.0.1.22 self extern_learn 
fa:cc:c1:0e:52:5d dev eth1 master br100 
fa:75:c2:b8:ee:c5 dev eth1 vlan 1 master br100 permanent
fa:75:c2:b8:ee:c5 dev eth1 master br100 permanent
33:33:00:00:00:01 dev eth1 self permanent
33:33:00:00:00:02 dev eth1 self permanent
01:00:5e:00:00:01 dev eth1 self permanent
33:33:ff:b8:ee:c5 dev eth1 self permanent
33:33:ff:00:00:00 dev eth1 self permanent
33:33:00:00:00:01 dev br100 self permanent
33:33:00:00:00:02 dev br100 self permanent
01:00:5e:00:00:6a dev br100 self permanent
33:33:00:00:00:6a dev br100 self permanent
01:00:5e:00:00:01 dev br100 self permanent
33:33:ff:d2:99:25 dev br100 self permanent
33:33:ff:00:00:00 dev br100 self permanent
72:26:f4:d3:c9:e8 dev eth2 master br200 
e6:9f:f0:12:99:07 dev eth2 vlan 1 master br200 permanent
e6:9f:f0:12:99:07 dev eth2 master br200 permanent
33:33:00:00:00:01 dev eth2 self permanent
33:33:00:00:00:02 dev eth2 self permanent
01:00:5e:00:00:01 dev eth2 self permanent
33:33:ff:12:99:07 dev eth2 self permanent
33:33:ff:00:00:00 dev eth2 self permanent
5a:f8:1f:0b:ce:3a dev vxlan200 vlan 1 extern_learn master br200 
5a:f8:1f:0b:ce:3a dev vxlan200 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 vlan 1 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 extern_learn master br200 
d2:b9:f8:92:55:a5 dev vxlan200 vlan 1 master br200 permanent
d2:b9:f8:92:55:a5 dev vxlan200 master br200 permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.22 self permanent
5a:f8:1f:0b:ce:3a dev vxlan200 dst 10.0.1.22 self extern_learn 
42:30:a3:d4:2b:ce dev vxlan200 dst 10.0.1.23 self extern_learn 
33:33:00:00:00:01 dev br200 self permanent
33:33:00:00:00:02 dev br200 self permanent
01:00:5e:00:00:6a dev br200 self permanent
33:33:00:00:00:6a dev br200 self permanent
01:00:5e:00:00:01 dev br200 self permanent
33:33:ff:92:55:a5 dev br200 self permanent
33:33:ff:00:00:00 dev br200 self permanent
33:33:00:00:00:01 dev eth0 self permanent
33:33:ff:00:00:21 dev eth0 self permanent
01:00:5e:00:00:01 dev eth0 self permanent
33:33:ff:00:01:15 dev eth0 self permanent
33:33:00:00:00:02 dev eth0 self permanent
33:33:ff:00:00:00 dev eth0 self permanent

show interface vxlan200 on r1 ...
Interface vxlan200 is up, line protocol is up
  Link ups:       1    last: 2021/02/03 20:13:06.27
  Link downs:     2    last: 2021/02/03 20:13:06.26
  vrf: default
  index 12 metric 0 mtu 1500 speed 4294967295 
  flags: <UP,BROADCAST,RUNNING,MULTICAST>
  Type: Unknown
  HWaddr: d2:b9:f8:92:55:a5
  inet6 fe80::d0b9:f8ff:fe92:55a5/64
  Interface Type Vxlan
  VxLAN Id 200 VTEP IP: 10.0.1.21 Access VLAN Id 1

  Master interface: br200

show evpn mac vni 200 on r1 ...
Number of MACs (local and remote) known for this VNI: 4
MAC               Type   Intf/Remote VTEP      VLAN  Seq #'s
72:26:f4:d3:c9:e8 local  eth2                        0/0
42:30:a3:d4:2b:ce remote 10.0.1.23                   0/0
d2:b9:f8:92:55:a5 local  br200                 1     0/0
5a:f8:1f:0b:ce:3a remote 10.0.1.22                   0/0

bridge fdb show br br200 on r1 ...
33:33:00:00:00:02 dev gretap0 self permanent
33:33:00:00:00:02 dev erspan0 self permanent
a2:ac:31:f6:13:8a dev vxlan100 vlan 1 extern_learn master br100 
a2:ac:31:f6:13:8a dev vxlan100 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 vlan 1 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 extern_learn master br100 
b2:52:3c:d2:99:25 dev vxlan100 vlan 1 master br100 permanent
b2:52:3c:d2:99:25 dev vxlan100 master br100 permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.22 self permanent
ca:6a:b3:33:97:44 dev vxlan100 dst 10.0.1.23 self extern_learn 
a2:ac:31:f6:13:8a dev vxlan100 dst 10.0.1.22 self extern_learn 
fa:cc:c1:0e:52:5d dev eth1 master br100 
fa:75:c2:b8:ee:c5 dev eth1 vlan 1 master br100 permanent
fa:75:c2:b8:ee:c5 dev eth1 master br100 permanent
33:33:00:00:00:01 dev eth1 self permanent
33:33:00:00:00:02 dev eth1 self permanent
01:00:5e:00:00:01 dev eth1 self permanent
33:33:ff:b8:ee:c5 dev eth1 self permanent
33:33:ff:00:00:00 dev eth1 self permanent
33:33:00:00:00:01 dev br100 self permanent
33:33:00:00:00:02 dev br100 self permanent
01:00:5e:00:00:6a dev br100 self permanent
33:33:00:00:00:6a dev br100 self permanent
01:00:5e:00:00:01 dev br100 self permanent
33:33:ff:d2:99:25 dev br100 self permanent
33:33:ff:00:00:00 dev br100 self permanent
72:26:f4:d3:c9:e8 dev eth2 master br200 
e6:9f:f0:12:99:07 dev eth2 vlan 1 master br200 permanent
e6:9f:f0:12:99:07 dev eth2 master br200 permanent
33:33:00:00:00:01 dev eth2 self permanent
33:33:00:00:00:02 dev eth2 self permanent
01:00:5e:00:00:01 dev eth2 self permanent
33:33:ff:12:99:07 dev eth2 self permanent
33:33:ff:00:00:00 dev eth2 self permanent
5a:f8:1f:0b:ce:3a dev vxlan200 vlan 1 extern_learn master br200 
5a:f8:1f:0b:ce:3a dev vxlan200 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 vlan 1 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 extern_learn master br200 
d2:b9:f8:92:55:a5 dev vxlan200 vlan 1 master br200 permanent
d2:b9:f8:92:55:a5 dev vxlan200 master br200 permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.22 self permanent
5a:f8:1f:0b:ce:3a dev vxlan200 dst 10.0.1.22 self extern_learn 
42:30:a3:d4:2b:ce dev vxlan200 dst 10.0.1.23 self extern_learn 
33:33:00:00:00:01 dev br200 self permanent
33:33:00:00:00:02 dev br200 self permanent
01:00:5e:00:00:6a dev br200 self permanent
33:33:00:00:00:6a dev br200 self permanent
01:00:5e:00:00:01 dev br200 self permanent
33:33:ff:92:55:a5 dev br200 self permanent
33:33:ff:00:00:00 dev br200 self permanent
33:33:00:00:00:01 dev eth0 self permanent
33:33:ff:00:00:21 dev eth0 self permanent
01:00:5e:00:00:01 dev eth0 self permanent
33:33:ff:00:01:15 dev eth0 self permanent
33:33:00:00:00:02 dev eth0 self permanent
33:33:ff:00:00:00 dev eth0 self permanent

show bgp summary on r2 ...

L2VPN EVPN Summary:
BGP router identifier 10.0.1.22, local AS number 65000 vrf-id 0
BGP table version 0
RIB entries 11, using 2024 bytes of memory
Peers 2, using 41 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
10.0.1.11       4      65000      11       7        0    0    0 00:00:25            8
10.0.1.12       4      65000      11       7        0    0    0 00:00:25            8

Total number of neighbors 2

show interface vxlan100 on r2 ...
Interface vxlan100 is up, line protocol is up
  Link ups:       1    last: 2021/02/03 20:13:06.25
  Link downs:     2    last: 2021/02/03 20:13:06.25
  vrf: default
  index 8 metric 0 mtu 1500 speed 4294967295 
  flags: <UP,BROADCAST,RUNNING,MULTICAST>
  Type: Unknown
  HWaddr: ce:7e:94:b9:ef:18
  inet6 fe80::cc7e:94ff:feb9:ef18/64
  Interface Type Vxlan
  VxLAN Id 100 VTEP IP: 10.0.1.22 Access VLAN Id 1

  Master interface: br100

show evpn mac vni 100 on r2 ...
Number of MACs (local and remote) known for this VNI: 4
MAC               Type   Intf/Remote VTEP      VLAN  Seq #'s
fa:cc:c1:0e:52:5d remote 10.0.1.21                   0/0
ca:6a:b3:33:97:44 remote 10.0.1.23                   0/0
ce:7e:94:b9:ef:18 local  br100                 1     0/0
a2:ac:31:f6:13:8a local  eth1                        0/0

bridge fdb show br br100 on r2 ...
33:33:00:00:00:02 dev gretap0 self permanent
33:33:00:00:00:02 dev erspan0 self permanent
fa:cc:c1:0e:52:5d dev vxlan100 vlan 1 extern_learn master br100 
fa:cc:c1:0e:52:5d dev vxlan100 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 vlan 1 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 extern_learn master br100 
ce:7e:94:b9:ef:18 dev vxlan100 vlan 1 master br100 permanent
ce:7e:94:b9:ef:18 dev vxlan100 master br100 permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.21 self permanent
ca:6a:b3:33:97:44 dev vxlan100 dst 10.0.1.23 self extern_learn 
fa:cc:c1:0e:52:5d dev vxlan100 dst 10.0.1.21 self extern_learn 
33:33:00:00:00:01 dev br100 self permanent
33:33:00:00:00:02 dev br100 self permanent
01:00:5e:00:00:6a dev br100 self permanent
33:33:00:00:00:6a dev br100 self permanent
01:00:5e:00:00:01 dev br100 self permanent
33:33:ff:b9:ef:18 dev br100 self permanent
33:33:ff:00:00:00 dev br100 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 vlan 1 extern_learn master br200 
72:26:f4:d3:c9:e8 dev vxlan200 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 vlan 1 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 extern_learn master br200 
22:ff:72:d6:47:b2 dev vxlan200 vlan 1 master br200 permanent
22:ff:72:d6:47:b2 dev vxlan200 master br200 permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.21 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 dst 10.0.1.21 self extern_learn 
42:30:a3:d4:2b:ce dev vxlan200 dst 10.0.1.23 self extern_learn 
33:33:00:00:00:01 dev br200 self permanent
33:33:00:00:00:02 dev br200 self permanent
01:00:5e:00:00:6a dev br200 self permanent
33:33:00:00:00:6a dev br200 self permanent
01:00:5e:00:00:01 dev br200 self permanent
33:33:ff:d6:47:b2 dev br200 self permanent
33:33:ff:00:00:00 dev br200 self permanent
a2:ac:31:f6:13:8a dev eth1 master br100 
ea:e5:95:69:a4:f2 dev eth1 vlan 1 master br100 permanent
ea:e5:95:69:a4:f2 dev eth1 master br100 permanent
33:33:00:00:00:01 dev eth1 self permanent
33:33:00:00:00:02 dev eth1 self permanent
01:00:5e:00:00:01 dev eth1 self permanent
33:33:ff:69:a4:f2 dev eth1 self permanent
33:33:ff:00:00:00 dev eth1 self permanent
5a:f8:1f:0b:ce:3a dev eth2 master br200 
72:6a:2b:8f:db:4e dev eth2 vlan 1 master br200 permanent
72:6a:2b:8f:db:4e dev eth2 master br200 permanent
33:33:00:00:00:01 dev eth2 self permanent
33:33:00:00:00:02 dev eth2 self permanent
01:00:5e:00:00:01 dev eth2 self permanent
33:33:ff:8f:db:4e dev eth2 self permanent
33:33:ff:00:00:00 dev eth2 self permanent
33:33:00:00:00:01 dev eth0 self permanent
33:33:ff:00:00:22 dev eth0 self permanent
01:00:5e:00:00:01 dev eth0 self permanent
33:33:ff:00:01:16 dev eth0 self permanent
33:33:00:00:00:02 dev eth0 self permanent
33:33:ff:00:00:00 dev eth0 self permanent

show interface vxlan200 on r2 ...
Interface vxlan200 is up, line protocol is up
  Link ups:       1    last: 2021/02/03 20:13:06.27
  Link downs:     2    last: 2021/02/03 20:13:06.26
  vrf: default
  index 10 metric 0 mtu 1500 speed 4294967295 
  flags: <UP,BROADCAST,RUNNING,MULTICAST>
  Type: Unknown
  HWaddr: 22:ff:72:d6:47:b2
  inet6 fe80::20ff:72ff:fed6:47b2/64
  Interface Type Vxlan
  VxLAN Id 200 VTEP IP: 10.0.1.22 Access VLAN Id 1

  Master interface: br200

show evpn mac vni 200 on r2 ...
Number of MACs (local and remote) known for this VNI: 4
MAC               Type   Intf/Remote VTEP      VLAN  Seq #'s
72:26:f4:d3:c9:e8 remote 10.0.1.21                   0/0
42:30:a3:d4:2b:ce remote 10.0.1.23                   0/0
5a:f8:1f:0b:ce:3a local  eth2                        0/0
22:ff:72:d6:47:b2 local  br200                 1     0/0

bridge fdb show br br200 on r2 ...
33:33:00:00:00:02 dev gretap0 self permanent
33:33:00:00:00:02 dev erspan0 self permanent
fa:cc:c1:0e:52:5d dev vxlan100 vlan 1 extern_learn master br100 
fa:cc:c1:0e:52:5d dev vxlan100 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 vlan 1 extern_learn master br100 
ca:6a:b3:33:97:44 dev vxlan100 extern_learn master br100 
ce:7e:94:b9:ef:18 dev vxlan100 vlan 1 master br100 permanent
ce:7e:94:b9:ef:18 dev vxlan100 master br100 permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.21 self permanent
ca:6a:b3:33:97:44 dev vxlan100 dst 10.0.1.23 self extern_learn 
fa:cc:c1:0e:52:5d dev vxlan100 dst 10.0.1.21 self extern_learn 
33:33:00:00:00:01 dev br100 self permanent
33:33:00:00:00:02 dev br100 self permanent
01:00:5e:00:00:6a dev br100 self permanent
33:33:00:00:00:6a dev br100 self permanent
01:00:5e:00:00:01 dev br100 self permanent
33:33:ff:b9:ef:18 dev br100 self permanent
33:33:ff:00:00:00 dev br100 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 vlan 1 extern_learn master br200 
72:26:f4:d3:c9:e8 dev vxlan200 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 vlan 1 extern_learn master br200 
42:30:a3:d4:2b:ce dev vxlan200 extern_learn master br200 
22:ff:72:d6:47:b2 dev vxlan200 vlan 1 master br200 permanent
22:ff:72:d6:47:b2 dev vxlan200 master br200 permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.23 self permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.21 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 dst 10.0.1.21 self extern_learn 
42:30:a3:d4:2b:ce dev vxlan200 dst 10.0.1.23 self extern_learn 
33:33:00:00:00:01 dev br200 self permanent
33:33:00:00:00:02 dev br200 self permanent
01:00:5e:00:00:6a dev br200 self permanent
33:33:00:00:00:6a dev br200 self permanent
01:00:5e:00:00:01 dev br200 self permanent
33:33:ff:d6:47:b2 dev br200 self permanent
33:33:ff:00:00:00 dev br200 self permanent
a2:ac:31:f6:13:8a dev eth1 master br100 
ea:e5:95:69:a4:f2 dev eth1 vlan 1 master br100 permanent
ea:e5:95:69:a4:f2 dev eth1 master br100 permanent
33:33:00:00:00:01 dev eth1 self permanent
33:33:00:00:00:02 dev eth1 self permanent
01:00:5e:00:00:01 dev eth1 self permanent
33:33:ff:69:a4:f2 dev eth1 self permanent
33:33:ff:00:00:00 dev eth1 self permanent
5a:f8:1f:0b:ce:3a dev eth2 master br200 
72:6a:2b:8f:db:4e dev eth2 vlan 1 master br200 permanent
72:6a:2b:8f:db:4e dev eth2 master br200 permanent
33:33:00:00:00:01 dev eth2 self permanent
33:33:00:00:00:02 dev eth2 self permanent
01:00:5e:00:00:01 dev eth2 self permanent
33:33:ff:8f:db:4e dev eth2 self permanent
33:33:ff:00:00:00 dev eth2 self permanent
33:33:00:00:00:01 dev eth0 self permanent
33:33:ff:00:00:22 dev eth0 self permanent
01:00:5e:00:00:01 dev eth0 self permanent
33:33:ff:00:01:16 dev eth0 self permanent
33:33:00:00:00:02 dev eth0 self permanent
33:33:ff:00:00:00 dev eth0 self permanent

show bgp summary on r3 ...

L2VPN EVPN Summary:
BGP router identifier 10.0.1.23, local AS number 65000 vrf-id 0
BGP table version 0
RIB entries 11, using 2024 bytes of memory
Peers 2, using 41 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
10.0.1.11       4      65000      11       7        0    0    0 00:00:22            8
10.0.1.12       4      65000      11       7        0    0    0 00:00:27            8

Total number of neighbors 2

show interface vxlan100 on r3 ...
Interface vxlan100 is up, line protocol is up
  Link ups:       1    last: 2021/02/03 20:13:06.92
  Link downs:     2    last: 2021/02/03 20:13:06.92
  vrf: default
  index 8 metric 0 mtu 1500 speed 4294967295 
  flags: <UP,BROADCAST,RUNNING,MULTICAST>
  Type: Unknown
  HWaddr: d6:cb:2e:d2:31:11
  inet6 fe80::d4cb:2eff:fed2:3111/64
  Interface Type Vxlan
  VxLAN Id 100 VTEP IP: 10.0.1.23 Access VLAN Id 1

  Master interface: br100

show evpn mac vni 100 on r3 ...
Number of MACs (local and remote) known for this VNI: 4
MAC               Type   Intf/Remote VTEP      VLAN  Seq #'s
fa:cc:c1:0e:52:5d remote 10.0.1.21                   0/0
ca:6a:b3:33:97:44 local  eth1                        0/0
d6:cb:2e:d2:31:11 local  br100                 1     0/0
a2:ac:31:f6:13:8a remote 10.0.1.22                   0/0

bridge fdb show br br100 on r3 ...
33:33:00:00:00:02 dev gretap0 self permanent
33:33:00:00:00:02 dev erspan0 self permanent
fa:cc:c1:0e:52:5d dev vxlan100 vlan 1 extern_learn master br100 
fa:cc:c1:0e:52:5d dev vxlan100 extern_learn master br100 
a2:ac:31:f6:13:8a dev vxlan100 vlan 1 extern_learn master br100 
a2:ac:31:f6:13:8a dev vxlan100 extern_learn master br100 
d6:cb:2e:d2:31:11 dev vxlan100 vlan 1 master br100 permanent
d6:cb:2e:d2:31:11 dev vxlan100 master br100 permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.22 self permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.21 self permanent
a2:ac:31:f6:13:8a dev vxlan100 dst 10.0.1.22 self extern_learn 
fa:cc:c1:0e:52:5d dev vxlan100 dst 10.0.1.21 self extern_learn 
33:33:00:00:00:01 dev br100 self permanent
33:33:00:00:00:02 dev br100 self permanent
01:00:5e:00:00:6a dev br100 self permanent
33:33:00:00:00:6a dev br100 self permanent
01:00:5e:00:00:01 dev br100 self permanent
33:33:ff:d2:31:11 dev br100 self permanent
33:33:ff:00:00:00 dev br100 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 vlan 1 extern_learn master br200 
72:26:f4:d3:c9:e8 dev vxlan200 extern_learn master br200 
5a:f8:1f:0b:ce:3a dev vxlan200 vlan 1 extern_learn master br200 
5a:f8:1f:0b:ce:3a dev vxlan200 extern_learn master br200 
b2:bc:d8:f6:70:97 dev vxlan200 vlan 1 master br200 permanent
b2:bc:d8:f6:70:97 dev vxlan200 master br200 permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.22 self permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.21 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 dst 10.0.1.21 self extern_learn 
5a:f8:1f:0b:ce:3a dev vxlan200 dst 10.0.1.22 self extern_learn 
33:33:00:00:00:01 dev br200 self permanent
33:33:00:00:00:02 dev br200 self permanent
01:00:5e:00:00:6a dev br200 self permanent
33:33:00:00:00:6a dev br200 self permanent
01:00:5e:00:00:01 dev br200 self permanent
33:33:ff:f6:70:97 dev br200 self permanent
33:33:ff:00:00:00 dev br200 self permanent
ca:6a:b3:33:97:44 dev eth1 master br100 
da:75:a1:17:9a:ae dev eth1 vlan 1 master br100 permanent
da:75:a1:17:9a:ae dev eth1 master br100 permanent
33:33:00:00:00:01 dev eth1 self permanent
33:33:00:00:00:02 dev eth1 self permanent
01:00:5e:00:00:01 dev eth1 self permanent
33:33:ff:17:9a:ae dev eth1 self permanent
33:33:ff:00:00:00 dev eth1 self permanent
42:30:a3:d4:2b:ce dev eth2 master br200 
7e:8f:f4:cf:d5:1e dev eth2 vlan 1 master br200 permanent
7e:8f:f4:cf:d5:1e dev eth2 master br200 permanent
33:33:00:00:00:01 dev eth2 self permanent
33:33:00:00:00:02 dev eth2 self permanent
01:00:5e:00:00:01 dev eth2 self permanent
33:33:ff:cf:d5:1e dev eth2 self permanent
33:33:ff:00:00:00 dev eth2 self permanent
33:33:00:00:00:01 dev eth0 self permanent
33:33:ff:00:00:23 dev eth0 self permanent
01:00:5e:00:00:01 dev eth0 self permanent
33:33:ff:00:01:17 dev eth0 self permanent
33:33:00:00:00:02 dev eth0 self permanent
33:33:ff:00:00:00 dev eth0 self permanent

show interface vxlan200 on r3 ...
Interface vxlan200 is up, line protocol is up
  Link ups:       1    last: 2021/02/03 20:13:06.93
  Link downs:     2    last: 2021/02/03 20:13:06.93
  vrf: default
  index 10 metric 0 mtu 1500 speed 4294967295 
  flags: <UP,BROADCAST,RUNNING,MULTICAST>
  Type: Unknown
  HWaddr: b2:bc:d8:f6:70:97
  inet6 fe80::b0bc:d8ff:fef6:7097/64
  Interface Type Vxlan
  VxLAN Id 200 VTEP IP: 10.0.1.23 Access VLAN Id 1

  Master interface: br200

show evpn mac vni 200 on r3 ...
Number of MACs (local and remote) known for this VNI: 4
MAC               Type   Intf/Remote VTEP      VLAN  Seq #'s
72:26:f4:d3:c9:e8 remote 10.0.1.21                   0/0
42:30:a3:d4:2b:ce local  eth2                        0/0
5a:f8:1f:0b:ce:3a remote 10.0.1.22                   0/0
7e:8f:f4:cf:d5:1e local  br200                 1     0/0

bridge fdb show br br200 on r3 ...
33:33:00:00:00:02 dev gretap0 self permanent
33:33:00:00:00:02 dev erspan0 self permanent
fa:cc:c1:0e:52:5d dev vxlan100 vlan 1 extern_learn master br100 
fa:cc:c1:0e:52:5d dev vxlan100 extern_learn master br100 
a2:ac:31:f6:13:8a dev vxlan100 vlan 1 extern_learn master br100 
a2:ac:31:f6:13:8a dev vxlan100 extern_learn master br100 
d6:cb:2e:d2:31:11 dev vxlan100 vlan 1 master br100 permanent
d6:cb:2e:d2:31:11 dev vxlan100 master br100 permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.22 self permanent
00:00:00:00:00:00 dev vxlan100 dst 10.0.1.21 self permanent
a2:ac:31:f6:13:8a dev vxlan100 dst 10.0.1.22 self extern_learn 
fa:cc:c1:0e:52:5d dev vxlan100 dst 10.0.1.21 self extern_learn 
33:33:00:00:00:01 dev br100 self permanent
33:33:00:00:00:02 dev br100 self permanent
01:00:5e:00:00:6a dev br100 self permanent
33:33:00:00:00:6a dev br100 self permanent
01:00:5e:00:00:01 dev br100 self permanent
33:33:ff:d2:31:11 dev br100 self permanent
33:33:ff:00:00:00 dev br100 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 vlan 1 extern_learn master br200 
72:26:f4:d3:c9:e8 dev vxlan200 extern_learn master br200 
5a:f8:1f:0b:ce:3a dev vxlan200 vlan 1 extern_learn master br200 
5a:f8:1f:0b:ce:3a dev vxlan200 extern_learn master br200 
b2:bc:d8:f6:70:97 dev vxlan200 vlan 1 master br200 permanent
b2:bc:d8:f6:70:97 dev vxlan200 master br200 permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.22 self permanent
00:00:00:00:00:00 dev vxlan200 dst 10.0.1.21 self permanent
72:26:f4:d3:c9:e8 dev vxlan200 dst 10.0.1.21 self extern_learn 
5a:f8:1f:0b:ce:3a dev vxlan200 dst 10.0.1.22 self extern_learn 
33:33:00:00:00:01 dev br200 self permanent
33:33:00:00:00:02 dev br200 self permanent
01:00:5e:00:00:6a dev br200 self permanent
33:33:00:00:00:6a dev br200 self permanent
01:00:5e:00:00:01 dev br200 self permanent
33:33:ff:f6:70:97 dev br200 self permanent
33:33:ff:00:00:00 dev br200 self permanent
ca:6a:b3:33:97:44 dev eth1 master br100 
da:75:a1:17:9a:ae dev eth1 vlan 1 master br100 permanent
da:75:a1:17:9a:ae dev eth1 master br100 permanent
33:33:00:00:00:01 dev eth1 self permanent
33:33:00:00:00:02 dev eth1 self permanent
01:00:5e:00:00:01 dev eth1 self permanent
33:33:ff:17:9a:ae dev eth1 self permanent
33:33:ff:00:00:00 dev eth1 self permanent
42:30:a3:d4:2b:ce dev eth2 master br200 
7e:8f:f4:cf:d5:1e dev eth2 vlan 1 master br200 permanent
7e:8f:f4:cf:d5:1e dev eth2 master br200 permanent
33:33:00:00:00:01 dev eth2 self permanent
33:33:00:00:00:02 dev eth2 self permanent
01:00:5e:00:00:01 dev eth2 self permanent
33:33:ff:cf:d5:1e dev eth2 self permanent
33:33:ff:00:00:00 dev eth2 self permanent
33:33:00:00:00:01 dev eth0 self permanent
33:33:ff:00:00:23 dev eth0 self permanent
01:00:5e:00:00:01 dev eth0 self permanent
33:33:ff:00:01:17 dev eth0 self permanent
33:33:00:00:00:02 dev eth0 self permanent
33:33:ff:00:00:00 dev eth0 self permanent

show route table bgp.evpn.0  on rr1 ...

bgp.evpn.0: 12 destinations, 12 routes (12 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

2:10.0.1.21:2::0::fa:cc:c1:0e:52:5d/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
2:10.0.1.21:3::0::72:26:f4:d3:c9:e8/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
2:10.0.1.22:2::0::a2:ac:31:f6:13:8a/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
2:10.0.1.22:3::0::5a:f8:1f:0b:ce:3a/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
2:10.0.1.23:2::0::ca:6a:b3:33:97:44/304 MAC/IP        
                   *[BGP/170] 00:00:22, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0
2:10.0.1.23:3::0::42:30:a3:d4:2b:ce/304 MAC/IP        
                   *[BGP/170] 00:00:22, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0
3:10.0.1.21:2::0::10.0.1.21/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
3:10.0.1.21:3::0::10.0.1.21/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
3:10.0.1.22:2::0::10.0.1.22/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
3:10.0.1.22:3::0::10.0.1.22/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
3:10.0.1.23:2::0::10.0.1.23/248 IM            
                   *[BGP/170] 00:00:22, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0
3:10.0.1.23:3::0::10.0.1.23/248 IM            
                   *[BGP/170] 00:00:22, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0

show route table bgp.evpn.0  on rr2 ...

bgp.evpn.0: 12 destinations, 12 routes (12 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

2:10.0.1.21:2::0::fa:cc:c1:0e:52:5d/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
2:10.0.1.21:3::0::72:26:f4:d3:c9:e8/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
2:10.0.1.22:2::0::a2:ac:31:f6:13:8a/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
2:10.0.1.22:3::0::5a:f8:1f:0b:ce:3a/304 MAC/IP        
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
2:10.0.1.23:2::0::ca:6a:b3:33:97:44/304 MAC/IP        
                   *[BGP/170] 00:00:27, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0
2:10.0.1.23:3::0::42:30:a3:d4:2b:ce/304 MAC/IP        
                   *[BGP/170] 00:00:27, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0
3:10.0.1.21:2::0::10.0.1.21/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
3:10.0.1.21:3::0::10.0.1.21/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.21 via eth0
3:10.0.1.22:2::0::10.0.1.22/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
3:10.0.1.22:3::0::10.0.1.22/248 IM            
                   *[BGP/170] 00:00:26, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.22 via eth0
3:10.0.1.23:2::0::10.0.1.23/248 IM            
                   *[BGP/170] 00:00:27, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0
3:10.0.1.23:3::0::10.0.1.23/248 IM            
                   *[BGP/170] 00:00:27, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.0.1.23 via eth0

collecting xdp stats on r1 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:407 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250285
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250317
XDP_PASS             101 pkts (         1 pps)          14 Kbytes (     0 Mbits/s) period:2.250326
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250334
XDP_REDIRECT          14 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250342

collecting xdp stats on r1 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:417 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250244
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250312
XDP_PASS              14 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250325
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250334
XDP_REDIRECT           6 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250343

collecting xdp stats on r1 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:432 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250307
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250330
XDP_PASS              14 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250343
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250357
XDP_REDIRECT           8 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250371

collecting xdp stats on r2 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:412 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250242
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250275
XDP_PASS             103 pkts (         2 pps)          15 Kbytes (     0 Mbits/s) period:2.250286
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250295
XDP_REDIRECT           7 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250304

collecting xdp stats on r2 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:422 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250282
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250321
XDP_PASS              12 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250333
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250343
XDP_REDIRECT           3 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250352

collecting xdp stats on r2 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:427 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250276
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250311
XDP_PASS              12 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250323
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250332
XDP_REDIRECT           3 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250342

collecting xdp stats on r3 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:462 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250240
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250277
XDP_PASS              89 pkts (         1 pps)          12 Kbytes (     0 Mbits/s) period:2.250288
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250298
XDP_REDIRECT           7 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250306

collecting xdp stats on r3 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:467 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250275
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250312
XDP_PASS              11 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250325
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250334
XDP_REDIRECT           3 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250344

collecting xdp stats on r3 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:472 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250276
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250329
XDP_PASS               9 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250342
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250353
XDP_REDIRECT           4 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250364


success in 60 seconds
```

Remove containers:

```
$ make down
/remove_xdp.sh || true
removing xdp on eth0 @ r1 ...
INFO: xdp_link_detach() removed XDP prog ID:671 on ifindex:64
removing xdp on eth1 @ r1 ...
INFO: xdp_link_detach() removed XDP prog ID:687 on ifindex:9
removing xdp on eth2 @ r1 ...
INFO: xdp_link_detach() removed XDP prog ID:704 on ifindex:11
removing xdp on eth0 @ r2 ...
INFO: xdp_link_detach() removed XDP prog ID:672 on ifindex:62
removing xdp on eth1 @ r2 ...
INFO: xdp_link_detach() removed XDP prog ID:688 on ifindex:13
removing xdp on eth2 @ r2 ...
INFO: xdp_link_detach() removed XDP prog ID:703 on ifindex:15
removing xdp on eth0 @ r3 ...
INFO: xdp_link_detach() removed XDP prog ID:756 on ifindex:58
removing xdp on eth1 @ r3 ...
INFO: xdp_link_detach() removed XDP prog ID:764 on ifindex:17
removing xdp on eth2 @ r3 ...
INFO: xdp_link_detach() removed XDP prog ID:772 on ifindex:19
docker-compose down
Stopping rr2    ... done
Stopping r1     ... done
Stopping host21 ... done
Stopping rr1    ... done
Stopping r3     ... done
Stopping host31 ... done
Stopping host11 ... done
Stopping host32 ... done
Stopping host22 ... done
Stopping r2     ... done
Stopping host12 ... done
Removing vxlan_vethpass_1 ... done
Removing rr2              ... done
Removing r1               ... done
Removing host21           ... done
Removing vxlan_links_1    ... done
Removing rr1              ... done
Removing r3               ... done
Removing host31           ... done
Removing host11           ... done
Removing host32           ... done
Removing host22           ... done
Removing r2               ... done
Removing host12           ... done
Removing network vxlan_wan1
sudo chown -R mwiget r*
```
