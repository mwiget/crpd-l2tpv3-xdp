#!/bin/bash

#set -e

echo "setting loopback ip addresses ..."
ip addr add 10.0.0.23/32 dev lo
ip -6 addr add fd00::23/128 dev lo

until ip link show eth1 up; do
  echo "waiting for eth1 up ..."
  sleep 1
done

for vni in 100 200; do
  # create VXLAN interface
  ip link add vxlan${vni} type vxlan id ${vni} dstport 4789 local 10.0.1.23 nolearning
  # create companion bridge
  brctl addbr br${vni}
  brctl stp br${bni} off
  brctl addif br${vni} vxlan${vni}
  ip link set up dev br${vni}
  ip link set up dev vxlan${vni}
done

# attach interfaces to bridge
brctl addif br100 eth1
brctl addif br200 eth2

ulimit -l 2048
xdp_loader -d eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_vxlan
xdp_loader -d eth1 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_vxlan
xdp_loader -d eth2 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_vxlan
