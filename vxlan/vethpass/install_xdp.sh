#!/bin/bash

while true; do
  echo -n "waiting for 5 veth interfaces to be up ..."
  ifconfig |grep veth|wc -l | grep ^5$ >/dev/null && break
  echo -n "."
  sleep 1
done
echo ""

echo "installing xdp_pass on all veth interfaces ..."
for interface in $(ifconfig |grep ^veth | cut -d: -f1); do
  until ip link show $interface | grep LOWER_UP; do
    echo "waiting for $interface to be up ..."
    sleep 1
  done
  ulimit -l 1024 && xdp_loader -d $interface --auto-mode --force --filename /root/xdp_router.o --progsec xdp_pass
done

echo ""
ip link |grep ' xdp '
