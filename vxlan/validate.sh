#!/bin/bash

SECONDS=0

echo -n "waiting for BGP sessions on rr1 from all VTEPs are up ..."
while true; do
  docker exec rr1 cli show bgp summary | grep Establ | wc -l | grep 3 && break
  echo -n "."
  sleep 1
done

echo -n "waiting for BGP sessions on rr2 from all VTEPs are up ..."
while true; do
  docker exec rr2 cli show bgp summary | grep Establ | wc -l | grep 3 && break
  echo -n "."
  sleep 1
done

for ip in 192.168.11.21 192.168.11.31; do
  echo ""
  echo "ping from host11 to $ip ..."
  while true; do
    docker exec -ti host11 ping -c 3 $ip  && break
    docker exec -ti host11 ping -f -c 100 $ip  && break
    echo -n "."
    sleep 1
  done
done

for ip in 192.168.12.22 192.168.12.32; do
  echo ""
  echo "ping from host12 to $ip ..."
  while true; do
    docker exec -ti host12 ping -c 3 $ip  && break
    docker exec -ti host12 ping -f -c 100 $ip  && break
    echo -n "."
    sleep 1
  done
done

echo ""
for router in r1 r2 r3; do
  for interface in eth0 eth1 eth2; do
    echo "updating xdp fdb on $router interface $interface ..."
    if ! docker exec -ti $router /root/update_xdp_vxlan_fdb.sh $interface ; then
      echo -n ""
    fi
  done
done

for ip in 192.168.11.21 192.168.11.31; do
  echo ""
  echo "ping from host11 to $ip ..."
  while true; do
    docker exec -ti host11 ping -c 3 $ip  && break
    docker exec -ti host11 ping -f -c 100 $ip  && break
    echo -n "."
    sleep 1
  done
done

for ip in 192.168.12.22 192.168.12.32; do
  echo ""
  echo "ping from host12 to $ip ..."
  while true; do
    docker exec -ti host12 ping -c 3 $ip  && break
    docker exec -ti host12 ping -f -c 100 $ip  && break
    echo -n "."
    sleep 1
  done
done

for router in r1 r2 r3; do
  echo ""
  echo "show bgp summary on $router ..."
  docker exec $router vtysh -c "show bgp summary"
  for vni in 100 200; do
    echo ""
    echo "show interface vxlan${vni} on $router ..."
    docker exec $router vtysh -c "show interface vxlan${vni}"
    echo ""
    echo "show evpn mac vni ${vni} on $router ..."
    docker exec $router vtysh -c "show evpn mac vni ${vni}"
    echo ""
    echo "bridge fdb show br br${vni} on $router ..."
    docker exec $router bridge fdb show br${vni}
  done
done

for router in rr1 rr2; do
  echo ""
  echo "show route table bgp.evpn.0  on $router ..."
  docker exec $router cli show route table bgp.evpn.0
done

echo ""
for router in r1 r2 r3; do
  for interface in eth0 eth1 eth2; do
    echo "collecting xdp stats on $router ..."
    if ! docker exec -ti $router xdp_stats -s -d $interface ; then
      echo -n ""
    fi
  done
done

echo ""
echo "success in $SECONDS seconds"
