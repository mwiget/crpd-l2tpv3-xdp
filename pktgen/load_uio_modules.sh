#!/bin/bash
mkdir -p dpdk
docker run --rm -v $PWD/dpdk:/u --user $(id -u):$(id -g) pktgen /bin/cp -r /dpdk/. /u
ls -l dpdk

sudo modprobe uio
sudo insmod dpdk/igb_uio.ko
sudo modprobe vfio-pci
sudo modprobe uio_pci_generic
