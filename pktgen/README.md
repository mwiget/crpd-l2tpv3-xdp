To run pktgen, install liblua5.3-0

```
$ sudo apt install liblua5.3-0
```

vf_bus_error_fix.diff contains contains patch from https://patchwork.dpdk.org/patch/71962/
to fix bus error in pci_vfio when launching pktgen on VF

Likely fixed in newer dpdk versions, but I just needed this fix for now.

