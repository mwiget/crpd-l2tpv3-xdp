#!/bin/bash

PCI="0000:04:00.0"    # ens6f0 on xeon

sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI
sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI
sudo python3 dpdk/usertools/dpdk-devbind.py -s
