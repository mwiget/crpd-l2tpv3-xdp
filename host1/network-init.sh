#!/bin/bash

set -e
mount -t bpf bpf /sys/fs/bpf/

until ip link show eth0 up; do
  echo "waiting for eth0 up ..."
  sleep 1
done

ip addr add 192.168.1.2/24 dev eth0
ip -6 addr add fd00:11::2/64 dev eth0

ip link set up eth0
ip route add default via 192.168.1.1
ip -6 route add default via fd00:11::1

echo ""
echo "installing xdp_l2tpv3 on eth0 ..."
ulimit -l 2048
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0

tail -f /dev/null
