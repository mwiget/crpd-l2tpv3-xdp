docker-compose build
Step 1/5 : FROM alpine:latest
 ---> a24bb4013296
Step 2/5 : RUN apk add --no-cache docker iproute2
 ---> Using cache
 ---> 304d10379ff5
Step 3/5 : COPY add_link.sh /
 ---> Using cache
 ---> fe715024d379
Step 4/5 : RUN chmod a+rx /add_link.sh
 ---> Using cache
 ---> efa811641833
Step 5/5 : ENTRYPOINT ["/bin/ash", "/add_link.sh"]
 ---> Using cache
 ---> 738073e82b7f

Successfully built 738073e82b7f
Successfully tagged crpd-l2tpv3-xdp_link1:latest
Step 1/5 : FROM alpine:latest
 ---> a24bb4013296
Step 2/5 : RUN apk add --no-cache docker iproute2
 ---> Using cache
 ---> 304d10379ff5
Step 3/5 : COPY add_link.sh /
 ---> Using cache
 ---> fe715024d379
Step 4/5 : RUN chmod a+rx /add_link.sh
 ---> Using cache
 ---> efa811641833
Step 5/5 : ENTRYPOINT ["/bin/ash", "/add_link.sh"]
 ---> Using cache
 ---> 738073e82b7f

Successfully built 738073e82b7f
Successfully tagged crpd-l2tpv3-xdp_link2:latest
Step 1/5 : FROM alpine:latest
 ---> a24bb4013296
Step 2/5 : RUN apk add --no-cache docker iproute2
 ---> Using cache
 ---> 304d10379ff5
Step 3/5 : COPY add_link.sh /
 ---> Using cache
 ---> fe715024d379
Step 4/5 : RUN chmod a+rx /add_link.sh
 ---> Using cache
 ---> efa811641833
Step 5/5 : ENTRYPOINT ["/bin/ash", "/add_link.sh"]
 ---> Using cache
 ---> 738073e82b7f

Successfully built 738073e82b7f
Successfully tagged crpd-l2tpv3-xdp_link3:latest
Step 1/5 : FROM alpine:latest
 ---> a24bb4013296
Step 2/5 : RUN apk add --no-cache docker iproute2
 ---> Using cache
 ---> 304d10379ff5
Step 3/5 : COPY add_link.sh /
 ---> Using cache
 ---> fe715024d379
Step 4/5 : RUN chmod a+rx /add_link.sh
 ---> Using cache
 ---> efa811641833
Step 5/5 : ENTRYPOINT ["/bin/ash", "/add_link.sh"]
 ---> Using cache
 ---> 738073e82b7f

Successfully built 738073e82b7f
Successfully tagged crpd-l2tpv3-xdp_link4:latest
Step 1/5 : FROM alpine:latest
 ---> a24bb4013296
Step 2/5 : RUN apk add --no-cache docker iproute2
 ---> Using cache
 ---> 304d10379ff5
Step 3/5 : COPY add_link.sh /
 ---> Using cache
 ---> fe715024d379
Step 4/5 : RUN chmod a+rx /add_link.sh
 ---> Using cache
 ---> efa811641833
Step 5/5 : ENTRYPOINT ["/bin/ash", "/add_link.sh"]
 ---> Using cache
 ---> 738073e82b7f

Successfully built 738073e82b7f
Successfully tagged crpd-l2tpv3-xdp_link5:latest
docker-compose up -d
./validate-mac.sh

Host OS version:  Darwin mwjp.fritz.box 19.6.0 Darwin Kernel Version 19.6.0: Thu Oct 29 22:56:45 PDT 2020; root:xnu-6153.141.2.2~1/RELEASE_X86_64 x86_64
linuxkit version: Linux spine1 5.4.39-linuxkit #1 SMP Fri May 8 23:03:06 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux

waiting for all ISIS adjacencies to come up ...........................       3

show isis adj on spine1 ...
Interface             System         L State        Hold (secs) SNPA
eth0                  r1             2  Up                    6
eth1                  r2             2  Up                    7
eth2                  r3             2  Up                    7

waiting for all BGP peers to come up ...       3

show bgp summary on spine1 ...
Threading mode: BGP I/O
Default eBGP mode: advertise - accept, receive - accept
Groups: 2 Peers: 6 Down peers: 3
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       0          0          0          0          0          0
inet6.0              
                      33          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
192.168.101.2    4259905011          0          0       0       0          43 Idle  
192.168.102.2    4259905012          0          0       0       0          43 Idle  
192.168.103.2    4259905013          0          0       0       0          43 Idle  
fd00::11         4259905011          5          4       0       0           5 Establ
  inet6.0: 0/11/11/0
fd00::12         4259905012          6          4       0       0           2 Establ
  inet6.0: 0/9/9/0
fd00::13         4259905013          9          7       0       0          32 Establ
  inet6.0: 0/13/13/0

ping r2 from r1 ...
PING fd00::12(fd00::12) 56 data bytes
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- fd00::12 ping statistics ---
100 packets transmitted, 100 received, 0% packet loss, time 34ms
rtt min/avg/max/mdev = 0.011/0.064/0.388/0.070 ms, ipg/ewma 0.352/0.035 ms

ping r3 from r1 ...
PING fd00::13(fd00::13) 56 data bytes
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- fd00::13 ping statistics ---
100 packets transmitted, 100 received, 0% packet loss, time 26ms
rtt min/avg/max/mdev = 0.011/0.088/1.089/0.146 ms, ipg/ewma 0.271/0.049 ms

ping r1 from r2 ...
PING fd00::11(fd00::11) 56 data bytes
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- fd00::11 ping statistics ---
100 packets transmitted, 100 received, 0% packet loss, time 25ms
rtt min/avg/max/mdev = 0.012/0.079/0.706/0.112 ms, ipg/ewma 0.257/0.105 ms

ping r3 from r2 ...
PING fd00::13(fd00::13) 56 data bytes
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- fd00::13 ping statistics ---
100 packets transmitted, 100 received, 0% packet loss, time 30ms
rtt min/avg/max/mdev = 0.011/0.100/0.916/0.165 ms, ipg/ewma 0.307/0.077 ms

ping r1 from r3 ...
PING fd00::11(fd00::11) 56 data bytes
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- fd00::11 ping statistics ---
100 packets transmitted, 100 received, 0% packet loss, time 16ms
rtt min/avg/max/mdev = 0.012/0.054/0.329/0.057 ms, ipg/ewma 0.171/0.025 ms

ping r2 from r3 ...
PING fd00::12(fd00::12) 56 data bytes
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- fd00::12 ping statistics ---
100 packets transmitted, 100 received, 0% packet loss, time 24ms
rtt min/avg/max/mdev = 0.012/0.066/0.300/0.059 ms, ipg/ewma 0.247/0.042 ms

collecting xdp stats on spine1 eth0 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:238 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250360
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250436
XDP_PASS              17 pkts (         2 pps)           2 Kbytes (     0 Mbits/s) period:2.250440
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250443
XDP_REDIRECT         400 pkts (         0 pps)          47 Kbytes (     0 Mbits/s) period:2.250448

collecting xdp stats on spine1 eth1 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:242 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250402
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250487
XDP_PASS              19 pkts (         0 pps)           2 Kbytes (     0 Mbits/s) period:2.250426
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250416
XDP_REDIRECT         400 pkts (         0 pps)          47 Kbytes (     0 Mbits/s) period:2.250409

collecting xdp stats on spine1 eth2 ...

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:246 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250784
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250744
XDP_PASS              13 pkts (         1 pps)           1 Kbytes (     0 Mbits/s) period:2.250727
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250643
XDP_REDIRECT         400 pkts (         0 pps)          47 Kbytes (     0 Mbits/s) period:2.250731


show isis adj on spine1 ...
Interface             System         L State        Hold (secs) SNPA
eth0                  r1             2  Up                    6
eth1                  r2             2  Up                    7
eth2                  r3             2  Up                    7

show bgp summary on spine1 ...
Threading mode: BGP I/O
Default eBGP mode: advertise - accept, receive - accept
Groups: 2 Peers: 6 Down peers: 3
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       0          0          0          0          0          0
inet6.0              
                      39          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
192.168.101.2    4259905011          0          0       0       0          53 Idle  
192.168.102.2    4259905012          0          0       0       0          53 Idle  
192.168.103.2    4259905013          0          0       0       0          53 Idle  
fd00::11         4259905011          6          4       0       0          15 Establ
  inet6.0: 0/13/13/0
fd00::12         4259905012          7          4       0       0          12 Establ
  inet6.0: 0/13/13/0
fd00::13         4259905013          9          7       0       0          42 Establ
  inet6.0: 0/13/13/0

show ip route hidden extensive on spine1 ...

inet.0: 5 destinations, 5 routes (5 active, 0 holddown, 0 hidden)

iso.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)

inet6.0: 28 destinations, 70 routes (28 active, 0 holddown, 0 hidden)

Host OS version:  Darwin mwjp.fritz.box 19.6.0 Darwin Kernel Version 19.6.0: Thu Oct 29 22:56:45 PDT 2020; root:xnu-6153.141.2.2~1/RELEASE_X86_64 x86_64
linuxkit version: Linux spine1 5.4.39-linuxkit #1 SMP Fri May 8 23:03:06 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux

operational in 54 seconds
validation completed in 11 seconds
