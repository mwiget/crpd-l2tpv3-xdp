#!/bin/bash
nodes=$(kubectl get pod \
  --field-selector=status.phase=Running \
  -o custom-columns=name:metadata.name --no-headers )

for node in $nodes; do
  echo $node:
  kubectl exec $node -- $@
  echo ""
done
