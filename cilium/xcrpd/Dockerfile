FROM xdpbuild as xdp
FROM crpd:20.3R1.8
RUN apt-get update \
  && apt-get -y --no-install-recommends install bwm-ng ethtool tshark tcpdump curl jq \
  && rm -rf /var/lib/apt/lists/*

COPY wireshark /root/.config/wireshark
COPY tshark.sh /root/tshark.sh

COPY --from=xdp /xdp_stats /xdp_tunnels /xdp_loader /sbin/
COPY --from=xdp /tunnels.sh /xdp_router.o /root/

# augment the container with sshd config for ssh and netconf and add
# our ssh public key for access it at runtime

COPY sshd_config /etc/ssh
COPY mycrpd.key.pub /root/.ssh/authorized_keys

# augment the startup script to execute our own script. Typically, such
# script is mounted at runtime, but here everything gets packaged into
# the container

COPY runit-init.sh /sbin/
COPY network-init.sh /config/
RUN chmod +x /sbin/runit-init.sh /config/network-init.sh \
  && chmod 600 /root/.ssh/authorized_keys

# copy cRPD license key into its required location. Similar to the networking
# script, the folder /config is typically mounted to provide the key and junos
# config at runtime

COPY license /config/license/

COPY juniper.conf /config/

# patching rpd-helper not to set rp_filter (undo PR-1465182)
RUN cp /usr/sbin/rpd-helper /usr/sbin/rpd-helper.PR-1465182 \
  && grep -v rp_filter /usr/sbin/rpd-helper.PR-1465182 > /usr/sbin/rpd-helper

WORKDIR /root
# SIGRTMIN+3  ## using this leads to zombies in SP//, better use 'STOPSIGNAL 35'
STOPSIGNAL 35
