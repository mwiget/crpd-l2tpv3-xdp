#!/bin/bash

#set -e

# TODO change hostname in juniper.conf with the DNS name of our instance

# find IFD's, exclude SRIOV VF interfaces (have 'v' in their names)
XDPINT=$(ls /sys/class/net/ | grep '^enp\|^ens' | grep -v v)
for int in $XDPINT; do
  echo $INT
  ethtool -G $int rx 4096 || true
  ethtool -L $int combined 3 || true
  ulimit -l unlimited
  /sbin/xdp_loader --dev $int --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
  echo "checking xdp ..."
  ip link show dev $int
done

for int in $XDPINT; do
  echo $int ...
  /sbin/xdp_tunnels --dev $int < /dev/null
done

arp -s 192.168.40.49 40:a6:b7:08:83:90 || true
arp -s 192.168.41.49 40:a6:b7:08:83:91 || true
