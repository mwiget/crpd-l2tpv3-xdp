#!/bin/bash
VFIO="0000:65:02.0 0000:65:0a.0"
#VFIO="0000:65:02.0 0000:65:02.1 0000:65:0a.0 0000:65:0a.1"

for PCI in $VFIO; do
  echo $PCI ...
  echo $PCI | sudo tee "/sys/bus/pci/devices/$PCI/driver/unbind" 2>/dev/null || true
  readlink /sys/bus/pci/devices/$PCI/iommu_group
  sudo modprobe vfio-pci
  lspci -n -s $PCI | awk '{print $3}' | tr ':' ' ' | sudo tee /sys/bus/pci/drivers/vfio-pci/new_id
done

ls -l /dev/vfio/
