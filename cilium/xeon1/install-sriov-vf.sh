#!/bin/bash
sudo cp sriov-NIC.service /etc/systemd/system/
sudo cp sriov-dpdk.sh /usr/local/sbin/
sudo systemctl enable sriov-NIC
sudo systemctl start sriov-NIC
sudo systemctl status sriov-NIC

echo ""
lspci |grep "Virtual Function"

echo ""
echo "build and install SR-IOV CNI ..."
cd ../sriov-cni
make
sudo cp build/sriov /opt/cni/bin/
