# xeon1

```
$ lspci|grep X722
b7:00.0 Ethernet controller: Intel Corporation Ethernet Connection X722 for 10GBASE-T (rev 04)
b7:00.1 Ethernet controller: Intel Corporation Ethernet Connection X722 for 10GBASE-T (rev 04)
b7:00.2 Ethernet controller: Intel Corporation Ethernet Connection X722 for 10GbE SFP+ (rev 04)
b7:00.3 Ethernet controller: Intel Corporation Ethernet Connection X722 for 10GbE SFP+ (rev 04)
```


```
$ ethtool -i eno5
driver: i40e
version: 2.8.20-k
firmware-version: 3.33 0x80001006 1.1747.0
expansion-rom-version: 
bus-info: 0000:b7:00.0
supports-statistics: yes
supports-test: yes
supports-eeprom-access: yes
supports-register-dump: yes
supports-priv-flags: yes
mwiget@xeon1:~/git/crpd-l2tpv3-xdp/cilium/xeon1$ ethtool -i eno6
driver: i40e
version: 2.8.20-k
firmware-version: 3.33 0x80001006 1.1747.0
expansion-rom-version: 
bus-info: 0000:b7:00.1
supports-statistics: yes
supports-test: yes
supports-eeprom-access: yes
supports-register-dump: yes
supports-priv-flags: yes
mwiget@xeon1:~/git/crpd-l2tpv3-xdp/cilium/xeon1$ ethtool -i eno7
driver: i40e
version: 2.8.20-k
firmware-version: 3.33 0x80001006 1.1747.0
expansion-rom-version: 
bus-info: 0000:b7:00.2
supports-statistics: yes
supports-test: yes
supports-eeprom-access: yes
supports-register-dump: yes
supports-priv-flags: yes
mwiget@xeon1:~/git/crpd-l2tpv3-xdp/cilium/xeon1$ ethtool -i eno8
driver: i40e
version: 2.8.20-k
firmware-version: 3.33 0x80001006 1.1747.0
expansion-rom-version: 
bus-info: 0000:b7:00.3
supports-statistics: yes
supports-test: yes
supports-eeprom-access: yes
supports-register-dump: yes
supports-priv-flags: yes
```

```
$ lspci -n |grep b7:0
b7:00.0 0200: 8086:37d2 (rev 04)
b7:00.1 0200: 8086:37d2 (rev 04)
b7:00.2 0200: 8086:37d0 (rev 04)
b7:00.3 0200: 8086:37d0 (rev 04)
b7:02.0 0200: 8086:37cd (rev 04)
b7:02.1 0200: 8086:37cd (rev 04)
b7:02.2 0200: 8086:37cd (rev 04)
b7:02.3 0200: 8086:37cd (rev 04)
b7:02.4 0200: 8086:37cd (rev 04)
b7:02.5 0200: 8086:37cd (rev 04)
b7:02.6 0200: 8086:37cd (rev 04)
b7:02.7 0200: 8086:37cd (rev 04)
b7:06.0 0200: 8086:37cd (rev 04)
b7:06.1 0200: 8086:37cd (rev 04)
b7:06.2 0200: 8086:37cd (rev 04)
b7:06.3 0200: 8086:37cd (rev 04)
b7:06.4 0200: 8086:37cd (rev 04)
b7:06.5 0200: 8086:37cd (rev 04)
b7:06.6 0200: 8086:37cd (rev 04)
b7:06.7 0200: 8086:37cd (rev 04)
b7:0a.0 0200: 8086:37cd (rev 04)
b7:0a.1 0200: 8086:37cd (rev 04)
b7:0a.2 0200: 8086:37cd (rev 04)
b7:0a.3 0200: 8086:37cd (rev 04)
b7:0a.4 0200: 8086:37cd (rev 04)
b7:0a.5 0200: 8086:37cd (rev 04)
b7:0a.6 0200: 8086:37cd (rev 04)
b7:0a.7 0200: 8086:37cd (rev 04)
b7:0e.0 0200: 8086:37cd (rev 04)
b7:0e.1 0200: 8086:37cd (rev 04)
b7:0e.2 0200: 8086:37cd (rev 04)
b7:0e.3 0200: 8086:37cd (rev 04)
b7:0e.4 0200: 8086:37cd (rev 04)
b7:0e.5 0200: 8086:37cd (rev 04)
b7:0e.6 0200: 8086:37cd (rev 04)
b7:0e.7 0200: 8086:37cd (rev 04)
```

Example driver for a vf interface:

```
$ ethtool -i eno8v6
driver: iavf
version: 3.2.3-k
firmware-version: N/A
expansion-rom-version: 
bus-info: 0000:b7:0e.6
supports-statistics: yes
supports-test: no
supports-eeprom-access: no
supports-register-dump: no
supports-priv-flags: yes
```

Dual 40GE interface NIC:

```
$ lspci|grep XL710
65:00.0 Ethernet controller: Intel Corporation Ethernet Controller XL710 for 40GbE QSFP+ (rev 02)
65:00.1 Ethernet controller: Intel Corporation Ethernet Controller XL710 for 40GbE QSFP+ (rev 02)
```

```
$ ethtool -i enp101s0f0
driver: i40e
version: 2.8.20-k
firmware-version: 6.01 0x800035da 1.1747.0
expansion-rom-version: 
bus-info: 0000:65:00.0
supports-statistics: yes
supports-test: yes
supports-eeprom-access: yes
supports-register-dump: yes
supports-priv-flags: yes
mwiget@xeon1:~$ ethtool -i enp101s0f1
driver: i40e
version: 2.8.20-k
firmware-version: 6.01 0x800035da 1.1747.0
expansion-rom-version: 
bus-info: 0000:65:00.1
supports-statistics: yes
supports-test: yes
supports-eeprom-access: yes
supports-register-dump: yes
supports-priv-flags: yes
```

```
$ lspci -n |grep 65:00
65:00.0 0200: 8086:1583 (rev 02)
65:00.1 0200: 8086:1583 (rev 02)
```

Installing sriov-NIC.service:

```
$ ./install-sriov-vf.sh
```

enable vfio manually on the 2 40Ge ports:

```
$ ./sriov-dpdk.sh 
0000:65:00.0 ...
0000:65:00.0
../../../../kernel/iommu_groups/67
8086 1583
0000:65:00.1 ...
0000:65:00.1
../../../../kernel/iommu_groups/68
8086 1583
total 0
crw------- 1 root root 243,   0 Oct  7 13:52 67
crw------- 1 root root 243,   1 Oct  7 13:52 68
crw-rw-rw- 1 root root  10, 196 Oct  7 13:21 vfio
```

The success can be seen by the output of 'ls -l /dev/vfio/' above.


```
$ dpdk/usertools/dpdk-devbind.py -s

Network devices using DPDK-compatible driver
============================================
0000:65:00.0 'Ethernet Controller XL710 for 40GbE QSFP+ 1583' drv=vfio-pci unused=i40e
0000:65:00.1 'Ethernet Controller XL710 for 40GbE QSFP+ 1583' drv=vfio-pci unused=i40e

Network devices using kernel driver
===================================
0000:67:00.0 'I350 Gigabit Network Connection 1521' if=eno1 drv=igb unused=vfio-pci *Active*
0000:67:00.1 'I350 Gigabit Network Connection 1521' if=eno2 drv=igb unused=vfio-pci 
0000:67:00.2 'I350 Gigabit Network Connection 1521' if=eno3 drv=igb unused=vfio-pci 
0000:67:00.3 'I350 Gigabit Network Connection 1521' if=eno4 drv=igb unused=vfio-pci 
0000:b7:00.0 'Ethernet Connection X722 for 10GBASE-T 37d2' if=eno5 drv=i40e unused=vfio-pci 
0000:b7:00.1 'Ethernet Connection X722 for 10GBASE-T 37d2' if=eno6 drv=i40e unused=vfio-pci 
0000:b7:00.2 'Ethernet Connection X722 for 10GbE SFP+ 37d0' if=eno7 drv=i40e unused=vfio-pci *Active*
0000:b7:00.3 'Ethernet Connection X722 for 10GbE SFP+ 37d0' if=eno8 drv=i40e unused=vfio-pci *Active*
0000:b7:02.0 'Ethernet Virtual Function 700 Series 37cd' if=eno5v0 drv=iavf unused=vfio-pci 
0000:b7:02.1 'Ethernet Virtual Function 700 Series 37cd' if=eno5v1 drv=iavf unused=vfio-pci 
0000:b7:02.2 'Ethernet Virtual Function 700 Series 37cd' if=eno5v2 drv=iavf unused=vfio-pci 
0000:b7:02.3 'Ethernet Virtual Function 700 Series 37cd' if=eno5v3 drv=iavf unused=vfio-pci 
0000:b7:02.4 'Ethernet Virtual Function 700 Series 37cd' if=eno5v4 drv=iavf unused=vfio-pci 
0000:b7:02.5 'Ethernet Virtual Function 700 Series 37cd' if=eno5v5 drv=iavf unused=vfio-pci 
0000:b7:02.6 'Ethernet Virtual Function 700 Series 37cd' if=eno5v6 drv=iavf unused=vfio-pci 
0000:b7:02.7 'Ethernet Virtual Function 700 Series 37cd' if=eno5v7 drv=iavf unused=vfio-pci 
0000:b7:06.0 'Ethernet Virtual Function 700 Series 37cd' if=eno6v0 drv=iavf unused=vfio-pci 
0000:b7:06.1 'Ethernet Virtual Function 700 Series 37cd' if=eno6v1 drv=iavf unused=vfio-pci 
0000:b7:06.2 'Ethernet Virtual Function 700 Series 37cd' if=eno6v2 drv=iavf unused=vfio-pci 
0000:b7:06.3 'Ethernet Virtual Function 700 Series 37cd' if=eno6v3 drv=iavf unused=vfio-pci 
0000:b7:06.4 'Ethernet Virtual Function 700 Series 37cd' if=eno6v4 drv=iavf unused=vfio-pci 
0000:b7:06.5 'Ethernet Virtual Function 700 Series 37cd' if=eno6v5 drv=iavf unused=vfio-pci 
0000:b7:06.6 'Ethernet Virtual Function 700 Series 37cd' if=eno6v6 drv=iavf unused=vfio-pci 
0000:b7:06.7 'Ethernet Virtual Function 700 Series 37cd' if=eno6v7 drv=iavf unused=vfio-pci 
0000:b7:0a.0 'Ethernet Virtual Function 700 Series 37cd' if=eno7v0 drv=iavf unused=vfio-pci 
0000:b7:0a.1 'Ethernet Virtual Function 700 Series 37cd' if=eno7v1 drv=iavf unused=vfio-pci 
0000:b7:0a.2 'Ethernet Virtual Function 700 Series 37cd' if=eno7v2 drv=iavf unused=vfio-pci 
0000:b7:0a.3 'Ethernet Virtual Function 700 Series 37cd' if=eno7v3 drv=iavf unused=vfio-pci 
0000:b7:0a.4 'Ethernet Virtual Function 700 Series 37cd' if=eno7v4 drv=iavf unused=vfio-pci 
0000:b7:0a.5 'Ethernet Virtual Function 700 Series 37cd' if=eno7v5 drv=iavf unused=vfio-pci 
0000:b7:0a.6 'Ethernet Virtual Function 700 Series 37cd' if=eno7v6 drv=iavf unused=vfio-pci 
0000:b7:0a.7 'Ethernet Virtual Function 700 Series 37cd' if=eno7v7 drv=iavf unused=vfio-pci 
0000:b7:0e.0 'Ethernet Virtual Function 700 Series 37cd' if=eno8v0 drv=iavf unused=vfio-pci 
0000:b7:0e.1 'Ethernet Virtual Function 700 Series 37cd' if=eno8v1 drv=iavf unused=vfio-pci 
0000:b7:0e.2 'Ethernet Virtual Function 700 Series 37cd' if=eno8v2 drv=iavf unused=vfio-pci 
0000:b7:0e.3 'Ethernet Virtual Function 700 Series 37cd' if=eno8v3 drv=iavf unused=vfio-pci 
0000:b7:0e.4 'Ethernet Virtual Function 700 Series 37cd' if=eno8v4 drv=iavf unused=vfio-pci 
0000:b7:0e.5 'Ethernet Virtual Function 700 Series 37cd' if=eno8v5 drv=iavf unused=vfio-pci 
0000:b7:0e.6 'Ethernet Virtual Function 700 Series 37cd' if=eno8v6 drv=iavf unused=vfio-pci 
0000:b7:0e.7 'Ethernet Virtual Function 700 Series 37cd' if=eno8v7 drv=iavf unused=vfio-pci 

No 'Baseband' devices detected
==============================

Crypto devices using kernel driver
==================================
0000:b5:00.0 'C62x Chipset QuickAssist Technology 37c8' if= drv=c6xx unused=qat_c62x,vfio-pci 
0000:b6:00.0 'C62x Chipset QuickAssist Technology 37c8' if= drv=c6xx unused=qat_c62x,vfio-pci 

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================

Misc (rawdev) devices using kernel driver
=========================================
0000:00:04.0 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.1 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.2 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.3 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.4 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.5 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.6 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
0000:00:04.7 'Sky Lake-E CBDMA Registers 2021' if= drv=ioatdma unused=vfio-pci 
```


```
$ sudo cat /var/lib/kubelet/device-plugins/kubelet_internal_checkpoint |jq

{
  "Data": {
    "PodDeviceEntries": [
      {
        "PodUID": "72e3ac27-ffe9-4e3d-b758-98403b43622e",
        "ContainerName": "dpdk-10g",
        "ResourceName": "intel.com/intel_sriov_dpdk",
        "DeviceIDs": [
          "0000:65:00.0",
          "0000:65:00.1"
        ],
        "AllocResp": "CkEKJFBDSURFVklDRV9JTlRFTF9DT01fSU5URUxfU1JJT1ZfRFBESxIZMDAwMDo2NTowMC4wLDAwMDA6NjU6MDAuMRolCg4vZGV2L3ZmaW8vdmZpbxIOL2Rldi92ZmlvL3ZmaW8aA21ydxohCgwvZGV2L3ZmaW8vNjcSDC9kZXYvdmZpby82NxoDbXJ3GiEKDC9kZXYvdmZpby82OBIML2Rldi92ZmlvLzY4GgNtcnc="
      }
    ],
    "RegisteredDevices": {
      "intel.com/intel_sriov_dpdk": [
        "0000:65:00.0",
        "0000:65:00.1"
      ],
      "intel.com/intel_sriov_netdevice": [
        "0000:b7:0a.3",
        "0000:b7:06.2",
        "0000:b7:0e.5",
        "0000:b7:02.1",
        "0000:b7:06.0",
        "0000:b7:06.6",
        "0000:b7:0a.4",
        "0000:b7:0a.1",
        "0000:b7:0e.2",
        "0000:b7:0a.2",
        "0000:b7:02.6",
        "0000:b7:02.3",
        "0000:b7:0e.6",
        "0000:b7:0e.1",
        "0000:b7:06.5",
        "0000:b7:02.4",
        "0000:b7:0a.0",
        "0000:b7:0e.7",
        "0000:b7:06.4",
        "0000:b7:0e.3",
        "0000:b7:0a.7",
        "0000:b7:02.0",
        "0000:b7:06.3",
        "0000:b7:0a.5",
        "0000:b7:02.5",
        "0000:b7:0e.0",
        "0000:b7:0a.6",
        "0000:b7:06.7",
        "0000:b7:0e.4",
        "0000:b7:02.2",
        "0000:b7:06.1",
        "0000:b7:02.7"
      ],
      "intel.com/mlnx_sriov_rdma": []
    }
  },
  "Checksum": 2067896323
}
```

