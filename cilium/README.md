# cRPD underlay routing for k8s

```
                                      ,------------------------.
                                      |     ,--------------.   |
                                      |     |              |   |
+---------+       +---------+      +--+-----+--+        +--+---+--+
|   nuc   |       |  ryzen1 |      |   ryzen9  |        |  pktgen |
|  ctrl1  |       |  work1  |      |   work2   |        |  xeon1  |
+----+----+       +----+----+      +--+-----+--+        +--+---+--+
     |.41              |.51        .85|     |.85        .49|   |
     |                 |              |    -+--------------+-  |
     |                 |              |      192.168.3/24      |
     |                 |              |  2a02:168:5f67:3::/64  |
     |                 |              |                        |
    -+-----------------+--------------+------------------------+-
                        192.168.10/24
                    2a02:168:5f67:10::/64
```

Using k3s, but disable default CNI, installing it first on nuc:

```
$ cd nuc
$ ./install-k3s.sh
```

Deploy xcrpd on worker nodes 

```
$ cd xcrpd
$ make apply
```


With crpd instances running on all worker nodes, we get connectivity across worker nodes to xeon1.
Try this out with running an alpine instance:

```
$ $ kubectl run alpine -ti --image=alpine
If you don't see a command prompt, try pressing enter.
/ # ping 192.168.3.49
PING 192.168.3.49 (192.168.3.49): 56 data bytes
64 bytes from 192.168.3.49: seq=0 ttl=62 time=0.270 ms
64 bytes from 192.168.3.49: seq=1 ttl=62 time=0.243 ms
64 bytes from 192.168.3.49: seq=2 ttl=62 time=0.902 ms
^C
--- 192.168.3.49 ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 0.243/0.471/0.902 ms
/ # exit
```

inspect loaded BPF program. With alpine pod running

```
mwiget@ryzen9:~$ ip link
. . . 
20: lxc2b5388c04385@if19: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 76:f5:24:ad:06:b9 brd ff:ff:ff:ff:ff:ff link-netns cni-832793a7-150e-373c-852b-907ad472c64e
34: lxc4f1917da39b0@if33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 66:c4:d5:f3:d5:b7 brd ff:ff:ff:ff:ff:ff link-netns cni-55287176-ee18-f0fb-7eb3-7b3d0a394be5


mwiget@ryzen9:~$ tc filter show dev lxc4f1917da39b0 ingress
filter protocol all pref 1 bpf chain 0 
filter protocol all pref 1 bpf chain 0 handle 0x1 bpf_lxc.o:[from-container] direct-action not_in_hw id 530 tag 0a1a5be6abcf3732 
```



A good walkthru can be found here: http://arthurchiao.art/blog/cilium-life-of-a-packet-pod-to-service/


