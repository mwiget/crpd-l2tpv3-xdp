#!/bin/bash

SECONDS=0

MASTER="nuc"
#WORKERS="ryzen1 ryzen2 ryzen9 xeon1"
WORKERS="ryzen1 ryzen2 ryzen9"

echo "installing k3s master locally ..."

export INSTALL_K3S_EXEC=$(cat <<EOF
--flannel-backend=none
--no-flannel
--no-deploy servicelb
--no-deploy traefik
--write-kubeconfig-mode "0644"
EOF
)

echo with the following settings:
echo $INSTALL_K3S_EXEC
echo ""
curl -sfL https://get.k3s.io | sh -

echo ""
kubectl get nodes -o wide
echo ""

echo "installing k3s agents on $WORKER .."

for worker in $WORKERS; do
    K3S_URL="https://$MASTER:6443"
    NODE_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token)

    if [ -z "$NODE_TOKEN" ]; then
        echo "can't access /var/lib/rancher/k3s/server/node-token, is k3s running?"
        exit 1
    fi
    echo "adding node $worker ..."
    ssh $worker "curl -sfL https://get.k3s.io | NSTALL_K3S_EXEC='--no-flannel' K3S_URL=${K3S_URL} K3S_TOKEN=${NODE_TOKEN} sh -" &
    echo ""
done

./add-docker-registry-credentials.sh

echo ""
echo "installing host-local IPAM from containernetworking/plugins ..."
(cd ../plugins; ./build_linux.sh && sudo cp bin/host-local /opt/cni/bin/)

nodes=$(echo "$MASTER $WORKERS" | wc -w)
echo "waiting for all nodes ($nodes) to register ..."
while true; do
  current=$(kubectl get nodes --no-headers | wc -l)
  kubectl get nodes -o wide
  if [ $current -eq $nodes ]; then
    break
  fi
  sleep 2
done

echo ""
echo "instal kube-router ..."
kubectl apply -f generic-kuberouter-only-advertise-routes.yaml

echo "waiting for kube-routers to be active ..."
while true; do
  current=$(kubectl -n kube-system get pods -l k8s-app=kube-router --no-headers | wc -l)
  kubectl -n kube-system get pods -l k8s-app=kube-router
  if [ $current -eq $nodes ]; then
    break
  fi
  sleep 2
done

echo "installing cilium ..."
#kubectl create -f https://raw.githubusercontent.com/cilium/cilium/1.8.3/install/kubernetes/quick-install.yaml
kubectl create -f cilium-quick-install.yaml

echo ""
kubectl get nodes -o wide
kubectl get pods --all-namespaces -o wide
echo ""

echo "installing multus ..."
kubectl create -f multus-daemonset.yml
#kubectl create -f https://raw.githubusercontent.com/intel/multus-cni/master/images/multus-daemonset.yml

echo ""
echo "labelling worker nodes as workers ..."
for worker in $WORKERS; do
  kubectl label nodes $worker worker=true
done

#echo ""
#echo "adding Juniper cRPD to worker nodes ..."
#kubectl apply -f xcrpd.yaml

echo ""
kubectl get nodes -o wide
kubectl get pods --all-namespaces -o wide
echo ""

echo ""
echo "adding SR-IOV configMap ..."
kubectl create -f sriov-configMap.yaml

echo ""
echo "deploy SR-IOV network device plugin DaemonSet ..."
kubectl create -f sriovdp-daemonset.yaml  

echo ""
echo "create SR-IOV network CRD ..."
kubectl create -f sriov-crd.yaml

echo ""
echo "create SR-IOV DPDK network CRD ..."
kubectl create -f sriovdpdk-crd.yaml 

echo ""
echo "Completed in $SECONDS seconds"
