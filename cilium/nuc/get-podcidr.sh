#!/bin/bash

#kubectl get nodes -o jsonpath='{.items[*].spec.podCIDR}'
#kubectl get nodes -o json > x.json
#kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}'

# produces a table with name podCIDR InternalIP:

# ./get-podcidr.sh
# xeon1 10.42.4.0/24 192.168.0.49
# ryzen9 10.42.3.0/24 192.168.0.85
# nuc 10.42.0.0/24 192.168.0.41
# ryzen2 10.42.1.0/24 192.168.0.56
# ryzen1 10.42.2.0/24 192.168.0.51

kubectl get nodes -o jsonpath='{range .items[*]}{.metadata.name}{" "}{.spec.podCIDR}{" "}{.status.addresses[?(@.type=="InternalIP")].address}{"\n"}{end}'
