# install k3s with cilium and multus

note: need to run cilium/xeon1/create-sriov-vf.sh on xeon1

Adjust the master and workers in the script [install-k3s.sh](install-k3s.sh) then run it.


```
$ ./install-k3s.sh
. . .

adding Juniper cRPD to worker nodes ...
daemonset.apps/xcrpd created

NAME     STATUS     ROLES    AGE   VERSION        INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
nuc      Ready      master   54s   v1.18.9+k3s1   192.168.0.41    <none>        Ubuntu 20.04.1 LTS   5.4.0-48-generic   containerd://1.3.3-k3s2
ryzen1   Ready      <none>   17s   v1.18.9+k3s1   192.168.10.51   <none>        Ubuntu 20.04.1 LTS   5.4.0-48-generic   containerd://1.3.3-k3s2
ryzen2   NotReady   <none>   7s    v1.18.9+k3s1   192.168.0.56    <none>        Ubuntu 20.04.1 LTS   5.4.0-48-generic   containerd://1.3.3-k3s2
ryzen9   Ready      <none>   12s   v1.18.9+k3s1   192.168.0.85    <none>        Ubuntu 20.04.1 LTS   5.4.0-48-generic   containerd://1.3.3-k3s2
xeon1    NotReady   <none>   3s    v1.18.9+k3s1   192.168.0.49    <none>        Ubuntu 20.04.1 LTS   5.4.0-48-generic   containerd://1.3.3-k3s2
NAMESPACE     NAME                                     READY   STATUS              RESTARTS   AGE   IP              NODE     NOMINATED NODE   READINESS GATES
kube-system   coredns-7944c66d8d-sgtcv                 0/1     ContainerCreating   0          41s   <none>          nuc      <none>           <none>
kube-system   metrics-server-7566d596c8-sjfhc          0/1     ContainerCreating   0          41s   <none>          nuc      <none>           <none>
kube-system   local-path-provisioner-6d59f47c7-dfhv2   0/1     ContainerCreating   0          41s   <none>          nuc      <none>           <none>
kube-system   helm-install-traefik-hspbf               0/1     ContainerCreating   0          41s   <none>          nuc      <none>           <none>
kube-system   cilium-operator-6599b6fb6f-4sr9x         0/1     ContainerCreating   0          2s    192.168.0.56    ryzen2   <none>           <none>
kube-system   cilium-operator-6599b6fb6f-sh2s2         0/1     ContainerCreating   0          2s    192.168.10.51   ryzen1   <none>           <none>
kube-system   cilium-n777q                             0/1     Init:0/1            0          2s    192.168.0.41    nuc      <none>           <none>
kube-system   cilium-9d94n                             0/1     Init:0/1            0          2s    192.168.10.51   ryzen1   <none>           <none>
kube-system   cilium-7lg5t                             0/1     Init:0/1            0          2s    192.168.0.56    ryzen2   <none>           <none>
kube-system   cilium-vsnzf                             0/1     Init:0/1            0          2s    192.168.0.85    ryzen9   <none>           <none>
kube-system   cilium-7nsrq                             0/1     Init:0/1            0          2s    192.168.0.49    xeon1    <none>           <none>
kube-system   kube-multus-ds-amd64-wdrqp               0/1     ContainerCreating   0          1s    192.168.0.41    nuc      <none>           <none>
kube-system   kube-multus-ds-amd64-7zdjn               0/1     ContainerCreating   0          1s    192.168.10.51   ryzen1   <none>           <none>
kube-system   kube-multus-ds-amd64-bmcxg               0/1     ContainerCreating   0          1s    192.168.0.85    ryzen9   <none>           <none>
kube-system   kube-multus-ds-amd64-wjccr               0/1     ContainerCreating   0          1s    192.168.0.56    ryzen2   <none>           <none>
kube-system   kube-multus-ds-amd64-gxr9m               0/1     ContainerCreating   0          1s    192.168.0.49    xeon1    <none>           <none>
kube-system   xcrpd-xb2fg                              0/1     ContainerCreating   0          1s    192.168.10.51   ryzen1   <none>           <none>
kube-system   xcrpd-cq8g4                              0/1     ContainerCreating   0          1s    192.168.0.85    ryzen9   <none>           <none>

Completed in 92 seconds
```

Wait a few more minutes to download and launch all the pods in kube-system, then check the pods. On this run, it took just short of 4 minutes 
in total:

```
 kubectl get pods -A -o wide
NAMESPACE     NAME                                     READY   STATUS      RESTARTS   AGE     IP             NODE     NOMINATED NODE   READINESS GATES
kube-system   cilium-operator-6599b6fb6f-4sr9x         1/1     Running     0          3m2s    192.168.0.56   ryzen2   <none>           <none>
kube-system   cilium-operator-6599b6fb6f-sh2s2         1/1     Running     0          3m2s    192.168.0.51   ryzen1   <none>           <none>
kube-system   cilium-n777q                             1/1     Running     0          3m2s    192.168.0.41   nuc      <none>           <none>
kube-system   xcrpd-cq8g4                              1/1     Running     0          3m1s    192.168.0.85   ryzen9   <none>           <none>
kube-system   xcrpd-xb2fg                              1/1     Running     0          3m1s    192.168.0.51   ryzen1   <none>           <none>
kube-system   cilium-vsnzf                             0/1     Running     0          3m2s    192.168.0.85   ryzen9   <none>           <none>
kube-system   kube-multus-ds-amd64-wdrqp               1/1     Running     0          3m1s    192.168.0.41   nuc      <none>           <none>
kube-system   xcrpd-4vkv8                              1/1     Running     0          2m53s   192.168.0.49   xeon1    <none>           <none>
kube-system   kube-multus-ds-amd64-bmcxg               1/1     Running     0          3m1s    192.168.0.85   ryzen9   <none>           <none>
kube-system   xcrpd-5z7sk                              1/1     Running     0          2m57s   192.168.0.56   ryzen2   <none>           <none>
kube-system   cilium-9d94n                             0/1     Running     0          3m2s    192.168.0.51   ryzen1   <none>           <none>
kube-system   local-path-provisioner-6d59f47c7-dfhv2   1/1     Running     0          3m41s   10.0.0.241     nuc      <none>           <none>
kube-system   kube-multus-ds-amd64-gxr9m               1/1     Running     0          3m1s    192.168.0.49   xeon1    <none>           <none>
kube-system   kube-multus-ds-amd64-wjccr               1/1     Running     0          3m1s    192.168.0.56   ryzen2   <none>           <none>
kube-system   kube-multus-ds-amd64-7zdjn               1/1     Running     0          3m1s    192.168.0.51   ryzen1   <none>           <none>
kube-system   metrics-server-7566d596c8-sjfhc          1/1     Running     0          3m41s   10.0.0.96      nuc      <none>           <none>
kube-system   cilium-7nsrq                             1/1     Running     0          3m2s    192.168.0.49   xeon1    <none>           <none>
kube-system   helm-install-traefik-hspbf               0/1     Completed   0          3m41s   10.0.0.17      nuc      <none>           <none>
kube-system   svclb-traefik-cg9vt                      2/2     Running     0          8s      10.0.0.128     nuc      <none>           <none>
kube-system   svclb-traefik-qzdmn                      2/2     Running     0          8s      10.0.1.128     ryzen9   <none>           <none>
kube-system   svclb-traefik-646h4                      2/2     Running     0          8s      10.0.2.37      ryzen2   <none>           <none>
kube-system   svclb-traefik-mdbvl                      2/2     Running     0          9s      10.0.3.252     xeon1    <none>           <none>
kube-system   cilium-7lg5t                             1/1     Running     0          3m2s    192.168.0.56   ryzen2   <none>           <none>
kube-system   svclb-traefik-xcv9k                      2/2     Running     0          8s      10.0.4.171     ryzen1   <none>           <none>
kube-system   coredns-7944c66d8d-sgtcv                 1/1     Running     0          3m41s   10.0.0.124     nuc      <none>           <none>
kube-system   traefik-758cd5fc85-cfdk7                 0/1     Running     0          9s      10.0.1.190     ryzen9   <none>           <none>
```

Log into one of the crpd instances to check ISIS:

```
$ 
$ kubectl -n kube-system exec -ti xcrpd-cq8g4 -- bash

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===


 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC
 Linux ryzen9 5.4.0-48-generic #52-Ubuntu SMP Thu Sep 10 10:58:49 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    ryzen9 (127.0.1.1)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0-23, 57122 MBytes available
 _/        _/    _/  _/        _/   _/    AMD Ryzen 9 3900X 12-Core Processor
  _/_/_/  _/    _/  _/        _/_/_/     212.51.142.174 in Zug,Switzerland,AS13030 Init7 (Switzerland) Ltd.

root@ryzen9:~# cli
root@ryzen9> show isis adjacency 
Interface             System         L State        Hold (secs) SNPA
enp11s0f0             xeon1          2  Up                   23  0:25:90:bb:2c:a2
enp11s0f0             ryzen2         2  Up                   25  0:1b:21:99:b7:c8
enp11s0f0             ryzen1         2  Up                   23  0:1b:21:99:cc:14
enp11s0f1             xeon1          2  Up                   24  0:25:90:bb:2c:a3
enp5s0                xeon1          2  Up                   26  0:25:90:bb:30:48
enp5s0                ryzen2         2  Up                   26  3c:ec:ef:43:1c:b2
enp5s0                ryzen1         2  Up                   26  3c:ec:ef:43:1d:66

root@ryzen9> show interfaces routing 
Interface        State Addresses
lxc1499c9fc1164  Up    INET6 fe80::d0d1:fcff:feda:6331
lxcd8a1907c72a6  Up    INET6 fe80::c06b:3dff:fec6:5382
lxc_health       Up    INET6 fe80::6454:beff:fe72:7e47
cilium_host      Up    ISO   enabled
                       INET  10.0.1.96
                       INET6 fe80::f028:d4ff:fea3:2e54
cilium_net       Up    INET6 fe80::dcd5:f8ff:fec0:835d
veth2f69ce0      Up    ISO   enabled
                       INET6 fe80::d8da:53ff:fed8:a08a
lsi              Up    ISO   enabled
                       INET6 fe80::9832:f5ff:fefd:e1c
lo.0             Up    ISO   enabled
                       INET6 fe80::1
                       ISO   47.0005.abcd.abcd.0000.0000.0000.40a6.b723.b858
enp9s0f1         Up    ISO   enabled
                       INET  192.168.41.85
                       INET6 2a02:168:5f67:41::85
                       INET6 fe80::3a2:b007:2ff:d736
enp9s0f0         Up    ISO   enabled
                       INET  192.168.40.85
                       INET6 2a02:168:5f67:40::85
                       INET6 fe80::1a58:14f9:d0b0:55e9
enp5s0           Up    ISO   enabled
                       INET  192.168.0.85
                       INET6 2a02:168:5f67:0:23c0:597b:43f5:d26c
                       INET6 2a02:168:5f67:0:5cef:8ccb:3677:4ae6
                       INET6 2a02:168:5f67:0:b15f:2256:cdec:6ade
                       INET6 fe80::23c0:597b:43f5:d26c
enp11s0f3        Down  ISO   enabled
enp11s0f2        Down  ISO   enabled
enp11s0f1        Up    ISO   enabled
                       INET  192.168.3.85
                       INET6 2a02:168:5f67:3::85
                       INET6 fe80::8c28:b0d4:9890:d2fe
enp11s0f0        Up    ISO   enabled
                       INET  192.168.10.85
                       INET6 fe80::3093:575d:cdfd:e871
docker0          Up    ISO   enabled
                       INET  172.17.0.1
                       INET6 fe80::42:d1ff:fe96:d48a
cilium_vxlan     Up    ISO   enabled
                       INET6 fe80::2028:faff:fe55:8941
br-9bfd867f64ba  Down  ISO   enabled
                       INET  172.18.0.1
                       INET6 fc00:f853:ccd:e793::1
                       INET6 fe80::1
```



```
$ kubectl get pods -o wide -A
NAMESPACE     NAME                                     READY   STATUS      RESTARTS   AGE     IP             NODE     NOMINATED NODE   READINESS GATES
kube-system   kube-sriov-device-plugin-amd64-bq6hg     1/1     Running     0          3m56s   192.168.0.41   nuc      <none>           <none>
kube-system   kube-sriov-device-plugin-amd64-x2h9g     1/1     Running     0          3m56s   192.168.0.51   ryzen1   <none>           <none>
kube-system   kube-sriov-device-plugin-amd64-wg2dr     1/1     Running     0          3m56s   192.168.0.49   xeon1    <none>           <none>
kube-system   kube-sriov-device-plugin-amd64-bbcfr     1/1     Running     0          3m51s   192.168.0.56   ryzen2   <none>           <none>
kube-system   cilium-operator-6599b6fb6f-jw5xm         1/1     Running     0          4m      192.168.0.51   ryzen1   <none>           <none>
kube-system   cilium-operator-6599b6fb6f-7qbtc         1/1     Running     0          4m      192.168.0.85   ryzen9   <none>           <none>
kube-system   kube-sriov-device-plugin-amd64-b6rf9     1/1     Running     0          3m54s   192.168.0.85   ryzen9   <none>           <none>
kube-system   xcrpd-x7pgm                              1/1     Running     0          3m58s   192.168.0.51   ryzen1   <none>           <none>
kube-system   xcrpd-2w2m8                              1/1     Running     0          3m51s   192.168.0.56   ryzen2   <none>           <none>
kube-system   xcrpd-9klw6                              1/1     Running     0          3m54s   192.168.0.85   ryzen9   <none>           <none>
kube-system   kube-multus-ds-amd64-krgv6               1/1     Running     0          3m59s   192.168.0.51   ryzen1   <none>           <none>
kube-system   kube-multus-ds-amd64-j2zgr               1/1     Running     0          3m59s   192.168.0.41   nuc      <none>           <none>
kube-system   kube-multus-ds-amd64-65b5z               1/1     Running     0          3m59s   192.168.0.85   ryzen9   <none>           <none>
kube-system   xcrpd-fckf5                              1/1     Running     0          3m58s   192.168.0.49   xeon1    <none>           <none>
kube-system   cilium-h9l78                             1/1     Running     0          4m      192.168.0.49   xeon1    <none>           <none>
kube-system   kube-multus-ds-amd64-bhxx7               1/1     Running     0          3m59s   192.168.0.56   ryzen2   <none>           <none>
kube-system   kube-multus-ds-amd64-5jn65               1/1     Running     0          3m59s   192.168.0.49   xeon1    <none>           <none>
kube-system   local-path-provisioner-6d59f47c7-bqkfw   1/1     Running     0          4m34s   10.0.0.171     nuc      <none>           <none>
kube-system   cilium-np9gv                             1/1     Running     0          4m      192.168.0.41   nuc      <none>           <none>
kube-system   cilium-dt4dn                             1/1     Running     0          4m      192.168.0.85   ryzen9   <none>           <none>
kube-system   coredns-7944c66d8d-d6rq5                 1/1     Running     0          4m34s   10.0.0.48      nuc      <none>           <none>
kube-system   cilium-rxvhv                             1/1     Running     0          4m      192.168.0.56   ryzen2   <none>           <none>
kube-system   metrics-server-7566d596c8-b27ww          1/1     Running     0          4m34s   10.0.0.188     nuc      <none>           <none>
kube-system   cilium-24xt7                             1/1     Running     0          4m      192.168.0.51   ryzen1   <none>           <none>
kube-system   helm-install-traefik-zkgpg               0/1     Completed   0          4m35s   10.0.0.178     nuc      <none>           <none>
kube-system   svclb-traefik-lj9c2                      2/2     Running     0          62s     10.0.0.133     nuc      <none>           <none>
kube-system   svclb-traefik-tzrtb                      2/2     Running     0          62s     10.0.4.104     ryzen9   <none>           <none>
kube-system   svclb-traefik-n5tpr                      2/2     Running     0          62s     10.0.2.23      ryzen2   <none>           <none>
kube-system   svclb-traefik-9twsz                      2/2     Running     0          62s     10.0.3.185     xeon1    <none>           <none>
kube-system   traefik-758cd5fc85-b8w2w                 1/1     Running     0          62s     10.0.4.67      ryzen9   <none>           <none>
kube-system   svclb-traefik-pd9px                      2/2     Running     0          62s     10.0.1.75      ryzen1   <none>           <none>
```

## SR-IOV

Query allocatable resource list for a node (xeon1 in this example, which has VFs configured), use

```
$ kubectl get node xeon1 -o json| jq '.status.allocatable'
{
  "cpu": "16",
  "ephemeral-storage": "934255593149",
  "hugepages-1Gi": "12Gi",
  "intel.com/intel_sriov_dpdk": "0",
  "intel.com/intel_sriov_netdevice": "16",
  "intel.com/mlnx_sriov_rdma": "0",
  "memory": "53006208Ki",
  "pods": "110"
}
```

Launching a test pod:

```
$ kubectl create -f pod-tc1.yaml                                                                                                                                                                                                       
pod/testpod1 created                                                                                                                                                                                                                                                              
mwiget@nuc:~/git/crpd-l2tpv3-xdp/cilium/nuc$ kubectl get pods                                                                                                                                                                                                                     
NAME       READY   STATUS              RESTARTS   AGE                                                                                                                                                                                                                             
testpod1   0/1     ContainerCreating   0          2s
```

Use describe to see more details about progress

```
$ kubectl describe pods
Name:         testpod1                                                                                                                                                                                                                                                            
Namespace:    default
Priority:     0
Node:         xeon1/192.168.0.49
Start Time:   Tue, 06 Oct 2020 14:58:29 +0200
Labels:       <none>
Annotations:  k8s.v1.cni.cncf.io/network-status:
                [{
                    "name": "",
                    "interface": "eth0",
                    "ips": [
                        "10.0.0.36"
                    ],
                    "mac": "36:f3:57:5d:f8:07",
                    "default": true,
                    "dns": {}
                },{
                    "name": "default/sriov-net1",
                    "interface": "net1",
                    "ips": [
                        "192.168.41.1"
                    ],
                    "mac": "b2:67:59:5a:d1:7b",
                    "dns": {}
                }]
              k8s.v1.cni.cncf.io/networks: sriov-net1
              k8s.v1.cni.cncf.io/networks-status:
                [{
                    "name": "",
                    "interface": "eth0",
                    "ips": [
                        "10.0.0.36"
                    ],
                    "mac": "36:f3:57:5d:f8:07",
                    "default": true,
                    "dns": {}
                },{
                    "name": "default/sriov-net1",
                    "interface": "net1",
                    "ips": [
                        "192.168.41.1"
                    ],
                    "mac": "b2:67:59:5a:d1:7b",
                    "dns": {}
                }]
Status:       Pending
IP:           
IPs:          <none>
Containers:
  appcntr1:
    Container ID:  
    Image:         centos/tools
    Image ID:      
    Port:          <none>
    Host Port:     <none>
    Command:
      /bin/bash
      -c
      --
    Args:
      while true; do sleep 300000; done;
   State:          Waiting  
      Reason:       ContainerCreating
    Ready:          False                            
    Restart Count:  0                            
    Limits:       
      intel.com/intel_sriov_netdevice:  1
    Requests:                           
      intel.com/intel_sriov_netdevice:  1
    Environment:                        <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-24h6s (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  default-token-24h6s:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-24h6s
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason          Age   From               Message
  ----    ------          ----  ----               -------
  Normal  Scheduled       6s    default-scheduler  Successfully assigned default/testpod1 to xeon1
  Normal  AddedInterface  6s    multus             Add eth0 [10.0.0.36/32]
  Normal  AddedInterface  5s    multus             Add net1 [192.168.41.1/24] from default/sriov-net1
  Normal  Pulling         5s    kubelet            Pulling image "centos/tools"
```

and soon it is ready

```
$ kubectl get pods
NAME       READY   STATUS    RESTARTS   AGE
testpod1   1/1     Running   0          2m17s
```

```
$ kubectl exec -ti testpod1 -- bash
[root@testpod1 /]# ifconfig 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.36  netmask 255.255.255.255  broadcast 0.0.0.0
        inet6 fe80::34f3:57ff:fe5d:f807  prefixlen 64  scopeid 0x20<link>
        ether 36:f3:57:5d:f8:07  txqueuelen 0  (Ethernet)
        RX packets 13  bytes 2036 (1.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 11  bytes 866 (866.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

net1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.41.1  netmask 255.255.255.0  broadcast 192.168.41.255
        ether b2:67:59:5a:d1:7b  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

But somehow the net1 interface has linkdown:

```
mwiget@nuc:~/git/crpd-l2tpv3-xdp/cilium/nuc$ kubectl exec -ti testpod1 -- bash
[root@testpod1 /]# ip r
default via 10.0.0.114 dev eth0 mtu 1450 
10.0.0.114 dev eth0 scope link 
192.168.41.0/24 dev net1 proto kernel scope link src 192.168.41.1 linkdown 
[root@testpod1 /]# 
```

```
[root@testpod1 /]# ethtool net1
Settings for net1:
        Supported ports: [ ]
        Supported link modes:   Not reported
        Supported pause frame use: No
        Supports auto-negotiation: No
        Supported FEC modes: Not reported
        Advertised link modes:  Not reported
        Advertised pause frame use: No
        Advertised auto-negotiation: No
        Advertised FEC modes: Not reported
        Speed: Unknown!
        Duplex: Full
        Port: None
        PHYAD: 0
        Transceiver: internal
        Auto-negotiation: off
Cannot get wake-on-lan settings: Operation not permitted
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: no
[root@testpod1 /]# 
```

```
mwiget@nuc:~/git/crpd-l2tpv3-xdp/cilium/nuc$ kubectl exec -ti testpod1 -- bash
[root@testpod1 /]# ifconfig 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.2.227  netmask 255.255.255.255  broadcast 0.0.0.0
        inet6 fe80::e89e:86ff:fe4c:df70  prefixlen 64  scopeid 0x20<link>
        ether ea:9e:86:4c:df:70  txqueuelen 0  (Ethernet)
        RX packets 13  bytes 2036 (1.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 12  bytes 936 (936.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

net1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.41.1  netmask 255.255.255.0  broadcast 192.168.41.255
        inet6 fe80::2c80:39ff:fe66:a2e9  prefixlen 64  scopeid 0x20<link>
        ether 2e:80:39:66:a2:e9  txqueuelen 1000  (Ethernet)
        RX packets 96  bytes 18439 (18.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 24  bytes 11830 (11.5 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[root@testpod1 /]# ping 192.168.41.85
PING 192.168.41.85 (192.168.41.85) 56(84) bytes of data.
64 bytes from 192.168.41.85: icmp_seq=1 ttl=64 time=0.387 ms
64 bytes from 192.168.41.85: icmp_seq=2 ttl=64 time=0.085 ms
^C
--- 192.168.41.85 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1026ms
rtt min/avg/max/mdev = 0.085/0.236/0.387/0.151 ms
[root@testpod1 /]# arp -na
? (192.168.41.85) at 40:a6:b7:08:7e:20 [ether] on net1
[root@testpod1 /]# 
```

Adding now also dpdk support via ../xeon1/install-sriov-vf.sh (then rebooted xeon1):

```
$ ./show-xeon1-sriov-resources.sh 
{
  "cpu": "16",
  "ephemeral-storage": "934255593149",
  "hugepages-1Gi": "12Gi",
  "intel.com/intel_sriov_dpdk": "2",
  "intel.com/intel_sriov_netdevice": "32",
  "intel.com/mlnx_sriov_rdma": "0",
  "memory": "53006208Ki",
  "pods": "110"
}
```

Deployed 2 pods, one using sriov netdevice, the other one dpdk:

```
$ kubectl get pods

NAME       READY   STATUS    RESTARTS   AGE
testpod1   1/1     Running   1          52m
pktgen     1/1     Running   0          52m
```

```
$ kubectl describe pods

Name:         testpod1
Namespace:    default
Priority:     0
Node:         xeon1/192.168.0.49
Start Time:   Fri, 09 Oct 2020 12:09:46 +0200
Labels:       <none>
Annotations:  k8s.v1.cni.cncf.io/network-status:
                [{
                    "name": "",
                    "interface": "eth0",
                    "ips": [
                        "10.0.1.207"
                    ],
                    "mac": "ae:c8:71:92:66:d9",
                    "default": true,
                    "dns": {}
                },{
                    "name": "default/sriov-net1",
                    "interface": "net1",
                    "ips": [
                        "192.168.41.2"
                    ],
                    "mac": "be:82:d0:e5:92:97",
                    "dns": {}
                }]
              k8s.v1.cni.cncf.io/networks: sriov-net1
              k8s.v1.cni.cncf.io/networks-status:
                [{
                    "name": "",
                    "interface": "eth0",
                    "ips": [
                        "10.0.1.207"
                    ],
                    "mac": "ae:c8:71:92:66:d9",
                    "default": true,
                    "dns": {}
                },{
                    "name": "default/sriov-net1",
                    "interface": "net1",
                    "ips": [
                        "192.168.41.2"
                    ],
                    "mac": "be:82:d0:e5:92:97",
                    "dns": {}
                }]
Status:       Running
IP:           10.0.1.207
IPs:
  IP:  10.0.1.207
Containers:
  appcntr1:
    Container ID:  containerd://0703733ff5e3087497965a46e42bf8a3a4948928909808ccb7b769160340e915
    Image:         centos/tools
    Image ID:      docker.io/centos/tools@sha256:81159542603c2349a276a30cc147045bc642bd84a62c1b427a8d243ef1893e2f
    Port:          <none>
    Host Port:     <none>
    Command:
      /bin/bash
      -c
      --
    Args:
      while true; do sleep 300000; done;
    State:          Running
      Started:      Fri, 09 Oct 2020 12:47:46 +0200
    Last State:     Terminated
      Reason:       Unknown
      Exit Code:    255
      Started:      Fri, 09 Oct 2020 12:10:56 +0200
      Finished:     Fri, 09 Oct 2020 12:46:53 +0200
    Ready:          True
    Restart Count:  1
    Limits:
      intel.com/intel_sriov_netdevice:  1
    Requests:
      intel.com/intel_sriov_netdevice:  1
    Environment:                        <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-qg787 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-qg787:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-qg787
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason          Age                From               Message
  ----     ------          ----               ----               -------
  Normal   Scheduled       52m                default-scheduler  Successfully assigned default/testpod1 to xeon1
  Normal   AddedInterface  52m                multus             Add eth0 [10.0.1.231/32]
  Normal   AddedInterface  52m                multus             Add net1 [192.168.41.1/24] from default/sriov-net1
  Normal   Pulling         52m                kubelet            Pulling image "centos/tools"
  Normal   Pulled          50m                kubelet            Successfully pulled image "centos/tools"
  Normal   Created         50m                kubelet            Created container appcntr1
  Normal   Started         50m                kubelet            Started container appcntr1
  Warning  FailedMount     14m                kubelet            MountVolume.SetUp failed for volume "default-token-qg787" : failed to sync secret cache: timed out waiting for the condition
  Normal   SandboxChanged  14m (x2 over 14m)  kubelet            Pod sandbox changed, it will be killed and re-created.
  Normal   AddedInterface  14m                multus             Add eth0 [10.0.1.207/32]
  Normal   AddedInterface  14m                multus             Add net1 [192.168.41.2/24] from default/sriov-net1
  Normal   Pulled          14m                kubelet            Container image "centos/tools" already present on machine
  Normal   Created         14m                kubelet            Created container appcntr1
  Normal   Started         14m                kubelet            Started container appcntr1


Name:         pktgen
Namespace:    default
Priority:     0
Node:         xeon1/192.168.0.49
Start Time:   Fri, 09 Oct 2020 12:47:06 +0200
Labels:       <none>
Annotations:  k8s.v1.cni.cncf.io/network-status:
                [{
                    "name": "",
                    "interface": "eth0",
                    "ips": [
                        "10.0.1.192"
                    ],
                    "mac": "c6:36:04:fe:1f:d5",
                    "default": true,
                    "dns": {}
                },{
                    "name": "default/sriov1-vfio",
                    "interface": "net1",
                    "dns": {}
                },{
                    "name": "default/sriov1-vfio",
                    "interface": "net2",
                    "dns": {}
                }]
              k8s.v1.cni.cncf.io/networks: [ {"name": "sriov1-vfio"}, {"name": "sriov1-vfio"} ]
              k8s.v1.cni.cncf.io/networks-status:
                [{
                    "name": "",
                    "interface": "eth0",
                    "ips": [
                        "10.0.1.192"
                    ],
                    "mac": "c6:36:04:fe:1f:d5",
                    "default": true,
                    "dns": {}
                },{
                    "name": "default/sriov1-vfio",
                    "interface": "net1",
                    "dns": {}
                },{
                    "name": "default/sriov1-vfio",
                    "interface": "net2",
                    "dns": {}
                }]
Status:       Running
IP:           10.0.1.192
IPs:
  IP:  10.0.1.192
Containers:
  pktgen:
    Container ID:  containerd://f1937b3a7a3aa40f45c2ffda951e38a5cc2f9c35b0c45a3976845183e562cadf
    Image:         marcelwiget/pktgen
    Image ID:      docker.io/marcelwiget/pktgen@sha256:7bfa657749c290cd5020609ce31626b88dee015d51d33d95213af50fe286195e
    Port:          <none>
    Host Port:     <none>
    Command:
      /bin/bash
      -ec
      sleep infinity
    State:          Running
      Started:      Fri, 09 Oct 2020 12:47:57 +0200
    Ready:          True
    Restart Count:  0
    Limits:
      hugepages-1Gi:               4Gi
      intel.com/intel_sriov_dpdk:  2
      memory:                      4Gi
    Requests:
      hugepages-1Gi:               4Gi
      intel.com/intel_sriov_dpdk:  2
      memory:                      4Gi
    Environment:                   <none>
    Mounts:
      /lib/modules from lib-modules (rw)
      /mnt/huge-2048 from hugepage (rw)
      /usr/src from src (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-qg787 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  hugepage:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     HugePages
    SizeLimit:  <unset>
  lib-modules:
    Type:          HostPath (bare host directory volume)
    Path:          /lib/modules
    HostPathType:  
  src:
    Type:          HostPath (bare host directory volume)
    Path:          /usr/src
    HostPathType:  
  default-token-qg787:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-qg787
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason                  Age   From               Message
  ----     ------                  ----  ----               -------
  Warning  FailedScheduling        52m   default-scheduler  0/5 nodes are available: 1 Insufficient hugepages-1Gi, 5 Insufficient intel.com/intel_sriov_dpdk.
  Normal   Scheduled               14m   default-scheduler  Successfully assigned default/pktgen to xeon1
  Warning  FailedScheduling        52m   default-scheduler  0/5 nodes are available: 1 Insufficient hugepages-1Gi, 5 Insufficient intel.com/intel_sriov_dpdk.
  Warning  FailedCreatePodSandBox  14m   kubelet            Failed to create pod sandbox: rpc error: code = Unknown desc = failed to setup network for sandbox "7058e821f9cbb75ea7fd2b58fa9a9073c8f243e23a1972da8d6221461340ba36": Multus: [default/pktgen]: error getting pod: Get https://[10.43.0.1]:443/api/v1/namespaces/default/pods/pktgen?timeout=1m0s: dial tcp 10.43.0.1:443: i/o timeout
  Normal   AddedInterface          14m   multus             Add eth0 [10.0.1.192/32]
  Normal   AddedInterface          14m   multus             Add net2 [] from default/sriov1-vfio
  Normal   AddedInterface          14m   multus             Add net1 [] from default/sriov1-vfio
  Normal   Pulling                 14m   kubelet            Pulling image "marcelwiget/pktgen"
  Normal   Pulled                  13m   kubelet            Successfully pulled image "marcelwiget/pktgen"
  Normal   Created                 13m   kubelet            Created container pktgen
  Normal   Started                 13m   kubelet            Started container pktgen
