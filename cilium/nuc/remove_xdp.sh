#!/bin/bash

WORKERS="ryzen1 ryzen2 ryzen9 xeon1"

set -e
echo "extracting xdp tools from xdpbuild ..."
docker rm xdpbuild 2>/dev/null || true
docker create --rm --name xdpbuild xdpbuild
rm -f xdp_stats xdp_tunnels xdp_loader xdp_router.o
docker cp xdpbuild:/xdp_loader . >/dev/null
ls -l xdp_loader
docker rm xdpbuild >/dev/null

for workers in $WORKERS; do
  echo ""
  echo $workers ...
  scp xdp_loader $workers: >/dev/null
  ssh $workers "ip link |grep xdp|grep BROADCAST|cut -d':' -f2"
  ssh $workers "ip link |grep xdp|grep BROADCAST|cut -d':' -f2 | xargs -I % sudo ./xdp_loader -U --dev %"
done
