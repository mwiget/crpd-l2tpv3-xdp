#!/bin/bash

WORKERS="ryzen1 ryzen2 ryzen9 xeon1"
for worker in $WORKERS; do
    ssh $worker "sudo reboot"
    echo ""
done
