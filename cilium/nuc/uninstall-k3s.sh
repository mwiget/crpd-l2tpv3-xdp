#!/bin/bash

WORKERS="ryzen1 ryzen2 ryzen9 xeon1"
for worker in $WORKERS; do
    echo -n "uninstall first $worker ... "
    ssh $worker /usr/local/bin/k3s-agent-uninstall.sh || echo "not installed"
    ssh $worker sudo ip link del dev cilium_host
    ssh $worker sudo ip link del dev cilium_vxlan
    kubectl delete node $worker
done

/usr/local/bin/k3s-uninstall.sh || echo "not installed"

sudo ip link del dev cilium_host
sudo ip link del dev cilium_vxlan
