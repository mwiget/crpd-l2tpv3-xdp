#!/bin/bash

node=$1

if [ -z "$node" ]; then
  kubectl -n kube-system get pods -o wide |grep xcrpd- | awk '{print $7, $1}'
  exit
fi

pod=$(kubectl -n kube-system get pods -o wide |grep xcrpd- | grep $node | awk '{print $1}')
kubectl -n kube-system exec -ti $pod -- bash
