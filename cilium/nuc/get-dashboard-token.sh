#!/bin/bash

echo "token to connect to dashboard ..."
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')

echo "get the dashboard assigned port ..."
kubectl describe svc kubernetes-dashboard -n kubernetes-dashboard
