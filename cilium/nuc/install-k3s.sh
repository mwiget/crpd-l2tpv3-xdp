#!/bin/bash

SECONDS=0

MASTER="nuc"
WORKERS="ryzen1 ryzen2 ryzen9 xeon1"
#WORKERS="ryzen1 ryzen2 ryzen9"

echo "installing k3s master locally ..."

export INSTALL_K3S_EXEC=$(cat <<EOF
--flannel-backend=none
--no-flannel
--no-deploy servicelb
--no-deploy traefik
--write-kubeconfig-mode "0644"
EOF
)

echo with the following settings:
echo $INSTALL_K3S_EXEC
echo ""
curl -sfL https://get.k3s.io | sh -

echo ""
kubectl get nodes -o wide
echo ""

echo "installing k3s agents on $WORKER .."

for worker in $WORKERS; do
    K3S_URL="https://$MASTER:6443"
    NODE_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token)

    if [ -z "$NODE_TOKEN" ]; then
        echo "can't access /var/lib/rancher/k3s/server/node-token, is k3s running?"
        exit 1
    fi
    echo "adding node $worker ..."
    ssh $worker "curl -sfL https://get.k3s.io | NSTALL_K3S_EXEC='--no-flannel' K3S_URL=${K3S_URL} K3S_TOKEN=${NODE_TOKEN} sh -" &
    echo ""
done

./add-docker-registry-credentials.sh

echo ""
echo "installing host-local IPAM from containernetworking/plugins ..."
(cd ../plugins; ./build_linux.sh && sudo cp bin/host-local /opt/cni/bin/)

nodes=$(echo "$MASTER $WORKERS" | wc -w)
echo "waiting for all nodes ($nodes) to register ..."
while true; do
  current=$(kubectl get nodes --no-headers | wc -l)
  kubectl get nodes -o wide
  if [ $current -eq $nodes ]; then
    break
  fi
  sleep 2
done

echo ""
#echo "instal kube-router ..."
#kubectl apply -f generic-kuberouter-only-advertise-routes.yaml

echo "adding Juniper cRPD to worker nodes ..."
kubectl apply -f xcrpd.yaml

echo "waiting for crpd to be active ..."
while true; do
  current=$(kubectl -n kube-system get pods -l k8s-app=xcrpd --no-headers | wc -l)
  kubectl -n kube-system get pods -l k8s-app=xcrpd
  if [ $current -eq $nodes ]; then
    break
  fi
  sleep 2
done

echo "installing cilium ..."
#kubectl create -f https://raw.githubusercontent.com/cilium/cilium/1.8.3/install/kubernetes/quick-install.yaml
kubectl create -f cilium-quick-install.yaml
#sudo helm repo add cilium https://helm.cilium.io/

# via helm breaks the cluster with tunnel disabled. Likely I miss some options

#sudo helm install cilium cilium/cilium --set global.tag="v1.9.0-rc1" \
#  --set global.kubeProxyReplacement="strict" \
#  --set global.autoDirectNodeRoutes="true" \
#  --set global.nodePort.mode="dsr" \
#  --set global.tunnel="disabled" \
#  --set global.nativeRoutingCIDR="192.168.0.0/16" \
#  --set global.endpointRoutes="true" \
#  --set global.autoDirectNodeRoutes="true" \
#  --set global.containerRuntime.integration="containerd" \
#  --set global.containerRuntime.socketPath="/var/run/k3s/containerd/containerd.sock" \
#  --set global.hubble.enabled="true" \
#  --set global.hubble.listenAddress=":4244" \
#  --set global.hubble.metrics.enabled="{dns,drop,tcp,flow,port-distribution,icmp,http}" \
#  --set global.hubble.relay.enabled="true" \
#  --set global.hubble.ui.enabled="true" \
#  --kubeconfig /etc/rancher/k3s/k3s.yaml --namespace kube-system

echo ""
kubectl get nodes -o wide
kubectl get pods --all-namespaces -o wide
echo ""

echo "installing multus ..."
kubectl create -f multus-daemonset.yml
#kubectl create -f https://raw.githubusercontent.com/intel/multus-cni/master/images/multus-daemonset.yml

echo ""
echo "labelling worker nodes as workers ..."
for worker in $WORKERS; do
  kubectl label nodes $worker worker=true
done

echo ""
kubectl get nodes -o wide
kubectl get pods --all-namespaces -o wide
echo ""

echo ""
echo "adding SR-IOV configMap ..."
kubectl create -f sriov-configMap.yaml

echo ""
echo "deploy SR-IOV network device plugin DaemonSet ..."
kubectl create -f sriovdp-daemonset.yaml  

echo ""
echo "create SR-IOV network CRD ..."
kubectl create -f sriov-crd.yaml

echo ""
echo "create SR-IOV DPDK network CRD ..."
kubectl create -f sriovdpdk-crd.yaml 

echo ""
echo "install dashboard ..."
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

echo "Create a service account and bind cluster-admin role to this ..."
kubectl create serviceaccount admin-user --namespace kube-system
kubectl create clusterrolebinding admin-user --clusterrole=cluster-admin --serviceaccount=kube-system:admin-user

echo "token to connect to dashboard ..."
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')

echo "get the dashboard assigned port ..."
#kubectl get svc kubernetes-dashboard -n kubernetes-dashboard
kubectl describe svc kubernetes-dashboard -n kubernetes-dashboard

#echo "get the hubble-ui assigned port ..."
#kubectl describe svc hubble-ui -n kube-system

echo ""
echo "Completed in $SECONDS seconds"
