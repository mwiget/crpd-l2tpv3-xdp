#!/bin/bash
if [ "x86_64" != "$(uname -m)" ]; then
  echo "crpd not supported on $(uname -m)"
  exit 1
fi
if [ -z "$(docker images -q crpd)" ]; then
  echo "please install Juniper crpd container image"
  exit 1
fi
