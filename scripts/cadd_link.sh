#!/bin/bash
docker run -ti --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock --pid host addlink $1 $2
