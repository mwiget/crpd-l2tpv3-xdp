#!/bin/bash
echo "removing xdp from all interfaces ..."
/sbin/xdp_loader --dev eth0 --unload
/sbin/xdp_loader --dev eth1 --unload
/sbin/xdp_loader --dev eth2 --unload
/sbin/xdp_loader --dev eth3 --unload
/sbin/xdp_loader --dev eth4 --unload
