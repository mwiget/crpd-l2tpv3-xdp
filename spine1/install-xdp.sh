#!/bin/bash
echo "installing xdp_l2tpv3 ..."
ulimit -l 2048
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth1 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth2 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth3 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth4 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
echo "programming l2tpv3 tunnel in xdp_map ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth1
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth2
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth3
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth4
