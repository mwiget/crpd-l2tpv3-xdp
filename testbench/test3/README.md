```

=============================================================================================
test3: r1 -- veth -- r2, trafgen thru l2tpv3 tunnels using plain linux (with xdp)
=============================================================================================

9e9323d159f48bd7880580ebe28846431961e3d1437fa16b16829868e77070df
Device "veth0" does not exist.
waiting for veth0 up ...
Device "veth0" does not exist.
waiting for veth0 up ...
119: veth0@if120: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 3000 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether ce:50:93:61:ca:7e brd ff:ff:ff:ff:ff:ff link-netnsid 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        inet6 fd00:2::102  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::105  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::101  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::104  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::103  prefixlen 128  scopeid 0x0<global>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


PING fd00::1(fd00::1) 56 data bytes

--- fd00::1 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1008ms

waiting for v6 gateway to remote tunnel endpoints be reachable ...
PING fd00::1(fd00::1) 56 data bytes
64 bytes from fd00::1: icmp_seq=1 ttl=64 time=0.067 ms
64 bytes from fd00::1: icmp_seq=2 ttl=64 time=0.063 ms

--- fd00::1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1004ms
rtt min/avg/max/mdev = 0.063/0.065/0.067/0.002 ms
Tunnel 5, encap IP
  From fd00:2::105 to fd00:1::105
  Peer tunnel 5
Tunnel 4, encap IP
  From fd00:2::104 to fd00:1::104
  Peer tunnel 4
Tunnel 3, encap IP
  From fd00:2::103 to fd00:1::103
  Peer tunnel 3
Tunnel 2, encap IP
  From fd00:2::102 to fd00:1::102
  Peer tunnel 2
Tunnel 1, encap IP
  From fd00:2::101 to fd00:1::101
  Peer tunnel 1
Session 65531 in tunnel 5
  Peer session 65531, tunnel 5
  interface name: l2tpeth4
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65532 in tunnel 4
  Peer session 65532, tunnel 4
  interface name: l2tpeth3
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65533 in tunnel 3
  Peer session 65533, tunnel 3
  interface name: l2tpeth2
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65534 in tunnel 2
  Peer session 65534, tunnel 2
  interface name: l2tpeth1
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65535 in tunnel 1
  Peer session 65535, tunnel 1
  interface name: l2tpeth0
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788

installing xdp_router ...
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:veth0(ifindex:119)
 - Pinning maps in /sys/fs/bpf/veth0/

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:21 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(8): l2tp fd00:2::101 -> fd00:1::101
l2tpeth1(9): l2tp fd00:2::102 -> fd00:1::102
l2tpeth2(10): l2tp fd00:2::103 -> fd00:1::103
l2tpeth3(11): l2tp fd00:2::104 -> fd00:1::104
l2tpeth4(12): l2tp fd00:2::105 -> fd00:1::105

PING 192.168.101.1 (192.168.101.1) 56(84) bytes of data.
64 bytes from 192.168.101.1: icmp_seq=1 ttl=64 time=0.138 ms

--- 192.168.101.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.138/0.138/0.138/0.000 ms
PING 192.168.102.1 (192.168.102.1) 56(84) bytes of data.
64 bytes from 192.168.102.1: icmp_seq=1 ttl=64 time=0.108 ms

--- 192.168.102.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.108/0.108/0.108/0.000 ms
PING 192.168.103.1 (192.168.103.1) 56(84) bytes of data.
64 bytes from 192.168.103.1: icmp_seq=1 ttl=64 time=0.100 ms

--- 192.168.103.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.100/0.100/0.100/0.000 ms
PING 192.168.104.1 (192.168.104.1) 56(84) bytes of data.
64 bytes from 192.168.104.1: icmp_seq=1 ttl=64 time=0.108 ms

--- 192.168.104.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.108/0.108/0.108/0.000 ms
PING 192.168.105.1 (192.168.105.1) 56(84) bytes of data.
64 bytes from 192.168.105.1: icmp_seq=1 ttl=64 time=0.074 ms

--- 192.168.105.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.074/0.074/0.074/0.000 ms

adding ecmp routes to remote v4 loopback via l2tp tunnels ...

ip route:
10.0.0.0/8 proto static 
	nexthop via 192.168.101.1 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.1 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.1 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.1 dev l2tpeth3 weight 1 
	nexthop via 192.168.105.1 dev l2tpeth4 weight 1 
192.168.101.0/24 dev l2tpeth0 scope link 
192.168.101.1 dev l2tpeth0 proto kernel scope link src 192.168.101.2 
192.168.102.0/24 dev l2tpeth1 scope link 
192.168.102.1 dev l2tpeth1 proto kernel scope link src 192.168.102.2 
192.168.103.0/24 dev l2tpeth2 scope link 
192.168.103.1 dev l2tpeth2 proto kernel scope link src 192.168.103.2 
192.168.104.0/24 dev l2tpeth3 scope link 
192.168.104.1 dev l2tpeth3 proto kernel scope link src 192.168.104.2 
192.168.105.0/24 dev l2tpeth4 scope link 
192.168.105.1 dev l2tpeth4 proto kernel scope link src 192.168.105.2 

flood ping to remote v4 loopback ...
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- 10.0.0.1 ping statistics ---
1000 packets transmitted, 1000 received, 0% packet loss, time 41ms
rtt min/avg/max/mdev = 0.013/0.022/0.076/0.006 ms, ipg/ewma 0.041/0.022 ms

netstat -i:
Kernel Interface table
Iface      MTU    RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
l2tpeth0  2934      781      0      0 0           781      0      0      0 BMRU
l2tpeth1  2934        4      0      0 0             6      0      0      0 BMRU
l2tpeth2  2934      791      0      0 0           792      0      0      0 BMRU
l2tpeth3  2934      215      0      0 0           217      0      0      0 BMRU
l2tpeth4  2934      211      0      0 0           213      0      0      0 BMRU
lo       65536        0      0      0 0             0      0      0      0 LRU
veth0     3000     2030      0      0 0          2027      0      0      0 BMRU

xdp_stats --dev veth0 --single:

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:20 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250408
XDP_DROP              22 pkts (        10 pps)           3 Kbytes (     0 Mbits/s) period:2.250401
XDP_PASS            2019 pkts (         2 pps)         330 Kbytes (     0 Mbits/s) period:2.250406
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250412
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250419


capture flood ping to non-existing destinations ...
tcpdump: listening on veth0, link-type EN10MB (Ethernet), capture size 1500 bytes
254 packets captured
256 packets received by filter
0 packets dropped by kernel
10.11.12.1   : xmt/rcv/%loss = 1/0/100%
10.11.12.2   : xmt/rcv/%loss = 1/0/100%
10.11.12.3   : xmt/rcv/%loss = 1/0/100%
10.11.12.4   : xmt/rcv/%loss = 1/0/100%
10.11.12.5   : xmt/rcv/%loss = 1/0/100%
10.11.12.6   : xmt/rcv/%loss = 1/0/100%
10.11.12.7   : xmt/rcv/%loss = 1/0/100%
10.11.12.8   : xmt/rcv/%loss = 1/0/100%
10.11.12.9   : xmt/rcv/%loss = 1/0/100%
10.11.12.10  : xmt/rcv/%loss = 1/0/100%
10.11.12.11  : xmt/rcv/%loss = 1/0/100%
10.11.12.12  : xmt/rcv/%loss = 1/0/100%
10.11.12.13  : xmt/rcv/%loss = 1/0/100%
10.11.12.14  : xmt/rcv/%loss = 1/0/100%
10.11.12.15  : xmt/rcv/%loss = 1/0/100%
10.11.12.16  : xmt/rcv/%loss = 1/0/100%
10.11.12.17  : xmt/rcv/%loss = 1/0/100%
10.11.12.18  : xmt/rcv/%loss = 1/0/100%
10.11.12.19  : xmt/rcv/%loss = 1/0/100%
10.11.12.20  : xmt/rcv/%loss = 1/0/100%
10.11.12.21  : xmt/rcv/%loss = 1/0/100%
10.11.12.22  : xmt/rcv/%loss = 1/0/100%
10.11.12.23  : xmt/rcv/%loss = 1/0/100%
10.11.12.24  : xmt/rcv/%loss = 1/0/100%
10.11.12.25  : xmt/rcv/%loss = 1/0/100%
10.11.12.26  : xmt/rcv/%loss = 1/0/100%
10.11.12.27  : xmt/rcv/%loss = 1/0/100%
10.11.12.28  : xmt/rcv/%loss = 1/0/100%
10.11.12.29  : xmt/rcv/%loss = 1/0/100%
10.11.12.30  : xmt/rcv/%loss = 1/0/100%
10.11.12.31  : xmt/rcv/%loss = 1/0/100%
10.11.12.32  : xmt/rcv/%loss = 1/0/100%
10.11.12.33  : xmt/rcv/%loss = 1/0/100%
10.11.12.34  : xmt/rcv/%loss = 1/0/100%
10.11.12.35  : xmt/rcv/%loss = 1/0/100%
10.11.12.36  : xmt/rcv/%loss = 1/0/100%
10.11.12.37  : xmt/rcv/%loss = 1/0/100%
10.11.12.38  : xmt/rcv/%loss = 1/0/100%
10.11.12.39  : xmt/rcv/%loss = 1/0/100%
10.11.12.40  : xmt/rcv/%loss = 1/0/100%
10.11.12.41  : xmt/rcv/%loss = 1/0/100%
10.11.12.42  : xmt/rcv/%loss = 1/0/100%
10.11.12.43  : xmt/rcv/%loss = 1/0/100%
10.11.12.44  : xmt/rcv/%loss = 1/0/100%
10.11.12.45  : xmt/rcv/%loss = 1/0/100%
10.11.12.46  : xmt/rcv/%loss = 1/0/100%
10.11.12.47  : xmt/rcv/%loss = 1/0/100%
10.11.12.48  : xmt/rcv/%loss = 1/0/100%
10.11.12.49  : xmt/rcv/%loss = 1/0/100%
10.11.12.50  : xmt/rcv/%loss = 1/0/100%
10.11.12.51  : xmt/rcv/%loss = 1/0/100%
10.11.12.52  : xmt/rcv/%loss = 1/0/100%
10.11.12.53  : xmt/rcv/%loss = 1/0/100%
10.11.12.54  : xmt/rcv/%loss = 1/0/100%
10.11.12.55  : xmt/rcv/%loss = 1/0/100%
10.11.12.56  : xmt/rcv/%loss = 1/0/100%
10.11.12.57  : xmt/rcv/%loss = 1/0/100%
10.11.12.58  : xmt/rcv/%loss = 1/0/100%
10.11.12.59  : xmt/rcv/%loss = 1/0/100%
10.11.12.60  : xmt/rcv/%loss = 1/0/100%
10.11.12.61  : xmt/rcv/%loss = 1/0/100%
10.11.12.62  : xmt/rcv/%loss = 1/0/100%
10.11.12.63  : xmt/rcv/%loss = 1/0/100%
10.11.12.64  : xmt/rcv/%loss = 1/0/100%
10.11.12.65  : xmt/rcv/%loss = 1/0/100%
10.11.12.66  : xmt/rcv/%loss = 1/0/100%
10.11.12.67  : xmt/rcv/%loss = 1/0/100%
10.11.12.68  : xmt/rcv/%loss = 1/0/100%
10.11.12.69  : xmt/rcv/%loss = 1/0/100%
10.11.12.70  : xmt/rcv/%loss = 1/0/100%
10.11.12.71  : xmt/rcv/%loss = 1/0/100%
10.11.12.72  : xmt/rcv/%loss = 1/0/100%
10.11.12.73  : xmt/rcv/%loss = 1/0/100%
10.11.12.74  : xmt/rcv/%loss = 1/0/100%
10.11.12.75  : xmt/rcv/%loss = 1/0/100%
10.11.12.76  : xmt/rcv/%loss = 1/0/100%
10.11.12.77  : xmt/rcv/%loss = 1/0/100%
10.11.12.78  : xmt/rcv/%loss = 1/0/100%
10.11.12.79  : xmt/rcv/%loss = 1/0/100%
10.11.12.80  : xmt/rcv/%loss = 1/0/100%
10.11.12.81  : xmt/rcv/%loss = 1/0/100%
10.11.12.82  : xmt/rcv/%loss = 1/0/100%
10.11.12.83  : xmt/rcv/%loss = 1/0/100%
10.11.12.84  : xmt/rcv/%loss = 1/0/100%
10.11.12.85  : xmt/rcv/%loss = 1/0/100%
10.11.12.86  : xmt/rcv/%loss = 1/0/100%
10.11.12.87  : xmt/rcv/%loss = 1/0/100%
10.11.12.88  : xmt/rcv/%loss = 1/0/100%
10.11.12.89  : xmt/rcv/%loss = 1/0/100%
10.11.12.90  : xmt/rcv/%loss = 1/0/100%
10.11.12.91  : xmt/rcv/%loss = 1/0/100%
10.11.12.92  : xmt/rcv/%loss = 1/0/100%
10.11.12.93  : xmt/rcv/%loss = 1/0/100%
10.11.12.94  : xmt/rcv/%loss = 1/0/100%
10.11.12.95  : xmt/rcv/%loss = 1/0/100%
10.11.12.96  : xmt/rcv/%loss = 1/0/100%
10.11.12.97  : xmt/rcv/%loss = 1/0/100%
10.11.12.98  : xmt/rcv/%loss = 1/0/100%
10.11.12.99  : xmt/rcv/%loss = 1/0/100%
10.11.12.100 : xmt/rcv/%loss = 1/0/100%
10.11.12.101 : xmt/rcv/%loss = 1/0/100%
10.11.12.102 : xmt/rcv/%loss = 1/0/100%
10.11.12.103 : xmt/rcv/%loss = 1/0/100%
10.11.12.104 : xmt/rcv/%loss = 1/0/100%
10.11.12.105 : xmt/rcv/%loss = 1/0/100%
10.11.12.106 : xmt/rcv/%loss = 1/0/100%
10.11.12.107 : xmt/rcv/%loss = 1/0/100%
10.11.12.108 : xmt/rcv/%loss = 1/0/100%
10.11.12.109 : xmt/rcv/%loss = 1/0/100%
10.11.12.110 : xmt/rcv/%loss = 1/0/100%
10.11.12.111 : xmt/rcv/%loss = 1/0/100%
10.11.12.112 : xmt/rcv/%loss = 1/0/100%
10.11.12.113 : xmt/rcv/%loss = 1/0/100%
10.11.12.114 : xmt/rcv/%loss = 1/0/100%
10.11.12.115 : xmt/rcv/%loss = 1/0/100%
10.11.12.116 : xmt/rcv/%loss = 1/0/100%
10.11.12.117 : xmt/rcv/%loss = 1/0/100%
10.11.12.118 : xmt/rcv/%loss = 1/0/100%
10.11.12.119 : xmt/rcv/%loss = 1/0/100%
10.11.12.120 : xmt/rcv/%loss = 1/0/100%
10.11.12.121 : xmt/rcv/%loss = 1/0/100%
10.11.12.122 : xmt/rcv/%loss = 1/0/100%
10.11.12.123 : xmt/rcv/%loss = 1/0/100%
10.11.12.124 : xmt/rcv/%loss = 1/0/100%
10.11.12.125 : xmt/rcv/%loss = 1/0/100%
10.11.12.126 : xmt/rcv/%loss = 1/0/100%
10.11.12.127 : xmt/rcv/%loss = 1/0/100%
10.11.12.128 : xmt/rcv/%loss = 1/0/100%
10.11.12.129 : xmt/rcv/%loss = 1/0/100%
10.11.12.130 : xmt/rcv/%loss = 1/0/100%
10.11.12.131 : xmt/rcv/%loss = 1/0/100%
10.11.12.132 : xmt/rcv/%loss = 1/0/100%
10.11.12.133 : xmt/rcv/%loss = 1/0/100%
10.11.12.134 : xmt/rcv/%loss = 1/0/100%
10.11.12.135 : xmt/rcv/%loss = 1/0/100%
10.11.12.136 : xmt/rcv/%loss = 1/0/100%
10.11.12.137 : xmt/rcv/%loss = 1/0/100%
10.11.12.138 : xmt/rcv/%loss = 1/0/100%
10.11.12.139 : xmt/rcv/%loss = 1/0/100%
10.11.12.140 : xmt/rcv/%loss = 1/0/100%
10.11.12.141 : xmt/rcv/%loss = 1/0/100%
10.11.12.142 : xmt/rcv/%loss = 1/0/100%
10.11.12.143 : xmt/rcv/%loss = 1/0/100%
10.11.12.144 : xmt/rcv/%loss = 1/0/100%
10.11.12.145 : xmt/rcv/%loss = 1/0/100%
10.11.12.146 : xmt/rcv/%loss = 1/0/100%
10.11.12.147 : xmt/rcv/%loss = 1/0/100%
10.11.12.148 : xmt/rcv/%loss = 1/0/100%
10.11.12.149 : xmt/rcv/%loss = 1/0/100%
10.11.12.150 : xmt/rcv/%loss = 1/0/100%
10.11.12.151 : xmt/rcv/%loss = 1/0/100%
10.11.12.152 : xmt/rcv/%loss = 1/0/100%
10.11.12.153 : xmt/rcv/%loss = 1/0/100%
10.11.12.154 : xmt/rcv/%loss = 1/0/100%
10.11.12.155 : xmt/rcv/%loss = 1/0/100%
10.11.12.156 : xmt/rcv/%loss = 1/0/100%
10.11.12.157 : xmt/rcv/%loss = 1/0/100%
10.11.12.158 : xmt/rcv/%loss = 1/0/100%
10.11.12.159 : xmt/rcv/%loss = 1/0/100%
10.11.12.160 : xmt/rcv/%loss = 1/0/100%
10.11.12.161 : xmt/rcv/%loss = 1/0/100%
10.11.12.162 : xmt/rcv/%loss = 1/0/100%
10.11.12.163 : xmt/rcv/%loss = 1/0/100%
10.11.12.164 : xmt/rcv/%loss = 1/0/100%
10.11.12.165 : xmt/rcv/%loss = 1/0/100%
10.11.12.166 : xmt/rcv/%loss = 1/0/100%
10.11.12.167 : xmt/rcv/%loss = 1/0/100%
10.11.12.168 : xmt/rcv/%loss = 1/0/100%
10.11.12.169 : xmt/rcv/%loss = 1/0/100%
10.11.12.170 : xmt/rcv/%loss = 1/0/100%
10.11.12.171 : xmt/rcv/%loss = 1/0/100%
10.11.12.172 : xmt/rcv/%loss = 1/0/100%
10.11.12.173 : xmt/rcv/%loss = 1/0/100%
10.11.12.174 : xmt/rcv/%loss = 1/0/100%
10.11.12.175 : xmt/rcv/%loss = 1/0/100%
10.11.12.176 : xmt/rcv/%loss = 1/0/100%
10.11.12.177 : xmt/rcv/%loss = 1/0/100%
10.11.12.178 : xmt/rcv/%loss = 1/0/100%
10.11.12.179 : xmt/rcv/%loss = 1/0/100%
10.11.12.180 : xmt/rcv/%loss = 1/0/100%
10.11.12.181 : xmt/rcv/%loss = 1/0/100%
10.11.12.182 : xmt/rcv/%loss = 1/0/100%
10.11.12.183 : xmt/rcv/%loss = 1/0/100%
10.11.12.184 : xmt/rcv/%loss = 1/0/100%
10.11.12.185 : xmt/rcv/%loss = 1/0/100%
10.11.12.186 : xmt/rcv/%loss = 1/0/100%
10.11.12.187 : xmt/rcv/%loss = 1/0/100%
10.11.12.188 : xmt/rcv/%loss = 1/0/100%
10.11.12.189 : xmt/rcv/%loss = 1/0/100%
10.11.12.190 : xmt/rcv/%loss = 1/0/100%
10.11.12.191 : xmt/rcv/%loss = 1/0/100%
10.11.12.192 : xmt/rcv/%loss = 1/0/100%
10.11.12.193 : xmt/rcv/%loss = 1/0/100%
10.11.12.194 : xmt/rcv/%loss = 1/0/100%
10.11.12.195 : xmt/rcv/%loss = 1/0/100%
10.11.12.196 : xmt/rcv/%loss = 1/0/100%
10.11.12.197 : xmt/rcv/%loss = 1/0/100%
10.11.12.198 : xmt/rcv/%loss = 1/0/100%
10.11.12.199 : xmt/rcv/%loss = 1/0/100%
10.11.12.200 : xmt/rcv/%loss = 1/0/100%
10.11.12.201 : xmt/rcv/%loss = 1/0/100%
10.11.12.202 : xmt/rcv/%loss = 1/0/100%
10.11.12.203 : xmt/rcv/%loss = 1/0/100%
10.11.12.204 : xmt/rcv/%loss = 1/0/100%
10.11.12.205 : xmt/rcv/%loss = 1/0/100%
10.11.12.206 : xmt/rcv/%loss = 1/0/100%
10.11.12.207 : xmt/rcv/%loss = 1/0/100%
10.11.12.208 : xmt/rcv/%loss = 1/0/100%
10.11.12.209 : xmt/rcv/%loss = 1/0/100%
10.11.12.210 : xmt/rcv/%loss = 1/0/100%
10.11.12.211 : xmt/rcv/%loss = 1/0/100%
10.11.12.212 : xmt/rcv/%loss = 1/0/100%
10.11.12.213 : xmt/rcv/%loss = 1/0/100%
10.11.12.214 : xmt/rcv/%loss = 1/0/100%
10.11.12.215 : xmt/rcv/%loss = 1/0/100%
10.11.12.216 : xmt/rcv/%loss = 1/0/100%
10.11.12.217 : xmt/rcv/%loss = 1/0/100%
10.11.12.218 : xmt/rcv/%loss = 1/0/100%
10.11.12.219 : xmt/rcv/%loss = 1/0/100%
10.11.12.220 : xmt/rcv/%loss = 1/0/100%
10.11.12.221 : xmt/rcv/%loss = 1/0/100%
10.11.12.222 : xmt/rcv/%loss = 1/0/100%
10.11.12.223 : xmt/rcv/%loss = 1/0/100%
10.11.12.224 : xmt/rcv/%loss = 1/0/100%
10.11.12.225 : xmt/rcv/%loss = 1/0/100%
10.11.12.226 : xmt/rcv/%loss = 1/0/100%
10.11.12.227 : xmt/rcv/%loss = 1/0/100%
10.11.12.228 : xmt/rcv/%loss = 1/0/100%
10.11.12.229 : xmt/rcv/%loss = 1/0/100%
10.11.12.230 : xmt/rcv/%loss = 1/0/100%
10.11.12.231 : xmt/rcv/%loss = 1/0/100%
10.11.12.232 : xmt/rcv/%loss = 1/0/100%
10.11.12.233 : xmt/rcv/%loss = 1/0/100%
10.11.12.234 : xmt/rcv/%loss = 1/0/100%
10.11.12.235 : xmt/rcv/%loss = 1/0/100%
10.11.12.236 : xmt/rcv/%loss = 1/0/100%
10.11.12.237 : xmt/rcv/%loss = 1/0/100%
10.11.12.238 : xmt/rcv/%loss = 1/0/100%
10.11.12.239 : xmt/rcv/%loss = 1/0/100%
10.11.12.240 : xmt/rcv/%loss = 1/0/100%
10.11.12.241 : xmt/rcv/%loss = 1/0/100%
10.11.12.242 : xmt/rcv/%loss = 1/0/100%
10.11.12.243 : xmt/rcv/%loss = 1/0/100%
10.11.12.244 : xmt/rcv/%loss = 1/0/100%
10.11.12.245 : xmt/rcv/%loss = 1/0/100%
10.11.12.246 : xmt/rcv/%loss = 1/0/100%
10.11.12.247 : xmt/rcv/%loss = 1/0/100%
10.11.12.248 : xmt/rcv/%loss = 1/0/100%
10.11.12.249 : xmt/rcv/%loss = 1/0/100%
10.11.12.250 : xmt/rcv/%loss = 1/0/100%
10.11.12.251 : xmt/rcv/%loss = 1/0/100%
10.11.12.252 : xmt/rcv/%loss = 1/0/100%
10.11.12.253 : xmt/rcv/%loss = 1/0/100%
10.11.12.254 : xmt/rcv/%loss = 1/0/100%
-rw-r--r-- 1 root    root    257206 Jul 26 09:52 flood.cfg
-rw-r--r-- 1 tcpdump tcpdump  45320 Jul 26 09:52 flood.pcap
   508 packets to schedule
 82464 bytes in total
Running! Hang up with ^C!


xdp_stats --dev veth0 --single:

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:20 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250338
XDP_DROP              23 pkts (         0 pps)           4 Kbytes (     0 Mbits/s) period:2.250354
XDP_PASS            2025 pkts (         0 pps)         331 Kbytes (     0 Mbits/s) period:2.250347
XDP_TX           2972687 pkts (    414995 pps)      487520 Kbytes (   544 Mbits/s) period:2.250347
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250348


bwm-ng -u packets -o plain -c 1:
bwm-ng v0.6.2 (delay 0.500s); input: /proc/net/dev          iface                    Rx                   Tx               Total
==============================================================================
          veth0:       429114.00 P/s        442240.00 P/s        871354.00 P/s
       l2tpeth3:            0.00 P/s             0.00 P/s             0.00 P/s
       l2tpeth0:            0.00 P/s             0.00 P/s             0.00 P/s
       l2tpeth2:            0.00 P/s             0.00 P/s             0.00 P/s
       l2tpeth1:            0.00 P/s             0.00 P/s             0.00 P/s
       l2tpeth4:            0.00 P/s             0.00 P/s             0.00 P/s
             lo:            0.00 P/s             0.00 P/s             0.00 P/s
------------------------------------------------------------------------------
          total:       429114.00 P/s        442240.00 P/s        871354.00 P/s


SUCCESS!
SUMMARY test3: r1 -- veth -- r2, trafgen thru l2tpv3 tunnels using plain linux (with xdp): PASS
```
