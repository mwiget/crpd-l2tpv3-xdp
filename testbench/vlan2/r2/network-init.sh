#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::12/128 dev lo
ip -6 addr add fd00:2::12/128 dev lo

ip addr add 10.0.0.12/32 dev lo

interface=enp4s0f1
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip -6 addr add fd00:2222::2/64 dev $interface
ip addr add 169.254.1.12/16 dev $interface
while ! ping6 -c 1 -W 1 fd00:2222::1; do
  echo "waiting for fd00:2222::1 ..." && sleep 1
done
ip -6 route add fd00:1::2/128 via fd00:2222::1

while ! ping6 -c 1 -W 1 fd00:1::2; do
  echo "waiting for fd00:1::2 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00:2::12 remote fd00:1::2
ip l2tp add session tunnel_id 2 session_id 65535 peer_session_id 65534 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.2 peer 192.168.102.1 dev l2tpeth0
ip link set dev l2tpeth0 up
ip route add 10.0.0.0/24 via 192.168.102.1 dev l2tpeth0 src 10.0.0.12

echo ""
echo "installing xdp_l2tpv3 on $interface ..."
ulimit -l 1024
/sbin/xdp_loader --dev $interface -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "set xdp tunnel map on $interface ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev $interface

echo "all set."
tail -f /dev/null
