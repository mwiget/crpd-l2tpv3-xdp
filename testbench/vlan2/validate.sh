#!/bin/bash

SECONDS=0

while true; do
  docker exec -ti r1 ping -fc 3 10.0.0.1 && break
  echo "$SECONDS: waiting to reach spine1 from r1 via l2tpv3 ..."
  sleep 1
done
echo ""

while true; do
  docker exec -ti r1 ping -fc 3 10.0.0.12 && break
  echo "$SECONDS: waiting to reach r2 from r1 via l2tpv3 ..."
  sleep 1
done
echo ""

while true; do
  docker exec -ti r1 ping -fc 3 10.0.0.13 && break
  echo "$SECONDS: waiting to reach r3 from r1 via l2tpv3 ..."
  sleep 1
done

while true; do
  docker exec -ti r1 ping -fc 3 10.0.0.14 && break
  echo "$SECONDS: waiting to reach r4 from r1 via l2tpv3 ..."
  sleep 1
done

while true; do
  docker exec -ti r3 ping -fc 3 10.0.0.14 && break
  echo "$SECONDS: waiting to reach r4 from r3 via l2tpv3 ..."
  sleep 1
done

if ! docker exec -ti r1 xdp_stats -s -d eth0 ; then
  echo ""
  echo "xdp program missing on r1 !!"
  exit 1
fi

if ! docker exec -ti spine1 xdp_stats -s -d eth0 ; then
  echo ""
  echo "xdp program missing on spine1 !!"
  exit 1
fi

echo ""
echo "success in $SECONDS seconds"
