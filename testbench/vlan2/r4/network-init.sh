#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::14/128 dev lo
ip -6 addr add fd00:4::14/128 dev lo

ip addr add 10.0.0.14/32 dev lo

interface=enp11s0f1
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip link add link $interface name vlan400 type vlan id 400
ip -6 addr add fd00:4444::2/64 dev vlan400
ip link set dev vlan400 up
ip addr add 169.254.1.14/16 dev vlan400
while ! ping6 -c 1 -W 1 fd00:4444::1; do
  echo "waiting for fd00:4444::1 ..." && sleep 1
done
ip -6 route add fd00:1::4/128 via fd00:4444::1

while ! ping6 -c 1 -W 1 fd00:1::4; do
  echo "waiting for fd00:1::4 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00:4::14 remote fd00:1::4
ip l2tp add session tunnel_id 4 session_id 65535 peer_session_id 65532 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.104.2 peer 192.168.104.1 dev l2tpeth0
ip link set dev l2tpeth0 up
ip route add 10.0.0.0/24 via 192.168.104.1 dev l2tpeth0 src 10.0.0.14

echo ""
echo "installing xdp_l2tpv3 on $interface ..."
ulimit -l 1024
/sbin/xdp_loader --dev $interface -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
echo ""
echo "set xdp tunnel map on $interface ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev $interface

echo "all set."
tail -f /dev/null
