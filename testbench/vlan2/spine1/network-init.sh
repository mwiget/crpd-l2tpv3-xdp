#!/bin/bash

cookie="1122334455667788"
set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::1/128 dev lo
ip -6 addr add fd00:1::1/128 dev lo
ip -6 addr add fd00:1::2/128 dev lo
ip -6 addr add fd00:1::3/128 dev lo
ip -6 addr add fd00:1::4/128 dev lo

ip addr add 10.0.0.1/32 dev lo

interface=eth0
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip -6 addr add fd00:1111::1/64 dev $interface
ip addr add 169.254.1.1/16 dev $interface

interface=enp4s0f0
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip -6 addr add fd00:2222::1/64 dev $interface
ip addr add 169.254.1.1/16 dev $interface

interface=enp12s0f0
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip link add link $interface name vlan300 type vlan id 300
ip -6 addr add fd00:3333::1/64 dev vlan300
ip link set dev vlan300 up
ip addr add 169.254.1.1/16 dev vlan300

interface=enp11s0f0
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip link add link $interface name vlan400 type vlan id 400
ip -6 addr add fd00:4444::1/64 dev vlan400
ip link set dev vlan400 up
ip addr add 169.254.1.1/16 dev vlan400

while ! ping6 -c 1 -W 1 fd00:1111::2; do
  echo "waiting for fd00:1111::2 ..." && sleep 1
done
ip -6 route add fd00:1::11/128 via fd00:1111::2

while ! ping6 -c 1 -W 1 fd00:2222::2; do
  echo "waiting for fd00:2222::2 ..." && sleep 1
done
ip -6 route add fd00:2::12/128 via fd00:2222::2

while ! ping6 -c 1 -W 1 fd00:3333::2; do
  echo "waiting for fd00:3333::2 ..." && sleep 1
done
ip -6 route add fd00:3::13/128 via fd00:3333::2

while ! ping6 -c 1 -W 1 fd00:4444::2; do
  echo "waiting for fd00:4444::2 ..." && sleep 1
done
ip -6 route add fd00:4::14/128 via fd00:4444::2

# set (dummy) link local ip's, so ip4 routing works for decap packets

while ! ping6 -c 1 -W 1 fd00:1::11; do
  echo "waiting for fd00:1::11 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00:1::1 remote fd00:1::11
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.101.1/24 peer 192.168.101.2 dev l2tpeth0
ip link set dev l2tpeth0 up
ip route add 192.168.101.0/24 dev l2tpeth0

while ! ping6 -c 1 -W 1 fd00:2::12; do
  echo "waiting for fd00:2::12 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00:1::2 remote fd00:2::12
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.1 peer 192.168.102.2 dev l2tpeth1
ip link set dev l2tpeth1 up
ip route add 192.168.102.0/24 dev l2tpeth1

while ! ping6 -c 1 -W 1 fd00:3::13; do
  echo "waiting for fd00:3::13 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00:1::3 remote fd00:3::13
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.103.1 peer 192.168.103.2 dev l2tpeth2
ip link set dev l2tpeth2 up
ip route add 192.168.103.0/24 dev l2tpeth2

while ! ping6 -c 1 -W 1 fd00:4::14; do
  echo "waiting for fd00:4::14 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00:1::4 remote fd00:4::14
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.104.1 peer 192.168.104.2 dev l2tpeth3
ip link set dev l2tpeth3 up
ip route add 192.168.104.0/24 dev l2tpeth3

ip route add 10.0.0.11/32 via 192.168.101.2 dev l2tpeth0 src 10.0.0.1
ip route add 10.0.0.12/32 via 192.168.102.2 dev l2tpeth1 src 10.0.0.1
ip route add 10.0.0.13/32 via 192.168.103.2 dev l2tpeth2 src 10.0.0.1
ip route add 10.0.0.14/32 via 192.168.104.2 dev l2tpeth3 src 10.0.0.1

ip route add 10.0.0.24/32 via 192.168.104.2 dev l2tpeth3 src 10.0.0.1


echo ""
echo "installing xdp_l2tpv3 ..."
ulimit -l 1024
/sbin/xdp_loader --dev eth0 -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev enp4s0f0 -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev enp12s0f0 -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev enp11s0f0 -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "programming l2tpv3 tunnel in xdp_map ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0
/root/tunnels.sh | /sbin/xdp_tunnels --dev enp4s0f0
/root/tunnels.sh | /sbin/xdp_tunnels --dev enp12s0f0
/root/tunnels.sh | /sbin/xdp_tunnels --dev enp11s0f0

tail -f /dev/null
