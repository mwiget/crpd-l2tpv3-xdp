#!/bin/bash
PCI1="0000:03:00.0"
PCI2="0000:03:00.1"
PCI3="0000:04:00.0"
PCI4="0000:04:00.1"

sudo python3 dpdk/usertools/dpdk-devbind.py -b ixgbe $PCI1 $PCI2 $PCI3 $PCI4
