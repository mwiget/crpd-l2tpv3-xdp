tmux new-session -d 'bash'
tmux split-window -h 'mosh xeon git/crpd-l2tpv3-xdp/testbench/techfest1/run-pktgen.sh;bash'
tmux split-window -v 'docker stats --format "table {{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}"'
tmux split-window -v 'htop;bash'
tmux select-pane -U
tmux select-pane -U
tmux resize-pane -y 30
tmux select-pane -L 
tmux split-window -v 'docker exec -ti crpd1 bwm-ng -u packets -I enp9s0f0,enp11s0f0;bash'
tmux split-window -v 'docker exec -ti crpd2 bwm-ng -I enp9s0f1,enp11s0f2 -u bits -d;bash'
tmux select-pane -U
tmux select-pane -U
tmux resize-pane -y 20
tmux -2 attach-session -d
