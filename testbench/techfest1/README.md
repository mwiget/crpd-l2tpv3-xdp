# Techfest 2020 demo

```mermaid
graph LR
pktgen1 ---| 10G | crpd1((crpd1))
pktgen2 ---| 10G | crpd1
crpd2 ---| 10G | pktgen3
crpd2 ---| 10G | pktgen4
crpd1 -.-|l2tpeth0| crpd2
crpd1 -.-|l2tpeth1| crpd2
crpd1 ===| 40G | crpd2((crpd2))
crpd1 -.-|l2tpeth2| crpd2
crpd1 -.-|l2tpeth3| crpd2
```

Traffic sent bi-directionally between 2x10GE ports on either cRPD instance, which are interconnected via a 40GE loopback cable.
Both crpd instances run on the same server.

L2TPv3 tunnel endpoints are IPv6 loopbacks, announced via ISIS. 40Ge link only uses link local IPv6 addresses.

4 tunnels are established between unique v6 loopback IP's and a BGP v4 sessions share v4 routes for the generated traffic.
BGP multipath and per-packet load-balance config is used to announced ECMP routes to the linux kernel in each instance:

```
root@crpd1> show bgp summary 
Threading mode: BGP I/O
Groups: 1 Peers: 4 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                      36         20          0          0          0          0
inet6.0              
                       0          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
192.168.101.2    4259905002         73         74       0       0       31:16 Establ
  inet.0: 5/9/9/0
192.168.102.2    4259905002         72         73       0       0       31:14 Establ
  inet.0: 5/9/9/0
192.168.103.2    4259905002         72         73       0       0       31:10 Establ
  inet.0: 5/9/9/0
192.168.104.2    4259905002         71         73       0       0       31:06 Establ
  inet.0: 5/9/9/0

root@crpd1> show isis adjacency 
Interface             System         L State        Hold (secs) SNPA
enp9s0f0              crpd2          2  Up                   24

root@crpd1> show route 10.0.0.2    

inet.0: 20 destinations, 51 routes (20 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

10.0.0.2/32        *[BGP/170] 00:31:19, localpref 100, from 192.168.101.2
                      AS path: 4259905002 I, validation-state: unverified
                       to 192.168.101.2 via l2tpeth0
                       to 192.168.102.2 via l2tpeth1
                    >  to 192.168.103.2 via l2tpeth2
                       to 192.168.104.2 via l2tpeth3
                    [BGP/170] 00:31:27, localpref 100
                      AS path: 4259905002 I, validation-state: unverified
                    >  to 192.168.102.2 via l2tpeth1
                    [BGP/170] 00:31:23, localpref 100
                      AS path: 4259905002 I, validation-state: unverified
                    >  to 192.168.103.2 via l2tpeth2
                    [BGP/170] 00:31:19, localpref 100
                      AS path: 4259905002 I, validation-state: unverified
                    >  to 192.168.104.2 via l2tpeth3
```


```
root@crpd1> show configuration policy-options policy-statement loadbalance 
then {
    load-balance per-packet;
}

root@crpd1> show configuration protocols bgp 
family inet {
    unicast;
}
family inet6 {
    unicast;
}
group eBGP {
    type external;
    import accept-all;
    family inet {
        unicast;
    }
    export accept-all;
    neighbor 192.168.101.2 {
        multihop;
        local-address 192.168.101.1;
        peer-as 65000.65002;
    }
    neighbor 192.168.102.2 {
        multihop;
        local-address 192.168.102.1;
        peer-as 65000.65002;
    }
    neighbor 192.168.103.2 {
        multihop;
        local-address 192.168.103.1;
        peer-as 65000.65002;
    }
    neighbor 192.168.104.2 {
        multihop;
        local-address 192.168.104.1;
        peer-as 65000.65002;
    }
}
multipath;
```


```
root@crpd1> show isis adj

Interface             System         L State        Hold (secs) SNPA
enp9s0f0              crpd2          2  Up                   26
```

```
root@cprd1> show bgp summary

Threading mode: BGP I/O
Groups: 1 Peers: 4 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                      36         20          0          0          0          0
inet6.0              
                       0          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
192.168.101.2    4259905002        167        168       0       0     1:13:56 Establ
  inet.0: 5/9/9/0
192.168.102.2    4259905002        167        168       0       0     1:13:54 Establ
  inet.0: 5/9/9/0
192.168.103.2    4259905002        166        167       0       0     1:13:50 Establ
  inet.0: 5/9/9/0
192.168.104.2    4259905002        165        167       0       0     1:13:46 Establ
  inet.0: 5/9/9/0
```

```
root@cprd1# ip route

10.0.0.2 proto 22 
	nexthop via 192.168.101.2 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.2 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.2 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.2 dev l2tpeth3 weight 1 
10.1.0.0/16 via 10.251.0.2 dev enp11s0f0 proto 22 
10.2.0.0/16 via 10.252.0.2 dev enp11s0f1 proto 22 
10.3.0.0/16 proto 22 
	nexthop via 192.168.101.2 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.2 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.2 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.2 dev l2tpeth3 weight 1 
10.4.0.0/16 proto 22 
	nexthop via 192.168.101.2 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.2 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.2 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.2 dev l2tpeth3 weight 1 
10.251.0.0/24 dev enp11s0f0 proto kernel scope link src 10.251.0.1 
10.252.0.0/24 dev enp11s0f1 proto kernel scope link src 10.252.0.1 
10.253.0.0/24 proto 22 
	nexthop via 192.168.101.2 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.2 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.2 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.2 dev l2tpeth3 weight 1 
10.254.0.0/24 proto 22 
	nexthop via 192.168.101.2 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.2 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.2 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.2 dev l2tpeth3 weight 1 
192.168.101.0/24 dev l2tpeth0 proto kernel scope link src 192.168.101.1 
192.168.102.0/24 dev l2tpeth1 proto kernel scope link src 192.168.102.1 
192.168.103.0/24 dev l2tpeth2 proto kernel scope link src 192.168.103.1 
192.168.104.0/24 dev l2tpeth3 proto kernel scope link src 192.168.104.1 

```
