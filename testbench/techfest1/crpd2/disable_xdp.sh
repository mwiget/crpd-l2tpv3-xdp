#!/bin/bash

interfaces="enp9s0f1 enp11s0f2 enp11s0f3"
set -e

echo ""
echo "remove xdp_router ..."
for int in $interfaces; do
  echo $int ...
  /sbin/xdp_loader --dev $int --unload
done
