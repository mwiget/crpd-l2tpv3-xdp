#!/bin/bash

echo "generating static /32 routes for crpd1 and crpd2 ..."
./create_routes.sh 1 2 > crpd1/static-routes.txt
./create_routes.sh 2 3 > crpd2/static-routes.txt
wc -l crpd?/static-routes.txt
ls -l crpd?/static-routes.txt

echo "pushing routes to crpd1 and crpd2 ..."
docker exec -ti crpd1 cli -c "conf; load set /config/static-routes.txt; commit and-quit" &
docker exec -ti crpd2 cli -c "conf; load set /config/static-routes.txt; commit and-quit"
