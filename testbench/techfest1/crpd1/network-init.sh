#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

until ip link show enp11s0f1 up; do
   echo "waiting for interface enp11s0f1 up ..."
   sleep 1
done

#ip route del default

ip -6 addr add fd00::1/128 dev lo
ip -6 addr add fd00::11/128 dev lo
ip -6 addr add fd00::12/128 dev lo
ip -6 addr add fd00::13/128 dev lo
ip -6 addr add fd00::14/128 dev lo
ip addr add 10.0.0.1/32 dev lo

ip -6 addr add fd00:1::1/64 dev enp11s0f0
ip -6 addr add fd00:2::1/64 dev enp11s0f1

ip addr add 10.251.0.1/24 dev enp11s0f0
ip addr add 10.252.0.1/24 dev enp11s0f1

#echo "starting avah-autoipd for enp9s0f0 ..."
#/usr/sbin/avahi-autoipd --no-drop-root --daemonize enp9s0f0

echo "adding static lladdr v6 neighbor and arp ..."
ip -6 neigh add fd00:1::2 lladdr 00:1b:21:99:cc:14 nud permanent dev enp11s0f0
arp -s 10.251.0.2 00:1b:21:99:cc:14
ip -6 neigh add fd00:2::2 lladdr 00:1b:21:99:cc:15 nud permanent dev enp11s0f1
arp -s 10.252.0.2 00:1b:21:99:cc:15

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00::21; do
  echo "waiting for fd00::21 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00::11 remote fd00::21
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.101.1/24 dev l2tpeth0
ip link set dev l2tpeth0 up

ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00::12 remote fd00::22
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.1/24 dev l2tpeth1
ip link set dev l2tpeth1 up

ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00::13 remote fd00::23
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.103.1/24 dev l2tpeth2
ip link set dev l2tpeth2 up

ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00::14 remote fd00::24
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.104.1/24 dev l2tpeth3
ip link set dev l2tpeth3 up

/config/enable_xdp.sh

cores=5
ethtool -G enp9s0f0 rx 4096 || true
ethtool -G enp11s0f0 rx 4096 || true
ethtool -G enp11s0f1 rx 4096 || true

echo "all set."
