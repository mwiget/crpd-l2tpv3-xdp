#!/bin/bash

prog=${1:-xdp_l2tpv3}

interfaces="enp9s0f0 enp11s0f0 enp11s0f1"

set -e

echo ""
echo "installing xdp_router ..."
ulimit -l 1024
for int in $interfaces; do
  echo $int ...
  /sbin/xdp_loader --dev $int --auto-mode --force --filename /root/xdp_router.o --progsec $prog
  /root/tunnels.sh | /sbin/xdp_tunnels --dev $int
done
