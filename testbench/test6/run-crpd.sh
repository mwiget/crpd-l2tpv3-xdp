#!/bin/bash
TESTCASE="$(basename $PWD): r1 -- 10g link -- r2, flood thru l2tpv3 tunnels using plain linux (with xdp)"
echo ""
echo "============================================================================================="
echo "$TESTCASE"
echo "============================================================================================="
echo ""

trap "docker kill r1 r2 >/dev/null 2>&1" EXIT

set -e

docker kill r1 r2 || true

docker run -ti --rm --privileged \
   -d \
   -v ${PWD}/launch_r1.sh:/launch.sh:ro \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r1 --hostname r1 \
   l2tpv3router /launch.sh enp101s0f0

./add_phy.sh r1 enp101s0f0
