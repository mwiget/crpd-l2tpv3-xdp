#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fc00:1::1/128 dev lo
ip -6 addr add fc00:1::2/128 dev lo
ip -6 addr add fc00:1::3/128 dev lo
ip -6 addr add fc00:1::4/128 dev lo

ip addr add 10.0.0.1/32 dev lo

until ip link show ens8f0v0 up; do
  echo "waiting for ens8f0v0 up ..."
  sleep 1
done

ethtool --offload ens8f0v0 rxvlan off txvlan off || echo "cant disable vlan offload on ens8f0v0, must be done on ens8f0"

# add vlan 129 on ens8f0v0
ip link add link ens8f0v0 name vlan129 type vlan id 129
ip link set dev vlan129 up
ip addr add 169.254.1.1/16 dev vlan129

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fc00:1::11; do
  echo "waiting for fc00:1::11 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fc00:1::1 remote fc00:1::11
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.20.1/24 dev l2tpeth0
ip link set dev l2tpeth0 up

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fc00:1::12; do
  echo "waiting for fc00:1::12 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fc00:1::2 remote fc00:1::12
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.21.1/24 dev l2tpeth1
ip link set dev l2tpeth1 up

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fc00:1::13; do
  echo "waiting for fc00:1::13 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fc00:1::3 remote fc00:1::13
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.70.1/24 dev l2tpeth2
ip link set dev l2tpeth2 up

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fc00:1::14; do
  echo "waiting for fc00:1::14 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fc00:1::4 remote fc00:1::14
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.71.1/24 dev l2tpeth3
ip link set dev l2tpeth3 up

echo ""
echo "installing xdp_l2tpv3 ..."
ulimit -l 2048
/sbin/xdp_loader --dev ens8f0v0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "programming l2tpv3 tunnel in xdp_map ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev ens8f0v0
