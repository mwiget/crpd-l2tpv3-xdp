#!/bin/bash

set -e

until ip link show eth0 up; do
  echo "waiting for eth0 up ..."
  sleep 1
done

ip route del default
ip route add default via 192.168.20.1

tail -f /dev/null
