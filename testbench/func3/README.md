# Functional testing of l2tpv3 tunnels using xdp

Requirements:

Container l2tpv3router built (which requires xdpbuild)

```
$ ./run.sh
==============================================================================
func3: l2tpv3 tunnels r1-r2-r3 with XDP (no cRPD)
==============================================================================

+--------+               +--------+               +--------+
|   r1   |               |   r2   |               |   r3   |
|        |--fd00:1::/64--|        |--fd00:2::/64--|        |
|10.0.0.1|               |10.0.0.2|               |10.0.0.3|
+--------+               +--------+               +--------+

r1
r2
r3
4b0e2830c5ebc1b2162f7fe067048b46d04370e94c18dafa36b082d5624e7d39
ac48a1b5c98e8e58d9627b36b1b946c3c434c42e2d95b872fa2b59c474dc3845
54e3212418755949212059f9009215e13b1bfd0d8f55af78a586f87f3227f496
r1 4b0e2830c5eb
r2 ac48a1b5c98e
r1 has pid 30016
r2 has pid 30098
r1 has 0 eth interfaces
r2 has 0 eth interfaces
vr1-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether f2:66:12:6d:04:56  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vr2-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether b2:a3:e8:e7:e1:ce  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

setting mtu ...
moving endpoints to netns ...
bringing links up ...
r1:eth0 === r2:eth0
r2 ac48a1b5c98e
r3 54e321241875
r2 has pid 30098
r3 has pid 30169
r2 has 1 eth interfaces
r3 has 0 eth interfaces
vr2-1: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 0a:90:92:96:62:21  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vr3-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 0e:42:9a:f0:7e:bb  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

setting mtu ...
moving endpoints to netns ...
bringing links up ...
r2:eth1 === r3:eth0
validation finished in 3 seconds

launching shell for interactive use. Exit to terminate containers

Device "eth0" does not exist.
waiting for eth0 up ...
Device "eth0" does not exist.
waiting for eth0 up ...
143: eth0@if142: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 3000 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether f2:66:12:6d:04:56 brd ff:ff:ff:ff:ff:ff link-netnsid 1
fd00::12 dev lo proto kernel metric 256 pref medium
fd00::21 via fd00:1::2 dev eth0 src fd00::12 metric 1024 pref medium
fd00:1::/64 dev eth0 proto kernel metric 256 pref medium
fe80::/64 dev eth0 proto kernel metric 256 pref medium
waiting for remote tunnel endpoint be reachable ...
PING fd00::21(fd00::21) 56 data bytes

--- fd00::21 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

waiting for fd00::21 ...
PING fd00::21(fd00::21) 56 data bytes
64 bytes from fd00::21: icmp_seq=1 ttl=64 time=0.093 ms

--- fd00::21 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.093/0.093/0.093/0.000 ms

installing xdp_router ...
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:eth0(ifindex:143)
 - Pinning maps in /sys/fs/bpf/eth0/

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:129 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(2): l2tp fd00::12 -> fd00::21
Device "eth1" does not exist.
waiting for eth1 up ...
Device "eth1" does not exist.
waiting for eth1 up ...
145: eth1@if144: <BROADCAST,MULTICAST,UP> mtu 3000 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 0a:90:92:96:62:21 brd ff:ff:ff:ff:ff:ff link-netnsid 2
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 3000
        inet6 fe80::b0a3:e8ff:fee7:e1ce  prefixlen 64  scopeid 0x20<link>
        inet6 fd00:1::2  prefixlen 64  scopeid 0x0<global>
        ether b2:a3:e8:e7:e1:ce  txqueuelen 1000  (Ethernet)
        RX packets 3  bytes 306 (306.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2  bytes 196 (196.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eth1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 3000
        inet6 fd00:2::2  prefixlen 64  scopeid 0x0<global>
        ether 0a:90:92:96:62:21  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

fd00::12 via fd00:1::1 dev eth0 src fd00::21 metric 1024 pref medium
fd00::21 dev lo proto kernel metric 256 pref medium
fd00::23 dev lo proto kernel metric 256 pref medium
fd00::32 via fd00:2::3 dev eth1 src fd00::23 metric 1024 pref medium
fd00:1::/64 dev eth0 proto kernel metric 256 pref medium
fd00:2::/64 dev eth1 proto kernel metric 256 pref medium
fe80::/64 dev eth0 proto kernel metric 256 pref medium
fe80::/64 dev eth1 proto kernel metric 256 pref medium
waiting for remote tunnel endpoint be reachable ...
PING fd00::12(fd00::12) 56 data bytes

--- fd00::12 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

waiting for fd00::12 ...
PING fd00::12(fd00::12) 56 data bytes
64 bytes from fd00::12: icmp_seq=1 ttl=64 time=29.7 ms

--- fd00::12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 29.748/29.748/29.748/0.000 ms
PING fd00::32(fd00::32) 56 data bytes

--- fd00::32 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

waiting for fd00::32 ...
PING fd00::32(fd00::32) 56 data bytes
64 bytes from fd00::32: icmp_seq=1 ttl=64 time=32.6 ms

--- fd00::32 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 32.622/32.622/32.622/0.000 ms

installing xdp_router ...
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:eth0(ifindex:142)
 - Pinning maps in /sys/fs/bpf/eth0/
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:eth1(ifindex:145)
 - Pinning maps in /sys/fs/bpf/eth1/

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:135 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(2): l2tp fd00::21 -> fd00::12
l2tpeth1(3): l2tp fd00::23 -> fd00::32

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:138 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(2): l2tp fd00::21 -> fd00::12
l2tpeth1(3): l2tp fd00::23 -> fd00::32
Device "eth0" does not exist.
waiting for eth0 up ...
Device "eth0" does not exist.
waiting for eth0 up ...
144: eth0@if145: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 3000 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 0e:42:9a:f0:7e:bb brd ff:ff:ff:ff:ff:ff link-netnsid 0
waiting for remote tunnel endpoint be reachable ...
PING fd00::23(fd00::23) 56 data bytes

--- fd00::23 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

waiting for fd00::23 ...
PING fd00::23(fd00::23) 56 data bytes
64 bytes from fd00::23: icmp_seq=1 ttl=64 time=616 ms

--- fd00::23 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 616.063/616.063/616.063/0.000 ms

installing xdp_router ...
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:eth0(ifindex:144)
 - Pinning maps in /sys/fs/bpf/eth0/

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:132 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(2): l2tp fd00::32 -> fd00::23
PING 10.0.0.2 (10.0.0.2) 56(84) bytes of data.
64 bytes from 10.0.0.2: icmp_seq=1 ttl=64 time=0.320 ms
64 bytes from 10.0.0.2: icmp_seq=2 ttl=64 time=0.279 ms
64 bytes from 10.0.0.2: icmp_seq=3 ttl=64 time=0.277 ms
64 bytes from 10.0.0.2: icmp_seq=4 ttl=64 time=0.344 ms
64 bytes from 10.0.0.2: icmp_seq=5 ttl=64 time=0.276 ms
^C
--- 10.0.0.2 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4076ms
rtt min/avg/max/mdev = 0.276/0.299/0.344/0.027 ms
PING 10.0.0.3 (10.0.0.3) 56(84) bytes of data.
64 bytes from 10.0.0.3: icmp_seq=1 ttl=63 time=0.271 ms
64 bytes from 10.0.0.3: icmp_seq=2 ttl=63 time=0.320 ms
64 bytes from 10.0.0.3: icmp_seq=3 ttl=63 time=0.318 ms

--- 10.0.0.3 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2032ms
rtt min/avg/max/mdev = 0.271/0.303/0.320/0.022 ms
```

