#!/bin/bash

cookie="1122334455667788"
set -e
mount -t bpf bpf /sys/fs/bpf/

ip -6 addr add fd00::32/128 dev lo
ip addr add 10.0.0.3/32 dev lo

until ip link show eth0 up; do
   echo "waiting for eth0 up ..."
   sleep 1
done

sleep 1

ip -6 addr add fd00:2::3/64 dev eth0
ip -6 route add fd00::23/128 via fd00:2::2 src fd00::32

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00::23; do
  echo "waiting for fd00::23 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00::32 remote fd00::23
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65534 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.1 peer 192.168.102.2 dev l2tpeth0
ip link set dev l2tpeth0 up

ip route add 192.168.102.0/24 dev l2tpeth0
ip route add default via 192.168.102.2 src 10.0.0.3

echo ""
echo "installing xdp_router ..."
ulimit -l 1024
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
#/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_pass
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0

/bin/bash
