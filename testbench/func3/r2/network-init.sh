#!/bin/bash

cookie="1122334455667788"
set -e
mount -t bpf bpf /sys/fs/bpf/

ip -6 addr add fd00::21/128 dev lo
ip -6 addr add fd00::23/128 dev lo
ip addr add 10.0.0.2/32 dev lo

until ip link show eth1 up; do
   echo "waiting for eth1 up ..."
   sleep 1
done

ip -6 addr add fd00:1::2/64 dev eth0
ip -6 addr add fd00:2::2/64 dev eth1
ifconfig eth0
ifconfig eth1

ip -6 route add fd00::12/128 via fd00:1::1 src fd00::21
ip -6 route add fd00::32/128 via fd00:2::3 src fd00::23
ip -6 route

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00::12; do
  echo "waiting for fd00::12 ..."
  sleep 1
done
while ! ping6 -c 1 -W 1 fd00::32; do
  echo "waiting for fd00::32 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00::21 remote fd00::12
ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00::23 remote fd00::32

ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none

ip addr add 192.168.101.2 peer 192.168.101.1 dev l2tpeth0
ip addr add 192.168.102.2 peer 192.168.102.1 dev l2tpeth1

ip link set dev l2tpeth0 up
ip link set dev l2tpeth1 up

ip route add 192.168.101.0/24 dev l2tpeth0
ip route add 192.168.102.0/24 dev l2tpeth1

ip route add 10.0.0.1/32 via 192.168.101.1 src 10.0.0.2
ip route add 10.0.0.3/32 via 192.168.102.1 src 10.0.0.2

echo ""
echo "installing xdp_router ..."
ulimit -l 1024
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth1 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
#/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_pass
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth1

/bin/bash
