#!/bin/bash
TERMINATE="$1"  # terminate script after tests, otherwise start shell
TESTCASE="$(basename $PWD): l2tpv3 tunnels r1-r2-r3 with XDP (no cRPD)"

cat <<EOF

==============================================================================
$TESTCASE
==============================================================================

+--------+               +--------+               +--------+
|   r1   |               |   r2   |               |   r3   |
|        |--fd00:1::/64--|        |--fd00:2::/64--|        |
|10.0.0.1|               |10.0.0.2|               |10.0.0.3|
+--------+               +--------+               +--------+

EOF

trap "docker kill r1 r2 r3 && sudo chown -R $(id -u):$(id -u) >/dev/null 2>&1" EXIT

set -e

docker kill r1 r2 r3 >/dev/null 2>&1 || true

docker rm r1 r2 r3 2>/dev/null || true

SECONDS=0

docker run -ti --privileged -d \
   -v ${PWD}/r1/network-init.sh:/network-init.sh \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r1 --hostname r1 \
   l2tpv3router /network-init.sh

docker run -ti --privileged -d \
   -v ${PWD}/r2/network-init.sh:/network-init.sh \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r2 --hostname r2 \
   l2tpv3router /network-init.sh

docker run -ti --privileged -d \
   -v ${PWD}/r3/network-init.sh:/network-init.sh \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r3 --hostname r3 \
   l2tpv3router /network-init.sh

./add_link.sh r1 r2 
./add_link.sh r2 r3

#./validate.sh
echo "validation finished in $SECONDS seconds"

if [ -z "$TERMINATE" ]; then
  echo ""
  echo "launching shell for interactive use. Exit to terminate containers"
  echo ""
  /bin/bash
fi

