#!/bin/bash
TERMINATE="$1"  # terminate script after tests, otherwise start shell
TESTCASE="$(basename $PWD): l2tpv3 tunnels between cRPD with XDP"

cat <<EOF

==============================================================================
$TESTCASE
==============================================================================

            +-------------+
            |   spine1    |
            |  10.0.0.1   |
            +-------------+
             /     |     \\
            /      |      \\  l2tpv3 tunnels over
           /       |       \\    v6 veth links
          /        |        \\
+---------+   +---------+   +---------+
|   r1    |   |   r2    |   |   r3    |
|10.0.0.11|   |10.0.0.12|   |10.0.0.13|
+--------+    +---------+   +---------+

EOF

trap "docker kill spine1 r1 r2 r3 && sudo chown -R $(id -u):$(id -u) >/dev/null 2>&1" EXIT

set -e

docker kill spine1 r1 r2 r3 >/dev/null 2>&1 || true

SECONDS=0

docker run -ti --rm --privileged -d \
   -v ${PWD}/spine1:/config \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name spine1 --hostname spine1 \
   crpd_l2tpv3

docker run -ti --rm --privileged -d \
   -v ${PWD}/r1:/config \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r1 --hostname r1 \
   crpd_l2tpv3

./add_link.sh spine1 r1

docker run -ti --rm --privileged -d \
   -v ${PWD}/r2:/config \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r2 --hostname r2 \
   crpd_l2tpv3

./add_link.sh spine1 r2

docker run -ti --rm --privileged -d \
   -v ${PWD}/r3:/config \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r3 --hostname r3 \
   crpd_l2tpv3

./add_link.sh spine1 r3

./validate.sh
echo "validation finished in $SECONDS seconds"

if [ -z "$TERMINATE" ]; then
  echo ""
  echo "launching shell for interactive use. Exit to terminate containers"
  echo ""
  /bin/bash
fi

