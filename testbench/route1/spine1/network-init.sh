#!/bin/bash

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::1/128 dev lo
ip addr add 10.0.0.1/32 dev lo

until ip link show eth2 up; do
  echo "waiting for eth2 up ..."
  sleep 1
done

/usr/sbin/avahi-autoipd --no-drop-root --daemonize eth0
/usr/sbin/avahi-autoipd --no-drop-root --daemonize eth1
/usr/sbin/avahi-autoipd --no-drop-root --daemonize eth2

echo ""
echo "installing xdp_l2tpv3 ..."
ulimit -l 2048
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth1 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth2 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "programming tx_ports map ..."
/sbin/xdp_tunnels --dev eth0 < /dev/null
/sbin/xdp_tunnels --dev eth1 < /dev/null
/sbin/xdp_tunnels --dev eth2 < /dev/null
