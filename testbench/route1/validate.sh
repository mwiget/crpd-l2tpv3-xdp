#!/bin/bash

SECONDS=0

echo "ping ipv4 loopbacks via tunnel from spine1 ..."

while true; do
  docker exec -ti spine1 ping -c 1 10.0.0.11 && break
  echo "$SECONDS: waiting to reach r1 @10.0.0.11 ..."
  sleep 1
done

echo ""
while true; do
  docker exec -ti spine1 ping -c 1 10.0.0.12 && break
  echo "$SECONDS: waiting to reach r2 @10.0.0.12 ..."
  sleep 1
done

echo ""
while true; do
  docker exec -ti spine1 ping -c 1 10.0.0.13 && break
  echo "$SECONDS: waiting to reach r3 @10.0.0.13 ..."
  sleep 1
done

echo "ping ipv4 loopbacks from r1 ..."
docker exec -ti r1 ping -c 3 10.0.0.1
docker exec -ti r1 ping -c 3 10.0.0.12
docker exec -ti r1 ping -c 3 10.0.0.13

echo "ping ipv6 loopbacks from r1 ..."
docker exec -ti r1 ping6 -c 3 fd00:1::1
docker exec -ti r1 ping6 -c 3 fd00:2::12
docker exec -ti r1 ping6 -c 3 fd00:3::13

echo ""
echo "collecting xdp stats on spine1 ..."
if ! docker exec -ti spine1 xdp_stats -s -d eth0 ; then
  echo ""
  echo "xdp program missing on spine1 !!"
  docker exec -ti spine1 tail /root/network-init.log
  exit 1
fi

for instance in r1 r2 r3; do
  echo "collecting xdp stats on $instance ..."
  if ! docker exec -ti $instance xdp_stats -s -d eth0 ; then
    echo ""
    echo "xdp program missing on $instance !!"
    docker exec -ti $instance tail /root/network-init.log
    exit 1
  fi
done

echo ""
echo "show isis adj on spine1 ..."
docker exec spine1 cli show isis adj || docker exec spine1 vtysh -c "show isis neighbor"

echo ""
echo "show ip route hidden extensive on spine1 ..."
docker exec spine1 cli show route hidden extensive || true

echo ""
echo "success in $SECONDS seconds"
