
no longer trying this method (keep ttl untouched, leading to flooding).

```
=============================================================================================
test5: r1 -- 10g link -- r2, flood thru l2tpv3 tunnels using plain linux (with xdp)
=============================================================================================

298a8437e7e01af7f5240d2015c7e42b4ec476641f0669db557c2b35893b99a0
r1 298a8437e7e0
r1 has pid 745896
moving endpoints to netns ...
bringing links up ...
waiting for container r2 ...
Device "ens3f1" does not exist.
waiting for ens3f1 up ...
r2 705f14160d13
r2 has pid 746092
moving endpoints to netns ...
bringing links up ...
121: ens3f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 xdp qdisc mq state DOWN mode DEFAULT group default qlen 1000
    link/ether 00:1b:21:99:cc:15 brd ff:ff:ff:ff:ff:ff
    prog/xdp id 187 tag 2ad827ca1cb67162 jited 

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 fd00:2::102  prefixlen 128  scopeid 0x0<global>
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        inet6 fd00:2::101  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::105  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::104  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::103  prefixlen 128  scopeid 0x0<global>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


PING fd00::1(fd00::1) 56 data bytes

--- fd00::1 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1031ms

waiting for v6 gateway to remote tunnel endpoints be reachable ...
PING fd00::1(fd00::1) 56 data bytes
64 bytes from fd00::1: icmp_seq=1 ttl=64 time=0.086 ms
64 bytes from fd00::1: icmp_seq=2 ttl=64 time=0.090 ms

--- fd00::1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1004ms
rtt min/avg/max/mdev = 0.086/0.088/0.090/0.002 ms
Tunnel 5, encap IP
  From fd00:2::105 to fd00:1::105
  Peer tunnel 5
Tunnel 4, encap IP
  From fd00:2::104 to fd00:1::104
  Peer tunnel 4
Tunnel 3, encap IP
  From fd00:2::103 to fd00:1::103
  Peer tunnel 3
Tunnel 2, encap IP
  From fd00:2::102 to fd00:1::102
  Peer tunnel 2
Tunnel 1, encap IP
  From fd00:2::101 to fd00:1::101
  Peer tunnel 1
Session 65531 in tunnel 5
  Peer session 65531, tunnel 5
  interface name: l2tpeth4
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65532 in tunnel 4
  Peer session 65532, tunnel 4
  interface name: l2tpeth3
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65533 in tunnel 3
  Peer session 65533, tunnel 3
  interface name: l2tpeth2
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65534 in tunnel 2
  Peer session 65534, tunnel 2
  interface name: l2tpeth1
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65535 in tunnel 1
  Peer session 65535, tunnel 1
  interface name: l2tpeth0
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788

installing xdp_router ...
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:ens3f1(ifindex:121)
 - Pinning maps in /sys/fs/bpf/ens3f1/

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:63 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(8): l2tp fd00:2::101 -> fd00:1::101
l2tpeth1(9): l2tp fd00:2::102 -> fd00:1::102
l2tpeth2(10): l2tp fd00:2::103 -> fd00:1::103
l2tpeth3(11): l2tp fd00:2::104 -> fd00:1::104
l2tpeth4(12): l2tp fd00:2::105 -> fd00:1::105

PING 192.168.101.1 (192.168.101.1) 56(84) bytes of data.
64 bytes from 192.168.101.1: icmp_seq=1 ttl=64 time=0.145 ms

--- 192.168.101.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.145/0.145/0.145/0.000 ms
PING 192.168.102.1 (192.168.102.1) 56(84) bytes of data.
64 bytes from 192.168.102.1: icmp_seq=1 ttl=64 time=0.209 ms

--- 192.168.102.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.209/0.209/0.209/0.000 ms
PING 192.168.103.1 (192.168.103.1) 56(84) bytes of data.
64 bytes from 192.168.103.1: icmp_seq=1 ttl=64 time=0.223 ms

--- 192.168.103.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.223/0.223/0.223/0.000 ms
PING 192.168.104.1 (192.168.104.1) 56(84) bytes of data.
64 bytes from 192.168.104.1: icmp_seq=1 ttl=64 time=0.233 ms

--- 192.168.104.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.233/0.233/0.233/0.000 ms
PING 192.168.105.1 (192.168.105.1) 56(84) bytes of data.
64 bytes from 192.168.105.1: icmp_seq=1 ttl=64 time=0.188 ms

--- 192.168.105.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.188/0.188/0.188/0.000 ms

adding ecmp routes to remote v4 loopback via l2tp tunnels ...

ip route:
10.0.0.0/8 proto static 
	nexthop via 192.168.101.1 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.1 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.1 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.1 dev l2tpeth3 weight 1 
	nexthop via 192.168.105.1 dev l2tpeth4 weight 1 
192.168.101.0/24 dev l2tpeth0 scope link 
192.168.101.1 dev l2tpeth0 proto kernel scope link src 192.168.101.2 
192.168.102.0/24 dev l2tpeth1 scope link 
192.168.102.1 dev l2tpeth1 proto kernel scope link src 192.168.102.2 
192.168.103.0/24 dev l2tpeth2 scope link 
192.168.103.1 dev l2tpeth2 proto kernel scope link src 192.168.103.2 
192.168.104.0/24 dev l2tpeth3 scope link 
192.168.104.1 dev l2tpeth3 proto kernel scope link src 192.168.104.2 
192.168.105.0/24 dev l2tpeth4 scope link 
192.168.105.1 dev l2tpeth4 proto kernel scope link src 192.168.105.2 

flood ping to remote v4 loopback ...
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- 10.0.0.1 ping statistics ---
1000 packets transmitted, 1000 received, 0% packet loss, time 59ms
rtt min/avg/max/mdev = 0.035/0.046/0.102/0.006 ms, ipg/ewma 0.059/0.041 ms

netstat -i:
Kernel Interface table
Iface      MTU    RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
ens3f1    1500   993323      0 357278 0        993336      0      0      0 BMRU
l2tpeth0  1434        4      0      0 0             5      0      0      0 BMRU
l2tpeth1  1434        3      0      0 0             5      0      0      0 BMRU
l2tpeth2  1434     1000      0      0 0          1002      0      0      0 BMRU
l2tpeth3  1434        2      0      0 0             3      0      0      0 BMRU
l2tpeth4  1434        2      0      0 0             3      0      0      0 BMRU
lo       65536        0      0      0 0             0      0      0      0 LRU

xdp_stats --dev ens3f1 --single:

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:62 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250435
XDP_DROP              16 pkts (         7 pps)           2 Kbytes (     0 Mbits/s) period:2.250424
XDP_PASS            2016 pkts (       447 pps)         330 Kbytes (     1 Mbits/s) period:2.250424
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250426
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250427

10.1.1.1 is unreachable
10.1.1.2 is unreachable
10.1.1.3 is unreachable
10.1.1.4 is unreachable
10.1.1.5 is unreachable

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:62 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250340
XDP_DROP              16 pkts (         0 pps)           2 Kbytes (     0 Mbits/s) period:2.250316
XDP_PASS            2021 pkts (         0 pps)         330 Kbytes (     0 Mbits/s) period:2.250314
XDP_TX           1232431 pkts (    156828 pps)      202118 Kbytes (   206 Mbits/s) period:2.250314
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250314



SUCCESS!
SUMMARY test5: r1 -- 10g link -- r2, flood thru l2tpv3 tunnels using plain linux (with xdp): PASS
```
