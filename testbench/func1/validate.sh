#!/bin/bash

while true; do
  docker exec spine1 ping -c 1 -W 1 192.168.101.2 && break
  if [ $SECONDS -gt 60 ]; then
    echo "$SECONDS tunnel l2tpeth0 failed"
    exit
  fi
  echo "$SECONDS: waiting for tunnel l2tpeth0 ..."
  sleep 1
done

while true; do
  docker exec spine1 ping -c 1 -W 1 192.168.101.2 && break
  if [ $SECONDS -gt 60 ]; then
    echo ""
    echo "FAIL: tunnel l2tpeth0 failed"
    exit
  fi
  echo "$SECONDS: waiting for tunnel l2tpeth0 ..."
  sleep 1
done
echo "tunnel l2tpeth0 ready"

while true; do
  docker exec spine1 ping -c 1 -W 1 192.168.102.2 && break
  if [ $SECONDS -gt 60 ]; then
    echo ""
    echo "FAIL: tunnel l2tpeth1 failed"
    exit
  fi
  echo "$SECONDS: waiting for tunnel l2tpeth1 ..."
  sleep 1
done
echo "tunnel l2tpeth1 ready"

while true; do
  docker exec spine1 ping -c 1 -W 1 192.168.103.2 && break
  if [ $SECONDS -gt 60 ]; then
    echo ""
    echo "FAIL: tunnel l2tpeth2 failed"
    exit
  fi
  echo "$SECONDS: waiting for tunnel l2tpeth2 ..."
  sleep 1
done
echo "tunnel l2tpeth2 ready"

while true; do
  docker exec spine1 cli show isis adj | grep Up | wc -l | grep ^3$ && break
  if [ $SECONDS -gt 60 ]; then
    echo ""
    echo "FAIL: not all isis adjacencies up"
    exit
  fi
  echo "$SECONDS: waiting for isis adjacencies ..."
  sleep 1
done
echo "isis adjacencies up:"
docker exec spine1 cli show isis adj
echo ""

while true; do
  docker exec spine1 cli show bgp summary | grep Estab | wc -l | grep ^6$ && break
  if [ $SECONDS -gt 60 ]; then
    echo ""
    echo "FAIL: not all bgp sessions established"
    exit
  fi
  echo "$SECONDS: waiting for bgp sessions to be established ..."
  sleep 1
done
echo "bgp sessions established:"
docker exec spine1 cli show bgp summary
echo ""

echo "checking for hidden bgp routes ..."
if docker exec spine1 cli show route hidden| grep BGP; then
  echo ""
  echo "FAIL: hidden bgp routes found:"
  docker exec spine1 cli show route hidden extensive
  # exit    ignoring this test case for now until fixed in cRPD ...
fi
echo "no hidden bgp routes found"

echo ""

