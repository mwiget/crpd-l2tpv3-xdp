#!/bin/bash

# set src/dst mac.
# rewrite the dst IP to the target host 48.0.0.0/8 > 10.99.204.8/32/
# randomizing source IPS 16.0.0.0/8 > 10.10.0.0/16

tcprewrite \
  --enet-dmac=40:a6:b7:08:7e:21 \
  --enet-smac=40:a6:b7:08:7e:20 \
  --pnat=16.0.0.0/8:10.10.0.0/16,48.0.0.0/8:10.11.0.0/16 \
  --infile=imix-source.pcap \
  --outfile=imix.pcap
