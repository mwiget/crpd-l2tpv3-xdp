# Performance encap test using pktgen

imix.pcap and rewrite instructions used from https://github.com/atoonk/dpdk_pktgen/

```
apt-get update && apt-get -y --no-install-recommends install liblua5.3 libnuma-dev pciutils libpcap-dev
```

hugepage kernel config and iommu, required for pktgen:

```
$ grep huge /etc/default/grub
GRUB_CMDLINE_LINUX="hugepages=512 default_hugepagesz=1G hugepagesz=1G hugepages=4 iommu=pt intel_iommu=on pci=assign-busses"
```

```
$  grep huge /etc/fstab 
nodev /dev/hugepages1G hugetlbfs pagesize=1GB 0 0
```
