#!/bin/bash

cookie="1122334455667788"
set -e

interface=${1:-eth0}
exit_container=${2:-no}

mount -t bpf bpf /sys/fs/bpf/

until ip link show $interface up; do
   echo "waiting for $interface up ..."
   sleep 1
done

echo "assign ipv6 and ipv4 address to $interface ..."
ip -6 addr add fd00::1/64 dev $interface
ip -6 route add fd00::/16 via fd00::2

ip addr add 10.253.0.1/24 dev $interface

ip -6 addr add fd00:1::101/128 dev lo
ip -6 addr add fd00:1::102/128 dev lo
ip -6 addr add fd00:1::103/128 dev lo
ip -6 addr add fd00:1::104/128 dev lo
ip -6 addr add fd00:1::105/128 dev lo

ip addr add 10.0.0.1 dev lo

echo "setting rx buffer size to 4k on $interface ..."
ethtool -G $interface rx 4096 || true

echo "setting 10 cpus to $interface (rx tx combined) ..."
ethtool -L $interface combined 4 || true


echo ""
ifconfig lo
echo ""

echo "adding static lladdr v6 neighbor for fd00::2 ..."
ip -6 neigh add fd00::2 lladdr 02:11:22:33:44:55 nud permanent dev $interface

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00:1::101 remote fd00:2::101
ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00:1::102 remote fd00:2::102
ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00:1::103 remote fd00:2::103
ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00:1::104 remote fd00:2::104
ip l2tp add tunnel tunnel_id 5 peer_tunnel_id 5 encap ip local fd00:1::105 remote fd00:2::105

ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65534 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65533 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65532 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 5 session_id 65531 peer_session_id 65531 cookie $cookie peer_cookie $cookie l2spec_type none

ip addr add 192.168.101.1 peer 192.168.101.2 dev l2tpeth0
ip addr add 192.168.102.1 peer 192.168.102.2 dev l2tpeth1
ip addr add 192.168.103.1 peer 192.168.103.2 dev l2tpeth2
ip addr add 192.168.104.1 peer 192.168.104.2 dev l2tpeth3
ip addr add 192.168.105.1 peer 192.168.105.2 dev l2tpeth4

ip link set dev l2tpeth0 up
ip link set dev l2tpeth1 up
ip link set dev l2tpeth2 up
ip link set dev l2tpeth3 up
ip link set dev l2tpeth4 up

ip route add 192.168.101.0/24 dev l2tpeth0
ip route add 192.168.102.0/24 dev l2tpeth1
ip route add 192.168.103.0/24 dev l2tpeth2
ip route add 192.168.104.0/24 dev l2tpeth3
ip route add 192.168.105.0/24 dev l2tpeth4

arp -s 192.168.101.2 02:00:00:00:01:01
arp -s 192.168.102.2 02:00:00:00:01:02
arp -s 192.168.103.2 02:00:00:00:01:03
arp -s 192.168.104.2 02:00:00:00:01:04
arp -s 192.168.105.2 02:00:00:00:01:05

ip l2tp show tunnel
ip l2tp show session

echo "adding ecmp routes to remote v4 loopback via l2tp tunnels ..."
ip route add 10.0.0.0/8 proto static \
  nexthop dev l2tpeth0 via 192.168.101.2 weight 1 \
  nexthop dev l2tpeth1 via 192.168.102.2 weight 1 \
  nexthop dev l2tpeth2 via 192.168.103.2 weight 1 \
  nexthop dev l2tpeth3 via 192.168.104.2 weight 1 \
  nexthop dev l2tpeth4 via 192.168.105.2 weight 1

echo ""
echo "installing xdp_router ..."
ulimit -l 1024
#/sbin/xdp_loader --dev $interface --auto-mode --force --filename /root/xdp_router.o --progsec xdp_tx
/sbin/xdp_loader --dev $interface --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/root/tunnels.sh | /sbin/xdp_tunnels --dev $interface
echo ""

echo ""
echo "READY!"
if [ "no" == "$exit_container" ]; then
   /bin/bash
fi
exit 0
sudo ethtool -G $interface rx 4096
sudo ethtool -L $interface combined 10
