#!/bin/bash
TESTCASE="$(basename $PWD): ryzen(pktgen) --- 40g --- ryzen(r1): v4 encap in l2tpv3"
echo ""
echo "============================================================================================="
echo "$TESTCASE"
echo "============================================================================================="
echo ""

interface=enp9s0f1

trap "docker kill r1 >/dev/null 2>&1" EXIT

set -e

docker kill r1 || true

./add_phy.sh r1 $interface &
docker run -ti --rm --privileged \
   -v ${PWD}/launch_r1.sh:/launch.sh:ro \
   --sysctl net.ipv6.conf.all.disable_ipv6=0 \
   --sysctl net.ipv6.conf.all.forwarding=1 \
   --net none \
   --name r1 --hostname r1 \
   l2tpv3router /launch.sh $interface $1

echo "SUMMARY $TESTCASE: PASS"
