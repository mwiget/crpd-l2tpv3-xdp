#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::1/128 dev lo
ip -6 addr add fd00:1::1/128 dev lo
ip -6 addr add fd00:1::2/128 dev lo
ip -6 addr add fd00:1::3/128 dev lo
ip -6 addr add fd00:1::4/128 dev lo

ip addr add 10.0.0.1/32 dev lo

until ip link show enp2s0 up; do
  echo "waiting for enp2s0 up ..."
  sleep 1
done

until ip link show eth2 up; do
  echo "waiting for eth2 up ..."
  sleep 1
done

ethtool --offload enp2s0 rxvlan off txvlan off
ethtool --offload eth2 rxvlan off txvlan off

# set (dummy) link local ip's, so ip4 routing works for decap packets
ip addr add 169.254.1.1/16 dev eth0
ip addr add 169.254.1.1/16 dev eth1

# add vlan 100 on enp2s0
ip link add link enp2s0 name vlan100 type vlan id 100
ip link set dev vlan100 up
ip addr add 169.254.1.1/16 dev vlan100

# add vlan 200 on eth2
ip link add link eth2 name vlan200 type vlan id 200
ip link set dev vlan200 up
ip addr add 169.254.1.1/16 dev vlan200

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:1::11; do
  echo "waiting for fd00:1::11 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00:1::1 remote fd00:1::11
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.101.1/24 peer 192.168.101.2 dev l2tpeth0
ip link set dev l2tpeth0 up
#ip route add 192.168.101.0/24 dev l2tpeth0

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:2::12; do
  echo "waiting for fd00:2::12 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00:1::2 remote fd00:2::12
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.1 peer 192.168.102.2 dev l2tpeth1
ip link set dev l2tpeth1 up
ip route add 192.168.102.0/24 dev l2tpeth1

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:3::13; do
  echo "waiting for fd00:3::13 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00:1::3 remote fd00:3::13
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.103.1 peer 192.168.103.2 dev l2tpeth2
ip link set dev l2tpeth2 up
ip route add 192.168.103.0/24 dev l2tpeth2

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:4::14; do
  echo "waiting for fd00:4::14 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00:1::4 remote fd00:4::14
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.104.1 peer 192.168.104.2 dev l2tpeth3
ip link set dev l2tpeth3 up
ip route add 192.168.104.0/24 dev l2tpeth3

# temp fix until cRPD learns l2tpv3 routes via BGP
ip route add 10.0.0.11/32 via 192.168.101.2 dev l2tpeth0 src 10.0.0.1
ip route add 10.0.0.12/32 via 192.168.102.2 dev l2tpeth1 src 10.0.0.1
ip route add 10.0.0.13/32 via 192.168.103.2 dev l2tpeth2 src 10.0.0.1
ip route add 10.0.0.14/32 via 192.168.104.2 dev l2tpeth3 src 10.0.0.1

echo ""
echo "installing xdp_l2tpv3 ..."
ulimit -l 2048
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth1 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev eth2 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_loader --dev enp2s0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "programming l2tpv3 tunnel in xdp_map ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth1
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth2
/root/tunnels.sh | /sbin/xdp_tunnels --dev enp2s0
