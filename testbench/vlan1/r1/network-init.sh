#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::11/128 dev lo
ip -6 addr add fd00:1::11/128 dev lo

ip addr add 10.0.0.11/32 dev lo

until ip link show eth0 up; do
  echo "waiting for eth0 up ..."
  sleep 1
done

# set (dummy) link local ip's, so ip4 routing works for decap packets
ip addr add 169.254.1.11/16 dev eth0

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:1::1; do
  echo "waiting for fd00:1::1 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00:1::11 remote fd00:1::1

echo "adding l2tp sessions ..."
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none

ip addr add 192.168.101.2/24 peer 192.168.101.1 dev l2tpeth0

ip link set dev l2tpeth0 up

# temp fix until cRPD learns l2tpv3 routes via BGP
ip route add 10.0.0.0/24 via 192.168.101.1 dev l2tpeth0 src 10.0.0.11

echo ""
echo "installing xdp_l2tpv3 on eth0 ..."
ulimit -l 2048
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "set xdp tunnel map on eth0 ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev eth0
