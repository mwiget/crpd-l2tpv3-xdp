```
$ ./run.sh exit
=============================================================================================
test1: r1 -- veth -- r2, flood ping thru l2tpv3 tunnels using plain linux (no xdp)
=============================================================================================

b3e88396cfb25660fc3f0e62807ae1d9af662324b3d2986e14d25f00f56f5cf8
Device "veth0" does not exist.
waiting for veth0 up ...
Device "veth0" does not exist.
waiting for veth0 up ...
196: veth0@if197: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 3000 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether ce:b8:0f:e7:be:01 brd ff:ff:ff:ff:ff:ff link-netnsid 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 fd00:2::105  prefixlen 128  scopeid 0x0<global>
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        inet6 fd00:2::104  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::103  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::102  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::101  prefixlen 128  scopeid 0x0<global>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


PING fd00::1(fd00::1) 56 data bytes

--- fd00::1 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1009ms

waiting for v6 gateway to remote tunnel endpoints be reachable ...
PING fd00::1(fd00::1) 56 data bytes
64 bytes from fd00::1: icmp_seq=1 ttl=64 time=0.087 ms
64 bytes from fd00::1: icmp_seq=2 ttl=64 time=0.070 ms

--- fd00::1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.070/0.078/0.087/0.008 ms
Tunnel 5, encap IP
  From fd00:2::105 to fd00:1::105
  Peer tunnel 5
Tunnel 4, encap IP
  From fd00:2::104 to fd00:1::104
  Peer tunnel 4
Tunnel 3, encap IP
  From fd00:2::103 to fd00:1::103
  Peer tunnel 3
Tunnel 2, encap IP
  From fd00:2::102 to fd00:1::102
  Peer tunnel 2
Tunnel 1, encap IP
  From fd00:2::101 to fd00:1::101
  Peer tunnel 1
Session 65531 in tunnel 5
  Peer session 65531, tunnel 5
  interface name: l2tpeth4
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65532 in tunnel 4
  Peer session 65532, tunnel 4
  interface name: l2tpeth3
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65533 in tunnel 3
  Peer session 65533, tunnel 3
  interface name: l2tpeth2
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65534 in tunnel 2
  Peer session 65534, tunnel 2
  interface name: l2tpeth1
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65535 in tunnel 1
  Peer session 65535, tunnel 1
  interface name: l2tpeth0
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
PING 192.168.101.1 (192.168.101.1) 56(84) bytes of data.
64 bytes from 192.168.101.1: icmp_seq=1 ttl=64 time=0.143 ms

--- 192.168.101.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.143/0.143/0.143/0.000 ms
PING 192.168.102.1 (192.168.102.1) 56(84) bytes of data.
64 bytes from 192.168.102.1: icmp_seq=1 ttl=64 time=0.081 ms

--- 192.168.102.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.081/0.081/0.081/0.000 ms
PING 192.168.103.1 (192.168.103.1) 56(84) bytes of data.
64 bytes from 192.168.103.1: icmp_seq=1 ttl=64 time=0.043 ms

--- 192.168.103.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.043/0.043/0.043/0.000 ms
PING 192.168.104.1 (192.168.104.1) 56(84) bytes of data.
64 bytes from 192.168.104.1: icmp_seq=1 ttl=64 time=0.037 ms

--- 192.168.104.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.037/0.037/0.037/0.000 ms
PING 192.168.105.1 (192.168.105.1) 56(84) bytes of data.
64 bytes from 192.168.105.1: icmp_seq=1 ttl=64 time=0.061 ms

--- 192.168.105.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.061/0.061/0.061/0.000 ms

adding ecmp routes to remote v4 loopback via l2tp tunnels ...

ip route:
10.0.0.0/8 proto static src 10.0.0.2 
	nexthop via 192.168.101.1 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.1 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.1 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.1 dev l2tpeth3 weight 1 
	nexthop via 192.168.105.1 dev l2tpeth4 weight 1 
192.168.101.1 dev l2tpeth0 proto kernel scope link src 192.168.101.2 
192.168.102.1 dev l2tpeth1 proto kernel scope link src 192.168.102.2 
192.168.103.1 dev l2tpeth2 proto kernel scope link src 192.168.103.2 
192.168.104.1 dev l2tpeth3 proto kernel scope link src 192.168.104.2 
192.168.105.1 dev l2tpeth4 proto kernel scope link src 192.168.105.2 

flood ping to remote v4 loopback ...
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- 10.0.0.1 ping statistics ---
1000 packets transmitted, 1000 received, 0% packet loss, time 34ms
rtt min/avg/max/mdev = 0.009/0.017/0.063/0.005 ms, ipg/ewma 0.034/0.010 ms

netstat -i:
Kernel Interface table
Iface      MTU    RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
l2tpeth0  2934     1004      0      0 0          1500      0      0      0 BMRU
l2tpeth1  2934      507      0      0 0             5      0      0      0 BMRU
l2tpeth2  2934        3      0      0 0             4      0      0      0 BMRU
l2tpeth3  2934        3      0      0 0           483      0      0      0 BMRU
l2tpeth4  2934      476      1      1 0             5      0      0      0 BMRU
lo       65536        0      0      0 0             0      0      0      0 LRU
veth0     3000     2016      0      0 0          2014      0      0      0 BMRU

SUCCESS!
SUMMARY test1: r1 -- veth -- r2, flood ping thru l2tpv3 tunnels using plain linux (no xdp): PASS
```
