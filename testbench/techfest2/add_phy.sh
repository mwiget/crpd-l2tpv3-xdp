#!/bin/bash

c1=$1
interface=$2
if [ -z "$interface" ]; then
    echo "$0 <container1> <interface>"
    exit 1
fi

set -e	# terminate on error

sudo mkdir -p /var/run/netns

SECONDS=0

until [ ! -z "$(docker ps -q -f name=$c1)" ]; do
   echo "waiting for container $c1 ..."
   sleep 1
   if [ $SECONDS -gt 5 ]; then
      echo "$c1 not running"
      exit 1
   fi
done

fc1=$(docker ps -q -f name=$c1)
echo "$c1 $fc1"
pid1=$(docker inspect -f "{{.State.Pid}}" $fc1)
if [ -z "$pid1" ]; then
    echo "Can't find pid for container $c1"
    exit 1
fi

echo "$c1 has pid $pid1"

sudo ln -sf /proc/$pid1/ns/net /var/run/netns/$c1

sudo ifconfig $interface mtu 3000

echo "moving endpoints to netns ..."
sudo ip link set $interface netns $c1

echo "bringing links up ..."
sudo ip netns exec $c1 ip link set up $interface
