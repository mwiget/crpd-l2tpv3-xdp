#!/bin/bash

set -e

mount -t bpf bpf /sys/fs/bpf/

until ip link show enp11s0f3 up; do
   echo "waiting for interface enp11s0f3 up ..."
   sleep 1
done

#ip route del default

ip -6 addr add fd00::1/128 dev lo
ip addr add 10.0.0.1/32 dev lo

ip -6 addr add fd00:1::1/64 dev enp11s0f0
ip -6 addr add fd00:2::1/64 dev enp11s0f1
ip -6 addr add fd00:3::1/64 dev enp11s0f2
ip -6 addr add fd00:4::1/64 dev enp11s0f3

ip addr add 10.251.0.1/24 dev enp11s0f0
ip addr add 10.252.0.1/24 dev enp11s0f1
ip addr add 10.253.0.1/24 dev enp11s0f2
ip addr add 10.254.0.1/24 dev enp11s0f3

echo "adding static lladdr v6 neighbor and arp ..."
ip -6 neigh add fd00:1::2 lladdr 00:1b:21:99:cc:14 nud permanent dev enp11s0f0
arp -s 10.251.0.2 00:1b:21:99:cc:14
ip -6 neigh add fd00:2::2 lladdr 00:1b:21:99:cc:15 nud permanent dev enp11s0f1
arp -s 10.252.0.2 00:1b:21:99:cc:15
ip -6 neigh add fd00:3::2 lladdr 00:1b:21:99:b7:c8 nud permanent dev enp11s0f2
arp -s 10.253.0.2 00:1b:21:99:b7:c8
ip -6 neigh add fd00:4::2 lladdr 00:1b:21:99:b7:c9 nud permanent dev enp11s0f3
arp -s 10.254.0.2 00:1b:21:99:b7:c8

/config/enable_xdp.sh

echo "all set."
