#!/bin/bash

interfaces="enp11s0f0 enp11s0f1 enp11s0f2 enp11s0f3"
cores=5

set -e

echo ""
echo "installing xdp_router ..."
ulimit -l 1024
for int in $interfaces; do
  echo $int ...
  ethtool -G $int rx 4096 || true
  ethtool -L $int combined $cores || true
  /sbin/xdp_loader --dev $int --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
done
for int in $interfaces; do
  echo $int ...
  /sbin/xdp_tunnels --dev $int </dev/null
done

