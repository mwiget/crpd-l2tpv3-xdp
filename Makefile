all: build pktgen

build:
	make -C xdp
	make -C l2tpv3router
	make -C addlink
	scripts/crpd_present.sh && make -C crpd_l2tpv3 || true

pktgen:
	scripts/crpd_present.sh && make -C pktgen

up: build down
	sudo modprobe l2tp-eth
	docker-compose up -d
	scripts/add_link.sh spine1 r1
	scripts/add_link.sh spine1 r2
	scripts/add_link.sh spine1 r3
	scripts/add_link.sh spine1 host1
	scripts/add_link.sh spine1 host2
	./validate.sh

frr: build down
	docker-compose -f docker-compose-frr.yml up -d
	scripts/add_link.sh spine1 r1
	scripts/add_link.sh spine1 r2
	scripts/add_link.sh spine1 r3
	scripts/add_link.sh spine1 host1
	scripts/add_link.sh spine1 host2
	./validate.sh

mac:
	docker-compose build
	docker-compose up -d
	./validate-mac.sh

down:
	docker-compose down
	docker-compose -f docker-compose-frr.yml down
