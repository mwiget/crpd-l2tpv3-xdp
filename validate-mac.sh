#!/bin/bash

echo ""
echo -n "Host OS version:  "
uname -a
echo -n "linuxkit version: "
docker exec spine1 uname -a || true

SECONDS=0

echo ""
echo -n "waiting for all ISIS adjacencies to come up "
while true; do
  docker exec spine1 cli show isis adj 2>/dev/null | grep Up | wc -l | grep 3 && break
  echo -n "."
  sleep 1
done

echo ""
echo "show isis adj on spine1 ..."
docker exec spine1 cli show isis adj || docker exec spine1 vtysh -c "show isis neighbor"
echo ""

echo -n "waiting for all BGP peers to come up "
while true; do
  docker exec  spine1 cli show bgp summary |grep Establ |wc -l | grep '3\|6' && break
  echo -n "."
  sleep 1
done

operational=$SECONDS
SECONDS=0

echo ""
echo "show bgp summary on spine1 ..."
docker exec spine1 cli show bgp summary || docker exec spine1 vtysh -c "show bgp summary"

echo ""
echo "ping r2 from r1 ..."
docker exec -ti r1 ping -f -c 100 fd00::12
echo ""

echo "ping r3 from r1 ..."
docker exec -ti r1 ping -f -c 100 fd00::13
echo ""

echo "ping r1 from r2 ..."
docker exec -ti r2 ping -f -c 100 fd00::11
echo ""

echo "ping r3 from r2 ..."
docker exec -ti r2 ping -f -c 100 fd00::13
echo ""

echo "ping r1 from r3 ..."
docker exec -ti r3 ping -f -c 100 fd00::11
echo ""

echo "ping r2 from r3 ..."
docker exec -ti r3 ping -f -c 100 fd00::12
echo ""

instance=spine1
for interface in eth0 eth1 eth2; do
  echo "collecting xdp stats on $instance $interface ..."
  if ! docker exec -ti $instance xdp_stats -s -d $interface ; then
    echo ""
    echo "xdp program missing on $instance $interface !!"
    docker exec -ti $instance tail /root/network-init.log
    exit 1
  fi
done

echo ""
echo "show isis adj on spine1 ..."
docker exec spine1 cli show isis adj || docker exec spine1 vtysh -c "show isis neighbor"

echo ""
echo "show bgp summary on spine1 ..."
docker exec spine1 cli show bgp summary || docker exec spine1 vtysh -c "show bgp summary"

echo ""
echo "show ip route hidden extensive on spine1 ..."
docker exec spine1 cli show route hidden extensive || true

echo ""
echo -n "Host OS version:  "
uname -a
echo -n "linuxkit version: "
docker exec spine1 uname -a || true

echo ""
echo "operational in $operational seconds"
echo "validation completed in $SECONDS seconds"
